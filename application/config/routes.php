<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/

$route['default_controller'] = 'User/index';
$route['404_override'] = 'my404/index';




/* Admin Panel */
	$route['login']     = 'User/index';
	$route['logout']    = 'User/logout';
	$route['settings']    = 'User/settings';

	/* Calendar */
	$route['calendar']                = 'admin/Calendar';
	$route['calendar/list']           = 'admin/Calendar/lists';
	$route['calendar/add_event_type'] = 'admin/Calendar/add_event_type';
	$route['calendar/dlt_event_type'] = 'admin/Calendar/dlt_event_type';
	$route['calendar/add_event']      = 'admin/Calendar/add_event';
	$route['calendar/update_event']   = 'admin/Calendar/update_event';
	$route['calendar/remove_event']   = 'admin/Calendar/remove_event';

	/*$route['regions']       = 'admin/Region/index';
	$route['region/create'] = 'admin/Region/create';
	$route['region/edit']   = 'admin/Region/edit';
	$route['region/delete'] = 'admin/Region/delete';*/

	$route['appuser']       = 'admin/Appuser/index';
	$route['appuser/details/(:num)'] = 'admin/Appuser/details/$1';
	$route['appuser/notify/(:num)'] = 'admin/Appuser/notify/$1';
	$route['appuser/list_entries'] = 'admin/Appuser/list_entries';
	$route['notify'] = 'admin/Notify/index';


	$route['version']       = 'admin/Version/index';
	$route['version/create'] = 'admin/Version/create';
	$route['version/edit/(:num)'] = 'admin/Version/edit/$1';
	$route['version/details/(:num)'] = 'admin/Version/details/$1';
	$route['version/delete/(:num)'] = 'admin/Version/delete/$1';
	$route['version/list_entries'] = 'admin/Version/list_entries';


	$route['adminuser']       = 'admin/AdminUsers/index';
	$route['adminuser/create'] = 'admin/AdminUsers/create';
	$route['adminuser/edit/(:num)']   = 'admin/AdminUsers/edit/$1';
	$route['adminuser/delete/(:num)'] = 'admin/AdminUsers/delete/$1';
	$route['adminuser/details/(:num)'] = 'admin/AdminUsers/details/$1';
	$route['adminuser/list_entries'] = 'admin/AdminUsers/list_entries';



	$route['seasonal']       = 'admin/Seasonal/index';
	$route['seasonal/create'] = 'admin/Seasonal/create';
	$route['seasonal/edit/(:num)']   = 'admin/Seasonal/edit/$1';
	$route['seasonal/delete/(:num)'] = 'admin/Seasonal/delete/$1';
	$route['seasonal/details/(:num)'] = 'admin/Seasonal/details/$1';
	$route['seasonal/list_entries'] = 'admin/Seasonal/list_entries';



/** New Encrypted API */
	$route['api/activateAndroid'] = 'api/ApiProfile/activateAndroid';
	$route['api/activateiOS'] = 'api/ApiProfile/activateiOS';

	$route['api/updateAndroid'] = 'api/ApiProfile/updateProfileAndroid';
	$route['api/updateiOS'] = 'api/ApiProfile/updateProfileiOS';

	$route['api/profileiOS'] = 'api/ApiProfile/profileiOS';
	$route['api/profileAndroid'] = 'api/ApiProfile/profileAndroid';



/* Web API */
	$route['api/encrypt'] = 'api/ApiCustomer/encrypt';
	$route['api/notification'] = 'api/ApiCustomer/notification';
	$route['api/add_gcm_token'] = 'api/ApiCustomer/addGcmToken';
	$route['api/register'] = 'api/ApiCustomer/register';
	$route['api/activate'] = 'api/ApiCustomer/activate';
	$route['api/activateNew'] = 'api/ApiCustomer/activateNew';
	$route['api/activateNew/ios'] = 'api/ApiCustomer/activateNewiOS';
	$route['api/resendOTP'] = 'api/ApiCustomer/resendOTP';
	$route['api/resendOTP/ios'] = 'api/ApiCustomer/resendOTPiOS';
	$route['api/profile'] = 'api/ApiCustomer/profile';
	$route['api/update'] = 'api/ApiCustomer/updateCustomer';
	$route['api/setRegion'] = 'api/ApiCustomer/setRegion';
	$route['api/getTimeZone'] = 'api/ApiCustomer/getTimeZone';

	$route['api/regions'] = 'api/ApiRegions/index';
	$route['api/version/latest'] = 'api/ApiVersion/latest';
	$route['api/seasonal_image'] = 'api/ApiSeasonal/index';

	/* API Calendar Events */
	$route['api/events/monthly'] = 'api/ApiCalendar/monthly';
	$route['api/events/daily'] = 'api/ApiCalendar/daily';
	$route['api/events/create'] = 'api/ApiCalendar/create';
	$route['api/events/delete'] = 'api/ApiCalendar/customer_event_delete';

	$route['api/events/monthlyNew'] = 'api/ApiCalendar/monthlyNew';
	$route['api/events/dailyNew'] = 'api/ApiCalendar/dailyNew';

	$route['api/events/getSplash'] = 'api/ApiCalendar/getSplash';
	$route['api/events/encryptionTest'] = 'api/ApiCalendar/encryptionTest';
	

	$route['api/events/test'] = 'api/ApiCalendar/test';


	/* Railway API */
	$route['api/railway/suggestTrain'] = 'api/ApiRailway/suggestTrain';
	$route['api/railway/suggestStation'] = 'api/ApiRailway/suggestStation';
	$route['api/railway/pnrStatus'] = 'api/ApiRailway/pnrStatus';
	$route['api/railway/route'] = 'api/ApiRailway/route';
	$route['api/railway/liveStatus'] = 'api/ApiRailway/liveStatus';
	$route['api/railway/trainBetweenStations'] = 'api/ApiRailway/trainBetweenStations';


	/* encrypted */
	$route['api/nearest'] = 'api/ApiNearestBanks/index';
	$route['api/nearest/ios'] = 'api/ApiNearestBanks/iOSindex';
	$route['api/distance_matrix'] = 'api/ApiNearestBanks/getUrlResult';
	//$route['api/railway'] = 'api/ApiNearestBanks/getUrlResult';

$route['translate_uri_dashes'] = FALSE;
