<?php


get_instance()->load->helper('form_helper');


 ?>
<div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Create Admin User
            <small><?php //echo $sess_full_name; ?></small>
          </h1>
          <?php if( isset($breadcrumbs) ) echo $breadcrumbs; ?>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <?php if( $this->session->flashdata('error') ){?>
            <div class="col-md-12">
           <div class="alert alert-danger alert-dismissable">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
             <?php echo $this->session->flashdata('error'); ?>
           </div>
           </div>
          <?php } ?>
          <?php if( $this->session->flashdata('success') ){?>
            <div class="col-md-12">
           <div class="alert alert-success alert-dismissable">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
             <?php echo $this->session->flashdata('success'); ?>
           </div>
           </div>
          <?php } ?>
            <div class="col-md-6">

              <!-- general form elements -->
              <div class="box box-primary">
                <div class="box-header with-border">
                  <h3 class="box-title">Admin User Details</h3>
                </div><!-- /.box-header -->
                <!-- form start -->
                <?php echo form_open_multipart(''); ?>
                  <div class="box-body">
                    <div class="row">
                      <?php
                        $others = array(
                            'id'    => 'first_name',
                            'class' => 'form-control',
                            'placeholder' => 'First Name'
                          );
                        $first_name = set_value('first_name') ? set_value('first_name') : '';
                        form_input('first_name', $first_name, $others, ['div_class' => 'col-md-6', 'required' => 'true']);
                        $others = array(
                            'id'    => 'middle_name',
                            'class' => 'form-control',
                            'placeholder' => 'Middle Name'
                          );
                        $middle_name = set_value('middle_name') ? set_value('middle_name') : '';
                        form_input('middle_name', $middle_name, $others, ['div_class' => 'col-md-6']);
                       ?>
                    </div>

                    <div class="row">
                      <?php
                        $others = array(
                            'id'    => 'last_name',
                            'class' => 'form-control',
                            'placeholder' => 'Last Name'
                          );
                        $last_name = set_value('last_name') ? set_value('last_name') : '';
                        form_input('last_name', $last_name, $others, ['div_class' => 'col-md-6']);

                        $others = array(
                            'id'    => 'email',
                            'class' => 'form-control',
                            'placeholder' => 'Email address'
                          );
                        $email = set_value('email') ? set_value('email') : '';
                        form_input('email', $email, $others, ['div_class' => 'col-md-6']);

                        $others = array(
                            'id'    => 'username',
                            'class' => 'form-control',
                            'placeholder' => 'Username'
                          );
                        $username = set_value('username') ? set_value('username') : '';
                        form_input('username', $username, $others, ['div_class' => 'col-md-6', 'required' => 'true']);

                        $others = array(
                            'id'    => 'password',
                            'class' => 'form-control',
                            'placeholder' => 'Password'
                          );
                        form_password('password', set_value('password'), $others, ['div_class' => 'col-md-6', 'required' => 'true']);

                       ?>
                    </div>
                    <div class="form-group">
                      <?php
                      echo form_label('Profile Photo', 'profile_photo');
                      echo form_upload('profile_photo', 'Profile Photo');
                      $profile_photo = set_value('profile_photo') ? set_value('profile_photo') : '';
                      ?>
                     <!--  <img src="<?php /*echo $profile_photo;*/ ?>" /> -->
                    </div>
                  </div><!-- /.box-body -->

                  <div class="box-footer">
                    <?php echo form_submit('create', 'Create', ['class' => "btn btn-primary"]); ?>
                  </div>
                </form>
              </div><!-- /.box -->



            </div>




          </div>   <!-- /.row -->
          <div class="row">

            <div class="col-md-6">

            </div><!--/.col (right) -->


          </div>   <!-- /.row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->