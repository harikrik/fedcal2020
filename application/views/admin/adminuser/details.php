
<!-- =============================================== -->

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Admin User Details
      <!-- <small>School Dashboard</small>-->
      <a href="<?php echo base_url().$url_prefix; ?>adminuser/edit/<?php echo $id; ?>" class="btn btn-primary">
        <i class="fa fa-pencil"></i>
        Edit
      </a>
    </h1>
     <?php if( isset($breadcrumbs) ) echo $breadcrumbs; ?>
  </section>

  <!-- Main content -->
  <section class="content">
      <div class="row">
          <?php if( $this->session->flashdata('error') ){?>
            <div class="col-md-12">
           <div class="alert alert-danger alert-dismissable col-md-12">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
             <?php echo $this->session->flashdata('error'); ?>
           </div>
           </div>
          <?php } ?>
          <?php if( $this->session->flashdata('success') ){?>
            <div class="col-md-12">
           <div class="alert alert-success alert-dismissable">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
             <?php echo $this->session->flashdata('success'); ?>
           </div>
           </div>
          <?php } ?>
            <!-- left column -->
            <div class="col-md-6">
              <!-- general form elements -->
              <div class="box box-primary">
                <div class="box-header with-border">
                  <h3 class="box-title">Admin User</h3>
                </div><!-- /.box-header -->
                <!-- form start -->
                <form role="form">
                  <div class="box-body">
                    <div class="form-group">
                      <label for="exampleInputEmail1">Username</label>
                      <p class="ftext-muted"><?php echo $single_data->username; ?></p>
                    </div>
                    <div class="form-group">
                      <label for="exampleInputEmail1">Email</label>
                      <p class="ftext-muted"><?php echo $single_data->email; ?></p>
                    </div>
                    <div class="form-group">
                      <label for="exampleInputEmail1"> Name</label>
                      <p class="ftext-muted"><?php echo $single_data->first_name.' '.$single_data->middle_name.' '.$single_data->last_name; ?></p>
                    </div>
                  </div>
                </form>
              </div><!-- /.box -->
            </div>
      </div>
  </section><!-- /.content -->
</div><!-- /.content-wrapper -->


