
<!-- =============================================== -->

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper adminuserclass">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Admin Users
      <a href="<?php echo base_url().$url_prefix; ?>adminuser/create" class="btn btn-primary">
        <i class="fa fa-plus"></i>
        Create New
      </a>
      <!-- <small>School Dashboard</small>-->
    </h1>
    <?php if( isset($breadcrumbs) ) echo $breadcrumbs; ?>
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="row">
      <?php if( $this->session->flashdata('error') ){?>
         <div class="col-md-12">
       <div class="alert alert-danger alert-dismissable">
        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
         <?php echo $this->session->flashdata('error'); ?>
       </div>
       </div>
      <?php } ?>
      <?php if( $this->session->flashdata('success') ){?>
         <div class="col-md-12">
       <div class="alert alert-success alert-dismissable">
        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
         <?php echo $this->session->flashdata('success'); ?>
       </div>
       </div>
      <?php } ?>
       <div class="col-xs-12">
             <div class="box">
                <div class="box-header">
                  <!-- <h3 class="box-title">Data Table With Full Features</h3> -->
                </div><!-- /.box-header -->
                <div class="box-body">
                  <table id="example1" class="table table-bordered table-striped table-hover">
                    <thead>
                      <tr>
                        <th>#</th>
                        <th>Username</th>
                        <th>First Name</th>
                        <th>Email</th>
                        <th>Actions</th>
                     </tr>
                    </thead>
                    <tbody>

                   <!--
                      <tr>
                        <td>Misc</td>
                        <td>IE Mobile</td>
                        <td>Windows Mobile 6</td>
                        <td>-</td>
                      </tr>

                      -->
                    </tbody>

                    <!-- <tfoot>
                      <tr>
                        <th>#</th>
                        <th>Subject</th>
                        <th>Actions</th>
                      </tr>
                    </tfoot> -->
                  </table>
                </div><!-- /.box-body -->
              </div><!-- /.box -->
          </div>
    </div>


  </section><!-- /.content -->
</div><!-- /.content-wrapper -->


