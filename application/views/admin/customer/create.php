
<!-- =============================================== -->

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Customer Create
      <!-- <small>School Dashboard</small>-->
    </h1>
     <?php if( isset($breadcrumbs) ) echo $breadcrumbs; ?>
  </section>

  <!-- Main content -->
  <section class="content">
      <div class="row">
          <?php if( $this->session->flashdata('error') ){?>
            <div class="col-md-12">
           <div class="alert alert-danger alert-dismissable">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
             <?php echo $this->session->flashdata('error'); ?>
           </div>
           </div>
          <?php } ?>
          <?php if( $this->session->flashdata('success') ){?>
            <div class="col-md-12">
           <div class="alert alert-success alert-dismissable">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
             <?php echo $this->session->flashdata('success'); ?>
           </div>
           </div>
          <?php } ?>
            <!-- left column -->
            <div class="col-md-6">
              <!-- general form elements -->
              <div class="box box-primary">
                <div class="box-header with-border">
                  <h3 class="box-title"> Customer</h3>
                </div><!-- /.box-header -->
                <!-- form start -->
                <?php echo form_open_multipart(''); ?>
                  <div class="box-body">
                    <?php
                    $others = array(
                        'id'    => 'name',
                        'class' => 'form-control',
                        'placeholder' => 'Name'
                      );
                    $name = set_value('name');
                    form_input('name', $name, $others, ['required' => 'true']);

                    $others = array(
                        'id'    => 'email',
                        'class' => 'form-control',
                        'placeholder' => 'Email'
                      );
                    $email = set_value('email');
                    form_input('email', $email, $others, ['required' => 'true']);

                    $others = array(
                        'id'    => 'mobile',
                        'class' => 'form-control',
                        'placeholder' => 'Mobile'
                      );
                    $mobile = set_value('mobile');
                    form_input('mobile', $mobile, $others, ['required' => 'true']);
                    ?>
                    <?php $date_class = form_error('dob') ? ' has-error' : ''; ?>
                    <div class="form-group <?php echo $date_class;?>">
                      <label>Date <span class="text-red">*</span></label>
                      <div class="input-group">
                        <div class="input-group-addon">
                          <i class="fa fa-calendar"></i>
                        </div>
                        <input type="text" class="form-control" id="datemask" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask name="dob" readonly>
                      </div><!-- /.input group -->
                    </div><!-- /.form group -->  
                    
                  </div><!-- /.box-body -->

                  <div class="box-footer">
                    <?php echo form_submit('create', 'Create', ['class' => "btn btn-primary"]); ?>
                  </div>
                </form>
              </div><!-- /.box -->
            </div>

          </div>


  </section><!-- /.content -->
</div><!-- /.content-wrapper -->


