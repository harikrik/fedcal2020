
<!-- =============================================== -->

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Customer Details
    </h1>
     <?php if( isset($breadcrumbs) ) echo $breadcrumbs; ?>
  </section>

  <!-- Main content -->
  <section class="content">
      <div class="row">
          <?php if( $this->session->flashdata('error') ){?>
            <div class="col-md-12">
           <div class="alert alert-danger alert-dismissable col-md-12">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
             <?php echo $this->session->flashdata('error'); ?>
           </div>
           </div>
          <?php } ?>
          <?php if( $this->session->flashdata('success') ){?>
            <div class="col-md-12">
           <div class="alert alert-success alert-dismissable">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
             <?php echo $this->session->flashdata('success'); ?>
           </div>
           </div>
          <?php } ?>
            <!-- left column -->
            <div class="col-md-6">
              <!-- general form elements -->
              <div class="box box-primary">
                <div class="box-header with-border">
                  <h3 class="box-title">Customer</h3>
                </div><!-- /.box-header -->
                <!-- form start -->
                <form role="form">
                  <div class="box-body">
                    <div class="form-group">
                      <label for="exampleInputEmail1">Customer Name</label>
                      <p class="ftext-muted"><?php echo $single_data->name; ?></p>
                    </div>
                    <div class="form-group">
                      <label for="exampleInputEmail1">Customer Email</label>
                      <p class="ftext-muted"><?php echo $single_data->email; ?></p>
                    </div>
                    <div class="form-group">
                      <label for="exampleInputEmail1">Customer Mobile</label>
                      <p class="ftext-muted"><?php echo $single_data->mobile; ?></p>
                    </div>
                    <div class="form-group">
                      <label for="exampleInputEmail1">Customer DOB</label>
                      <p class="ftext-muted"><?php echo $single_data->dob; ?></p>
                    </div>
                  </div>
                </form>
              </div><!-- /.box -->
            </div>
      </div>
  </section><!-- /.content -->
</div><!-- /.content-wrapper -->


