
<!-- =============================================== -->

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Region Edit
      <!-- <small>School Dashboard</small>-->
    </h1>
     <?php if( isset($breadcrumbs) ) echo $breadcrumbs; ?>
  </section>

  <!-- Main content -->
  <section class="content">
      <div class="row">
          <?php if( $this->session->flashdata('error') ){?>
            <div class="col-md-12">
           <div class="alert alert-danger alert-dismissable">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
             <?php echo $this->session->flashdata('error'); ?>
           </div>
           </div>
          <?php } ?>
          <?php if( $this->session->flashdata('success') ){?>
            <div class="col-md-12">
           <div class="alert alert-success alert-dismissable">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
             <?php echo $this->session->flashdata('success'); ?>
           </div>
           </div>
          <?php } ?>
            <!-- left column -->
            <div class="col-md-6">
              <!-- general form elements -->
              <div class="box box-primary">
                <div class="box-header with-border">
                  <h3 class="box-title">Edit Region</h3>
                </div><!-- /.box-header -->
                <!-- form start -->
                <?php echo form_open_multipart(''); ?>
                  <div class="box-body">
                    <?php
                    $others = array(
                        'id'    => 'region',
                        'class' => 'form-control',
                        'placeholder' => 'Region Name'
                      );
                    $region = set_value('region') ? set_value('region') : $single_data->region;
                    form_input('region', $region, $others, ['required' => 'true']);
                    ?>
                  </div><!-- /.box-body -->

                  <div class="box-footer">
                    <?php echo form_submit('update', 'Update', ['class' => "btn btn-primary"]); ?>
                  </div>
                </form>
              </div><!-- /.box -->
            </div>

    </div>

  </section><!-- /.content -->
</div><!-- /.content-wrapper -->


