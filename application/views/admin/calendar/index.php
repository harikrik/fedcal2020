<!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="col-md-12 content-header">
            <h1 style="float: left;">
              Calendar
              <small>Control panel</small>
            </h1>
            <div class="form-group col-md-4 <?php if (form_error('regions')){ echo ' error';}?>">
              <div class="controls">
                  <?php 
                  echo form_dropdown('regions', $regions, [], ' class="form-control regions" ');
                 ?>
                </div>
                <?php if (form_error('regions')){ ?>
                  <span class="help-block"><?php echo form_error('regions'); ?></span>
                <?php }?>
            </div>
            <?php if( isset($breadcrumbs) ) echo $breadcrumbs; ?>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-md-3">
              <div class="box box-solid">
                <div class="box-header with-border">
                  <h4 class="box-title">Draggable Event Types</h4>
                </div>
                <div class="box-body">
                  <!-- the events -->
                  <div id="external-events">
                    <?php
                    if($event_types){
                      foreach ($event_types as $event_type) {
                        echo '<div class="external-event bg-'.$event_type->backgroundColor.'">
                          <span class="event_type_text">'.$event_type->name.'</span>
                          <span class="event_type hide">'.$event_type->id.'</span>
                        </div>';
                        /*
                          <button type="button" class="close dlt_event_type" data-dismiss="alert" aria-hidden="true" event_type_id="'.$event_type->id.'">×</button>
                        */
                      }
                      ?>
                    <?php } ?>
                    <!-- <div class="checkbox">
                      <label for="drop-remove">
                        <input type="checkbox" id="drop-remove">
                        remove after drop
                      </label>
                    </div> -->
                  </div>
                </div><!-- /.box-body -->
              </div><!-- /. box -->
              <!-- 

              <div class="box box-solid hide">
                <div class="box-header with-border">
                  <h3 class="box-title">Create Event Type</h3>
                </div>
                <div class="box-body">
                  <div class="btn-group" style="width: 100%; margin-bottom: 10px;">
                    <!--<button type="button" id="color-chooser-btn" class="btn btn-info btn-block dropdown-toggle" data-toggle="dropdown">Color <span class="caret"></span></button>--
                    <ul class="fc-color-picker" id="color-chooser">
                      <li><a color="aqua" class="text-aqua" href="#"><i class="fa fa-square"></i></a></li>
                      <li><a color="blue" class="text-blue" href="#"><i class="fa fa-square"></i></a></li>
                      <li><a color="light-blue" class="text-light-blue" href="#"><i class="fa fa-square"></i></a></li>
                      <li><a color="teal" class="text-teal" href="#"><i class="fa fa-square"></i></a></li>
                      <li><a color="yellow" class="text-yellow" href="#"><i class="fa fa-square"></i></a></li>
                      <li><a color="orange" class="text-orange" href="#"><i class="fa fa-square"></i></a></li>
                      <li><a color="green" class="text-green" href="#"><i class="fa fa-square"></i></a></li>
                    <!--   <li><a color="lime" class="text-lime" href="#"><i class="fa fa-square"></i></a></li> --
                      <li><a color="red" class="text-red" href="#"><i class="fa fa-square"></i></a></li>
                      <li><a color="purple" class="text-purple" href="#"><i class="fa fa-square"></i></a></li>
                      <li><a color="fuchsia" class="text-fuchsia" href="#"><i class="fa fa-square"></i></a></li>
                      <li><a color="muted" class="text-muted" href="#"><i class="fa fa-square"></i></a></li>
                      <li><a color="navy" class="text-navy" href="#"><i class="fa fa-square"></i></a></li>
                    </ul>
                  </div><!-- /btn-group --
                  <div class="input-group">
                    <input id="new_event_type" type="text" class="form-control" placeholder="Event Title">
                    <div class="input-group-btn">
                      <button id="add_event_type" type="button" class="btn btn-primary btn-flat">Add</button>
                    </div><!-- /btn-group --
                  </div><!-- /input-group --


                </div>
              </div> 

              -->
              <div class="box box-solid">
                <div class="box-header with-border">
                  <h3 class="box-title">Drag & Delete Calendar Event</h3>
                </div>
                <div class="box-body">
                  <div class="alert alert-danger alert-dismissable" id="calendarTrash">
                      <i class="fa fa-trash"></i> Drag & Delete <br/> Calendar Event
                  </div>
                </div>
              </div>
            </div><!-- /.col -->


            <div class="col-md-9">
              <div class="box box-primary">
                <div class="box-body no-padding">
                  <!-- THE CALENDAR -->
                  <div id="calendar"></div>
                </div><!-- /.box-body -->
              </div><!-- /. box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->