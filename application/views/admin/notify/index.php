<!-- =============================================== -->

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Send Message to App Users
    </h1>
     <?php if( isset($breadcrumbs) ) echo $breadcrumbs; ?>
  </section>

  <!-- Main content -->
  <section class="content">
      <div class="row">
          <?php if( $this->session->flashdata('error') ){?>
            <div class="col-md-12">
           <div class="alert alert-danger alert-dismissable">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
             <?php echo $this->session->flashdata('error'); ?>
           </div>
           </div>
          <?php } ?>
          <?php if( $this->session->flashdata('success') ){?>
            <div class="col-md-12">
           <div class="alert alert-success alert-dismissable">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
             <?php echo $this->session->flashdata('success'); ?>
           </div>
           </div>
          <?php } ?>
            <!-- left column -->
            <div class="col-md-6">
              <!-- general form elements -->
              <div class="box box-primary">
                <!-- form start -->
                <?php echo form_open_multipart(''); ?>
                  <div class="box-body">
                    <?php

                    $others = array(
                        'id'    => 'message',
                        'class' => 'form-control',
                        'placeholder' => 'Title'
                      );
                    $message = set_value('message') ? set_value('message') : '';
                    form_input('message', $message, $others, ['required' => 'true']);
                    
                    $others = array(
                        'id'    => 'description',
                        'class' => 'form-control',
                        'placeholder' => 'Description'
                      );
                    $description = set_value('description') ? set_value('description') : '';
                    form_textarea('description', $description, $others);

                    ?>   
                    <div class="form-group">
                      <?php
                      echo form_label('Image', 'image');
                      echo form_upload('image', 'Image');
                      $image = set_value('image') ? set_value('image') : '';
                      ?>
                     <!--  <img src="<?php /*echo $profile_photo;*/ ?>" /> -->
                    </div>

                    <div class="form-group">
                        <label for="selectType">Select User Type <span class="text-red">*</span></label>
                        <?php echo form_dropdown('type', array("all" => "All Users", "android" => "Android Users", "ios" => "iOS Users"),array("all"), ' class="form-control"') ?>
                    </div>

                  </div><!-- /.box-body -->

                  <div class="box-footer">
                    <?php echo form_submit('send', 'Send', ['class' => "btn btn-primary"]); ?>
                  </div>
                </form>
              </div><!-- /.box -->
            </div>

    </div>

  </section><!-- /.content -->
</div><!-- /.content-wrapper -->


