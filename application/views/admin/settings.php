<?php


get_instance()->load->helper('form_helper');


 ?>
<div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            User Settings
            <small><?php //echo $sess_full_name; ?></small>
          </h1>
          <?php if( isset($breadcrumbs) ) echo $breadcrumbs; ?>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <?php if( $this->session->flashdata('error') ){?>
            <div class="col-md-12">
           <div class="alert alert-danger alert-dismissable">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
             <?php echo $this->session->flashdata('error'); ?>
           </div>
           </div>
          <?php } ?>
          <?php if( $this->session->flashdata('success') ){?>
            <div class="col-md-12">
           <div class="alert alert-success alert-dismissable">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
             <?php echo $this->session->flashdata('success'); ?>
           </div>
           </div>
          <?php } ?>
            <div class="col-md-6">

              <!-- general form elements -->
              <div class="box box-primary">
                <div class="box-header with-border">
                  <h3 class="box-title">User Details</h3>
                </div><!-- /.box-header -->
                <!-- form start -->
                <?php echo form_open_multipart(''); ?>
                  <div class="box-body">
                    <div class="row">
                      <?php
                        $others = array(
                            'id'    => 'first_name',
                            'class' => 'form-control',
                            'placeholder' => 'First Name'
                          );
                        $first_name = set_value('first_name') ? set_value('first_name') : $userSettings->first_name;
                        form_input('first_name', $first_name, $others, ['div_class' => 'col-md-6', 'required' => 'true']);
                        $others = array(
                            'id'    => 'middle_name',
                            'class' => 'form-control',
                            'placeholder' => 'Middle Name'
                          );
                        $middle_name = set_value('middle_name') ? set_value('middle_name') : $userSettings->middle_name;
                        form_input('middle_name', $middle_name, $others, ['div_class' => 'col-md-6']);
                       ?>
                    </div>

                    <div class="row">
                      <?php
                        $others = array(
                            'id'    => 'last_name',
                            'class' => 'form-control',
                            'placeholder' => 'Last Name'
                          );
                        $last_name = set_value('last_name') ? set_value('last_name') : $userSettings->last_name;
                        form_input('last_name', $last_name, $others, ['div_class' => 'col-md-6']);

                        $others = array(
                            'id'    => 'email',
                            'class' => 'form-control',
                            'placeholder' => 'Email address'
                          );
                        $email = set_value('email') ? set_value('email') : $userSettings->email;
                        form_input('email', $email, $others, ['div_class' => 'col-md-6']);
                       ?>
                    </div>
                    <div class="form-group">
                      <?php
                      echo form_label('Profile Photo', 'profile_photo');
                      echo form_upload('profile_photo', 'Profile Photo');
                      $profile_photo = set_value('profile_photo') ? set_value('profile_photo') : $userSettings->profile_photo;
                      ?>
                     <!--  <img src="<?php /*echo $profile_photo;*/ ?>" /> -->
                    </div>
                  </div><!-- /.box-body -->

                  <div class="box-footer">
                    <?php echo form_submit('update_settings', 'Update', ['class' => "btn btn-primary"]); ?>
                  </div>
                </form>
              </div><!-- /.box -->



            </div>

            <div class="col-md-6">

              <!-- general form elements -->
              <div class="box box-primary">
                <div class="box-header with-border">
                  <h3 class="box-title">Change Password</h3>
                </div><!-- /.box-header -->
                <!-- form start -->
                <?php echo form_open(''); ?>
                  <div class="box-body">
                        <?php
                        $others = array(
                            'id'    => 'curr_password',
                            'class' => 'form-control',
                            'placeholder' => 'Existing Password'
                          );
                        form_password('curr_password', set_value('curr_password'), $others, ['required' => 'true']);
                        $others = array(
                            'id'    => 'password',
                            'class' => 'form-control',
                            'placeholder' => 'New Password'
                          );
                        form_password('password', set_value('password'), $others, ['required' => 'true']);
                        $others = array(
                            'id'    => 'cpassword',
                            'class' => 'form-control',
                            'placeholder' => 'Confirm New Password'
                          );
                        form_password('cpassword', set_value('cpassword'), $others, ['required' => 'true']); ?>
                  </div><!-- /.box-body -->
                  <div class="box-footer">
                    <?php echo form_submit('doChangePassword', 'Change Password', ['class' => "btn btn-primary"]); ?>
                  </div>
                </form>
              </div><!-- /.box -->

            </div>


          </div>   <!-- /.row -->
          <div class="row">

            <div class="col-md-6">

            </div><!--/.col (right) -->


          </div>   <!-- /.row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->