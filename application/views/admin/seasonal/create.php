<?php


get_instance()->load->helper('form_helper');


 ?>
<div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Create Seasonal Image
            <small><?php //echo $sess_full_name; ?></small>
          </h1>
          <?php if( isset($breadcrumbs) ) echo $breadcrumbs; ?>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <?php if( $this->session->flashdata('error') ){?>
            <div class="col-md-12">
           <div class="alert alert-danger alert-dismissable">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
             <?php echo $this->session->flashdata('error'); ?>
           </div>
           </div>
          <?php } ?>
          <?php if( $this->session->flashdata('success') ){?>
            <div class="col-md-12">
           <div class="alert alert-success alert-dismissable">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
             <?php echo $this->session->flashdata('success'); ?>
           </div>
           </div>
          <?php } ?>
            <div class="col-md-6">

              <!-- general form elements -->
              <div class="box box-primary">
                <div class="box-header with-border">
                  <h3 class="box-title">Seasonal Image</h3>
                </div><!-- /.box-header -->
                <!-- form start -->
                <?php echo form_open_multipart(''); ?>
                  <div class="box-body">
                    <div class="form-group">
                      <?php
                      echo form_label('Seasonal Image', 'image');
                      echo form_upload('image', 'Seasonal Image');
                      $image = set_value('image') ? set_value('image') : '';
                      ?>
                     <!--  <img src="<?php /*echo $profile_photo;*/ ?>" /> -->
                    </div>
                  </div><!-- /.box-body -->

                  <div class="box-footer">
                    <?php echo form_submit('create', 'Create', ['class' => "btn btn-primary"]); ?>
                  </div>
                </form>
              </div><!-- /.box -->



            </div>




          </div>   <!-- /.row -->
          <div class="row">

            <div class="col-md-6">

            </div><!--/.col (right) -->


          </div>   <!-- /.row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->