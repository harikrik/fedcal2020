<!-- Left side column. contains the sidebar -->
<aside class="main-sidebar">
  <!-- sidebar: style can be found in sidebar.less -->
  <section class="sidebar">
    <!-- Sidebar user panel -->
    <div class="user-panel">
      <div class="pull-left image">
        <img src="<?php echo base_url().$sess_profile_pic; ?>" class="img-circle hide" alt="User Image">
      </div>
      <div class="pull-left info">
        <p>
          Calendar App
          <?php //echo $sess_full_name; ?></p>
          <!-- Admin -->
          <?php echo $sess_usertype; ?>
      </div>
    </div>
    <!-- search form -->
    <!-- <form action="#" method="get" class="sidebar-form">
      <div class="input-group">
        <input type="text" name="q" class="form-control" placeholder="Search..."  autocomplete="off" id="sidebar_search">
        <span class="input-group-btn">
          <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i></button>
        </span>
      </div>
    </form> -->
    <!-- /.search form -->
    <!-- sidebar menu: : style can be found in sidebar.less -->
    <ul class="sidebar-menu">
      <li class="header">MAIN NAVIGATION</li>
      <li class="nav_item <?php echo $active_link == 'calendar' ? 'active' : ''; ?>">
        <a href="<?php echo base_url().$url_prefix; ?>calendar">
          <i class="fa fa-calendar"></i> <span>Calendar</span>
        </a>
      </li>

      <?php if( $sess_usertype == SUPERADMIN ){ ?>

        

      <li class="nav_item <?php echo $active_link == 'adminuser' ? 'active' : ''; ?>">
        <a href="#">
          <i class="fa fa-male"></i> <span>Admin Users</span>
          <i class="fa fa-angle-left pull-right"></i>
        </a>
        <ul class="treeview-menu">
          <li>
             <a href="<?php echo base_url().$url_prefix; ?>adminuser">
             <i class="fa fa-list"></i> List</a>
          </li>
          <li>
              <a href="<?php echo base_url().$url_prefix; ?>adminuser/create">
              <i class="fa fa-plus"></i> Create</a>
          </li>
        </ul>
      </li>

      <?php } ?>

      <li class="nav_item <?php echo $active_link == 'appuser' ? 'active' : ''; ?>">
        <a href="<?php echo base_url().$url_prefix; ?>appuser">
          <i class="fa fa-male"></i> <span>App Users</span>
        </a>
      </li>

      <li class="nav_item <?php echo $active_link == 'notify' ? 'active' : ''; ?>">
        <a href="<?php echo base_url().$url_prefix; ?>notify">
          <i class="fa fa-male"></i> <span>Bulk Notifications</span>
        </a>
      </li>

      <li class="nav_item <?php echo $active_link == 'version' ? 'active' : ''; ?>">
        <a href="<?php echo base_url().$url_prefix; ?>version">
          <i class="fa fa-list"></i> <span>App Versions</span>
        </a>
      </li>

      <li class="nav_item <?php echo $active_link == 'seasonal' ? 'active' : ''; ?>">
        <a href="<?php echo base_url().$url_prefix; ?>seasonal">
          <i class="fa fa-image"></i> <span>Seasonal Image</span>
        </a>
      </li>


    </ul>
  </section>
  <!-- /.sidebar -->
</aside>