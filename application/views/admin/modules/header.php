<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title><?php echo $title; ?> - FEDERAL BANK</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

    <?php include(A_CSS.'css.php'); ?>
     <style>
.fc-title{font-weight: bold;}
 @media
  only screen and (max-width: 760px),
  (min-device-width: 768px) and (max-device-width: 1024px)  {

    /* Force table to not be like tables anymore */
    table, thead, tbody, th, td, tr {
      display: block;
    }

    /* Hide table headers (but not display: none;, for accessibility) */
    thead tr {
      position: absolute;
      top: -9999px;
      left: -9999px;
    }

    tr { border: 1px solid #ccc; }

    td {
      /* Behave  like a "row" */
      border: none;
      border-bottom: 1px solid #eee;
      position: relative;
      padding-left: 65%;
    }

    td:before {
      /* Now like a table header */
      position: absolute;
      /* Top/left values mimic padding */
      top: 6px;
      left: 6px;
      width: 30%;
      padding-right: 10px;
      white-space: nowrap; text-align: left;font-size: 12px;
    }

    /*
    Label the data
    */
 

}

  /* Smartphones (portrait and landscape) ----------- */
@media only screen
  and (min-device-width : 320px)
  and (max-device-width : 480px) {
    body {
      padding: 0;
      margin: 0;
      width:100%!important; }
    }

  /* iPads (portrait and landscape) ----------- */
@media only screen and (min-device-width: 768px) and (max-device-width: 1024px) {
    body {
      width: 495px;
    }
  }

media only screen and (max-width: 760px), (max-device-width: 1024px) and (min-device-width: 768px)
td:before {
    position: absolute;
    top: 6px;
    left: 6px;
    width: 30%;
    padding-right: 10px;
    white-space: nowrap;
    text-align: left;
}
  </style>
  </head>
  <body class="hold-transition skin-blue sidebar-mini">
    <!-- Site wrapper -->
    <div class="wrapper">

      <header class="main-header">
        <a href="<?php echo base_url(); ?>" class="logo">
          <span class="logo-mini"><b>F</b>B</span>
          <span class="logo-lg"><b><i>FEDERAL BANK</i></b></span>
        </a>

        <nav class="navbar navbar-static-top" role="navigation">
          <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </a>
          <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">

              <li class="dropdown user user-menu">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Admin
                  <img src="<?php echo base_url().$sess_profile_pic; ?>" class="user-image hide" alt="User Image">
                </a>
                <ul class="dropdown-menu">

                  <li class="user-header" style="height: 85px;">
                    <img src="<?php echo base_url().$sess_profile_pic; ?>" class="img-circle hide" alt="User Image">
                    <p>
                      Calendar App
                      <?php //echo $sess_full_name; ?>
                      <small> <?php echo $sess_usertype; ?> </small>
                    </p>
                  </li>

                  <li class="user-footer">
                    <div class="pull-left">
                      <a href="<?php echo base_url().$url_prefix; ?>settings" class="btn btn-default btn-flat">Settings</a>
                    </div>
                    <div class="pull-right">
                      <a href="<?php echo base_url().$url_prefix; ?>logout" class="btn btn-default btn-flat">Sign out</a>
                    </div>
                  </li>
                </ul>
              </li>

            </ul>
          </div>
        </nav>
      </header>