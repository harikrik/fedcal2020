<footer class="main-footer">
        <div class="pull-right hidden-xs">
          <b>Version</b> 0.1.0
        </div>
        <strong>Copyright &copy; <?php echo date('Y'); ?> <a href="#">FEDERAL BANK</a>.</strong> All rights reserved.
      </footer>

      <?php include('control_sidebar.php'); ?>
    </div><!-- ./wrapper -->
    <?php include(A_JS.'js.php'); ?>
  </body>
</html>
