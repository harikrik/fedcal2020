<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
usertypes
+++++++++
1 = superadmin


*/

class MY_Controller extends CI_Controller{

	/* index.php?/ */
	public $url_prefix = '';

	public $imgType_a = array(
        'image/gif', 'image/jpeg', 'image/jpg', 'image/png'
        );

	public function __construct()
	{
        parent::__construct();
	}

	public function jsonInput(){
		$json_obj = json_decode('');
        if (isset($GLOBALS["HTTP_RAW_POST_DATA"]) && !empty($GLOBALS["HTTP_RAW_POST_DATA"])) {
            $json_text = $this->cleanMe($GLOBALS["HTTP_RAW_POST_DATA"]);
            // now insert into user table
            $json_obj = json_decode($json_text);
        }
        return $json_obj;
    }

    private function cleanMe($input)
    {
        $input = htmlspecialchars($input, ENT_IGNORE, 'utf-8');
        $input = strip_tags($input);
        $input = stripslashes($input);
        return $input;
    }

	public function jsonOutput($data = array() ){
		$this->output->set_content_type('application/json');
		echo json_encode($data);
		exit;
	}

	public function saveBase64($path_n_img_name, $base64_string){
		$img_str = base64_decode(preg_replace('#^data:image/\w+;base64,#i', '', $base64_string));
		file_put_contents($path_n_img_name, $data);
	}

	public function	dd($var=""){
		echo "<pre>";
		print_r($var);
		echo "</pre>";
		die();
	}

	public function breadcrumbs($links){
		$breadcrumb = '';
		$breadcrumb_li = '';
		$base = base_url();
		foreach( $links as $text=>$link ){
			$link = trim( $link );
			if( $link ){
				$breadcrumb_li .= "<li >
			        <a href='$base$this->url_prefix$link'>
			          $text
			        </a>
			      </li>";
			}
			else{
				$breadcrumb_li .= "<li class='active'>
			          $text
			      </li>";
			}

		}
		if( $breadcrumb_li ){
			$breadcrumb = "<ol class='breadcrumb'>$breadcrumb_li</ol>";
		}
		return $breadcrumb;

	}

	public function local_redirect($url){
		header('Location:' . base_url().$this->url_prefix . $url, TRUE);
		// redirect($url);
		exit;
	}

	public function loginCheck(){
		$logged_in = false;
		if (($this->session->userdata('logged_in'))) {
            /*$this->local_redirect($red_url);*/
            $logged_in = true;
        }
        /* else if ($this->input->cookie('cookie_user_id')) {
        	$this->load->model('User_model');
        	$cookieLogin = $this->User_model->cookieLogin(get_cookie('cookie_user_id'));
            if( !array_key_exists( 'error', $cookieLogin ) ){
	        	$this->setSession($cookieLogin);
	        	$logged_in = true;
	        }
        } */
        return $logged_in;
	}

	public function setSession($login){
		/*$this->session->sess_destroy();*/


		$this->logoutUsers();
		$userdata = array(
			'username'    => $login->username,
			'usertype'    => $login->usertype,
			'full_name'   => '',
			'profile_pic' => '',
			'uid'         => $login->user_id,
			'logged_in'   => TRUE
        );
        $this->session->set_userdata($userdata);

        /*print_r($login);
        print_r($this->session);
        exit;*/
        /*$this->local_redirect($red_url);*/
	}

	public function logoutUsers(){
		$userdata = array(
			'username'    => '',
			'usertype'    => '',
			'full_name'   => '',
			'profile_pic' => '',
			'uid'         => '',
			'logged_in'   => FALSE,
        );
		$this->session->set_userdata($userdata);
		/*$this->session->sess_destroy();*/
	}

	public function display($data, $return = FALSE){

		if(!isset($data['page_location'])) {
			$page_path = 'admin/';
		} else {
			$page_path = $data['page_location'].'/';
		}

		if($this->session->userdata('logged_in')){
			$data['sess_username']    = $this->session->userdata('username');
			$data['sess_usertype']    = $this->session->userdata('usertype');
			$data['sess_full_name']   = $this->session->userdata('full_name');
			$data['sess_profile_pic'] = $this->session->userdata('profile_pic');
			$data['sess_uid']         = $this->session->userdata('uid');
			$data['sess_logged_in']   = $this->session->userdata('logged_in');
		}
		$data['active_link'] = $this->uri->segment(1);
		$data['url_prefix']   = $this->url_prefix;
		$header = $this->load->view('admin/modules/header', $data, TRUE);
		$sidebar = $this->load->view('admin/modules/sidebar', $data, TRUE);
		$footer = $this->load->view('admin/modules/footer', $data, TRUE);

		$body = $this->load->view($page_path.$data['page_name'], $data, TRUE);

		$output = $header . $sidebar . $body . $footer;

		if($return)
			return $output;
		else
			echo $output;

	}

	public function sendOTPOld($message, $mobile, $type = "Individual"){
        $username = "fedcal";
        $mypassword = 'E16$marilda';
        $sendername = "FEDCAL";
        $domain = "www.smsgatewaycenter.com/library/";
        //API Domain
        $type = "Individual";
        //Individual/Bulk/Group
        $lang = "English";
        //English/Other - Default is English
        $method = "POST";

        //---------------------------------
        //sanitize inputs
        $username = urlencode($username);
        $mypassword = urlencode($mypassword);
        $sendername = urlencode($sendername);
        $message = urlencode($message);

        $parameters = "UserName=".$username."&Password=".$mypassword."&Type=".$type."&To=".$mobile."&Mask=".$sendername."&Message=".$message."&Language=".$lang;
        $apiurl = "https://".$domain."/send_sms_2.php";
        $ch = curl_init($apiurl);

        if($method == "POST"){
            curl_setopt($ch, CURLOPT_POST,1);
            curl_setopt($ch, CURLOPT_POSTFIELDS,$parameters);
        } else {
            $get_url = $apiurl."?".$parameters;
            curl_setopt($ch, CURLOPT_POST,0);
            curl_setopt($ch, CURLOPT_URL, $get_url);
        }

        curl_setopt($ch, CURLOPT_FOLLOWLOCATION,1);
        curl_setopt($ch, CURLOPT_HEADER,0);
        // DO NOT RETURN HTTP HEADERS
        curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
        // RETURN THE CONTENTS OF THE CALL
        $return_val = curl_exec($ch);
    }

		public function sendOTP($message, $mob, $type = "Individual"){

			$curl = curl_init();
					//$apikey = 'somerandomkey';//if you use apikey then userid and password is not required
					$userId = 'fedcal';
					$password = 'E16$marilda';
					$sendMethod = 'simpleMsg'; //(simpleMsg|groupMsg|excelMsg)
					$messageType = 'text'; //(text|unicode|flash)
					$senderId = 'FEDOTP';
					//$senderId = 'FEDCAL';
					
					$mobile = $mob;//comma separated
					$msg = $message;
					$scheduleTime = '';//mention time if you want to schedule else leave blank

					curl_setopt_array($curl, array(
					  CURLOPT_URL => "http://www.smsgateway.center/SMSApi/rest/send",
					  CURLOPT_RETURNTRANSFER => true,
					  CURLOPT_ENCODING => "",
					  CURLOPT_MAXREDIRS => 10,
					  CURLOPT_TIMEOUT => 30,
					  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
					  CURLOPT_CUSTOMREQUEST => "POST",
					  CURLOPT_POSTFIELDS => "userId=$userId&password=$password&senderId=$senderId&sendMethod=$sendMethod&msgType=$messageType&mobile=$mobile&msg=$msg&duplicateCheck=true&format=json",
					  CURLOPT_HTTPHEADER => array(
					    "apikey: $apikey",
					    "cache-control: no-cache",
					    "content-type: application/x-www-form-urlencoded"
					  ),
					));

					$response = curl_exec($curl);
					print_r($response);
					$err = curl_error($curl);

					curl_close($curl);
					$return_val = $response;

			}




	public function sendIntlOTPOld($message, $mobile, $type = "Individual"){
        $username = "fedcalintlsms";
        $mypassword = 'E16$marilda';
        $sendername = "FEDCAL";
        $domain = "www.smsgatewaycenter.com/library/";
        //API Domain
        $type = "Individual";
        //Individual/Bulk/Group
        $lang = "English";
        //English/Other - Default is English
        $method = "POST";

        //---------------------------------
        //sanitize inputs
        $username = urlencode($username);
        $mypassword = urlencode($mypassword);
        $sendername = urlencode($sendername);
        $message = urlencode($message);

        $parameters = "UserName=".$username."&Password=".$mypassword."&Type=".$type."&To=".$mobile."&Mask=".$sendername."&Message=".$message."&Language=".$lang;
        $apiurl = "https://".$domain."/send_sms_2.php";
        $ch = curl_init($apiurl);

        if($method == "POST"){
            curl_setopt($ch, CURLOPT_POST,1);
            curl_setopt($ch, CURLOPT_POSTFIELDS,$parameters);
        } else {
            $get_url = $apiurl."?".$parameters;
            curl_setopt($ch, CURLOPT_POST,0);
            curl_setopt($ch, CURLOPT_URL, $get_url);
        }

        curl_setopt($ch, CURLOPT_FOLLOWLOCATION,1);
        curl_setopt($ch, CURLOPT_HEADER,0);
        // DO NOT RETURN HTTP HEADERS
        curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
        // RETURN THE CONTENTS OF THE CALL
        $return_val = curl_exec($ch);
    }

		public function sendIntlOTP($message, $mob, $type = "Individual"){

			$curl = curl_init();
					//$apikey = 'somerandomkey';//if you use apikey then userid and password is not required
					$userId = 'fedcalintlsms';
					$password = 'E16$marilda';
					$sendMethod = 'simpleMsg'; //(simpleMsg|groupMsg|excelMsg)
					$messageType = 'text'; //(text|unicode|flash)
					$senderId = 'FEDCAL';
					$mobile = $mob;//comma separated
					$msg = $message;
					$scheduleTime = '';//mention time if you want to schedule else leave blank

					curl_setopt_array($curl, array(
					  CURLOPT_URL => "http://www.smsgateway.center/SMSApi/rest/send",
					  CURLOPT_RETURNTRANSFER => true,
					  CURLOPT_ENCODING => "",
					  CURLOPT_MAXREDIRS => 10,
					  CURLOPT_TIMEOUT => 30,
					  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
					  CURLOPT_CUSTOMREQUEST => "POST",
					  CURLOPT_POSTFIELDS => "userId=$userId&password=$password&senderId=$senderId&sendMethod=$sendMethod&msgType=$messageType&mobile=$mobile&msg=$msg&duplicateCheck=true&format=json",
					  CURLOPT_HTTPHEADER => array(
					    "apikey: $apikey",
					    "cache-control: no-cache",
					    "content-type: application/x-www-form-urlencoded"
					  ),
					));

					$response = curl_exec($curl);
					$err = curl_error($curl);

					curl_close($curl);
					$return_val = $response;

			}

	public function sendPushNotif($registrationIds, $message, $time, $type, $extra_data = [], $device_os = 'android'){
    	$msg = array
        (
            'alert'   => $message,
            'body'   => $message,
            'message'   => $message,
            'time'   => $time,
            'title'     => 'Federal Bank - Calendar',
            'subtitle'  => '',
            'type'  => $type,
            'tickerText'    => $message,
            'vibrate'   => 1,
            'sound'     => 'default',
            'largeIcon' => 'large_icon',
            'smallIcon' => 'small_icon',
            'click_action' => 'DRAWER_ACTIVITY',
        );

        $msg = array_merge($msg, $extra_data);

        $fields = array
        (
			'registration_ids' => $registrationIds,
            'priority'         => 'high',
			'data'             => $msg
        );
        if( $device_os == 'ios' ){
        	$fields = array_merge( $fields, array( 'notification' => $msg ) );
        }
        $headers = array
        (
            'Authorization: key=' . API_ACCESS_KEY,
            'Content-Type: application/json'
        );
        $ch = curl_init();
        curl_setopt( $ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send' );
        curl_setopt( $ch,CURLOPT_POST, true );
        curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
        curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
        curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
        curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
        $result = curl_exec($ch );
        curl_close( $ch );
    }

	public function our_filtering($aColumns, $inputArray){
		/*
	  * Filtering
	  * NOTE this does not match the built-in DataTables filtering which does it
	  * word by word on any field. It's possible to do here, but concerned about efficiency
	  * on very large tables, and MySQL's regex functionality is very limited
	  */
	  $sWhere = "";
	  if( isset( $inputArray[ 'sSearch' ] ) && $inputArray[ 'sSearch' ] != "" ){
	    $sWhere = " AND (";
	    for( $i = 0; $i < count( $aColumns ); $i++ ){
	    	/* just to make sure there is any multiple columns in a single param */
	    	$column_a = explode(',', $aColumns[ $i ]);
			foreach( $column_a as $column ){
				$column = trim( $column );
				if( $column ){
					$sWhere .= "" . $column . " LIKE '%" . $inputArray[ 'sSearch' ] . "%' OR ";
				}
			}
	    }
	    $sWhere = substr_replace( $sWhere, "", -3 );
	    $sWhere .= ')';
	  }

	  /*
	   * Paging
	   */
	  $sLimit = "";
	  if( isset( $inputArray[ 'iDisplayStart' ] ) && $inputArray[ 'iDisplayLength' ] != '-1' ){
	    $sLimit = "LIMIT " . intval( $inputArray[ 'iDisplayStart' ] ) . ", " .
	      intval( $inputArray[ 'iDisplayLength' ] );
	  }

	  /*
	  * Ordering
	  */
	  $sOrder = "";
	  if( isset( $inputArray[ 'iSortCol_0' ] ) ){
	    $sOrder = "ORDER BY  ";
	    for( $i = 0; $i < intval( $inputArray[ 'iSortingCols' ] ); $i++ ){
	      if( $inputArray[ 'bSortable_' . intval( $inputArray[ 'iSortCol_' . $i ] ) ] == "true" ){
	        $sOrder .= "" . $aColumns[ intval( $inputArray[ 'iSortCol_' . $i ] ) ] . " " .
	          ( $inputArray[ 'sSortDir_' . $i ] === 'asc' ? 'asc' : 'desc' ) . ", ";
	      }
	    }
	    $sOrder = substr_replace( $sOrder, "", -2 );
	    if( $sOrder == "ORDER BY" ){
	      $sOrder = "";
	    }
	  }

	  /* Individual column filtering */
	  for( $i = 0; $i < count( $aColumns ); $i++ ){
	    if( isset( $inputArray[ 'bSearchable_' . $i ] ) && $inputArray[ 'bSearchable_' . $i ] == "true" && $inputArray[ 'sSearch_' . $i ] != '' ){
	      $sWhere .= " AND " . $aColumns[ $i ] . " LIKE '%" . $inputArray[ 'sSearch_' . $i ] . "%' ";
	    }
	  }

	  return array(
	  	'sWhere' => $sWhere,
	  	'sLimit' => $sLimit,
	  	'sOrder' => $sOrder
	  	);
	}

	public function do_upload($upload_path, $form_file_name, $upload_config = [])
    {
        $config['upload_path']          = $upload_path;
        $config['allowed_types']        = 'gif|jpg|png|jpeg';
        $config['max_size']             = 0;
        $config['max_width']            = 0;
        $config['max_height']           = 0;

        $this->load->library('upload', $config);

        if ( ! $this->upload->do_upload($form_file_name))
        {
            $data['upload_error'] = $this->upload->display_errors();
            return $data;
        }
        else
        {

        	$big_thumb_w = isset( $upload_config['big_thumb_w'] ) ? $upload_config['big_thumb_w'] : 800;
        	$big_thumb_h = isset( $upload_config['big_thumb_h'] ) ? $upload_config['big_thumb_h'] : 800;

        	$small_thumb_w = isset( $upload_config['small_thumb_w'] ) ? $upload_config['small_thumb_w'] : 200;
        	$small_thumb_h = isset( $upload_config['small_thumb_h'] ) ? $upload_config['small_thumb_h'] : 200;

        	$upload_data = $this->upload->data();

        	$file_name = $upload_data['raw_name'].$upload_data['file_ext'];

        	$big_thumb = $upload_path.$file_name;
        	$small_thumb = $upload_path.'thumbs/'.$file_name;

        	$this->load->library('image_lib');

        	$config['image_library'] = 'gd2';
			$config['source_image'] = $big_thumb;
			$config['create_thumb'] = FALSE;
			$config['maintain_ratio'] = TRUE;
			$config['width']         = $big_thumb_w;
			$config['height']       = $big_thumb_h;

			$this->image_lib->clear();
		    $this->image_lib->initialize($config);
			if ( ! $this->image_lib->resize())
			{
				$this->dd( $this->image_lib->display_errors() );
		        return array( 'error' => $this->image_lib->display_errors());

			}
			else{
				copy($big_thumb, $small_thumb);

				$config['image_library'] = 'gd2';
				$config['source_image'] = $small_thumb;
				$config['create_thumb'] = FALSE;
				$config['maintain_ratio'] = TRUE;
				$config['width']         = $small_thumb_w;
				$config['height']       = $small_thumb_h;

				$this->image_lib->clear();
			    $this->image_lib->initialize($config);
			    if ( ! $this->image_lib->resize())
				{
			        return array( 'error' => $this->image_lib->display_errors());
			        unset($big_thumb);
				}
				else{
					return array( 'pic' => $file_name);
				}
			}
        }
    }

    public function validateDate($date, $format = 'Y-m-d H:i:s')
	{
		/* http://php.net/manual/en/function.checkdate.php */
	    $d = DateTime::createFromFormat($format, $date);
	    return $d && $d->format($format) == $date;
	}

	public function generateRandomString($length = 10) {
	    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
	    $charactersLength = strlen($characters);
	    $randomString = '';
	    for ($i = 0; $i < $length; $i++) {
	        $randomString .= $characters[rand(0, $charactersLength - 1)];
	    }
	    return $randomString;
	}

    public function read_excel($filename){
        $file = UPLOADS.'excel/'.$filename;
        $this->load->library('excel');
        //read file from path
        $objPHPExcel = PHPExcel_IOFactory::load($file);

        //get only the Cell Collection
        $cell_collection = $objPHPExcel->getActiveSheet()->getCellCollection();
        //print_r($cell_collection);exit;
        //extract to a PHP readable array format
        $header =array();
        $column_name_a = array();
        foreach ($cell_collection as $cell) {
            $column = $objPHPExcel->getActiveSheet()->getCell($cell)->getColumn();
            $row = $objPHPExcel->getActiveSheet()->getCell($cell)->getRow();
            $data_value = $objPHPExcel->getActiveSheet()->getCell($cell)->getValue();

            //header will/should be in row 1 only. of course this can be modified to suit your need.

            $data_value = htmlspecialchars($data_value);

            if ($row == 1) {
            	$column_name_a[] = trim($data_value);
            }

            if ($row > 1) {
                $data_value = htmlspecialchars($data_value);
                $data_value = trim($data_value);
                switch($column){
                    case 'A': /*  */
                        $data[$row][$column_name_a[0]] = $data_value;
                        break;
                    case 'B': /*  */
                        $data[$row][$column_name_a[1]] = $data_value;
                        break;
                    case 'C': /*  */
                        $data[$row][$column_name_a[2]] = $data_value;
                        break;
                    case 'D': /*  */
                        $data[$row][$column_name_a[3]] = $data_value;
                        break;
                    case 'E': /*  */
                        $data[$row][$column_name_a[4]] = $data_value;
                        break;
                    case 'F': /*  */
                        $data[$row][$column_name_a[5]] = $data_value;
                        break;
                    case 'G': /*  */
                        $data[$row][$column_name_a[6]] = $data_value;
                        break;
                    case 'H': /*  */
                        $data[$row][$column_name_a[7]] = $data_value;
                        break;
                    case 'I': /*  */
                        $data[$row][$column_name_a[8]] = $data_value;
                        break;
                    case 'J': /*  */
                        $data[$row][$column_name_a[9]] = $data_value;
                        break;
                    case 'K': /*  */
                        $data[$row][$column_name_a[10]] = $data_value;
                        break;
                    case 'L': /*  */
                        $data[$row][$column_name_a[11]] = $data_value;
                        break;
                    case 'M': /*  */
                        $data[$row][$column_name_a[12]] = $data_value;
                        break;
                    case 'N': /*  */
                        $data[$row][$column_name_a[13]] = $data_value;
                        break;
                    case 'O': /*  */
                        $data[$row][$column_name_a[14]] = $data_value;
                        break;
                    case 'P': /*  */
                        $data[$row][$column_name_a[15]] = $data_value;
                        break;
                    case 'Q': /*  */
                        $data[$row][$column_name_a[16]] = $data_value;
                        break;
                    case 'R': /*  */
                        $data[$row][$column_name_a[17]] = $data_value;
                        break;
                    case 'S': /*  */
                        $data[$row][$column_name_a[18]] = $data_value;
                        break;
                    case 'T': /*  */
                        $data[$row][$column_name_a[19]] = $data_value;
                        break;
                    case 'U': /*  */
                        $data[$row][$column_name_a[20]] = $data_value;
                        break;
                    case 'V': /*  */
                        $data[$row][$column_name_a[21]] = $data_value;
                        break;
                    case 'W': /*  */
                        $data[$row][$column_name_a[22]] = $data_value;
                        break;
                    case 'x': /*  */
                        $data[$row][$column_name_a[23]] = $data_value;
                        break;
                    case 'y': /*  */
                        $data[$row][$column_name_a[24]] = $data_value;
                        break;
                    case 'z': /*  */
                        $data[$row][$column_name_a[25]] = $data_value;
                        break;
                }
            }
        }
        $excel_fields = array(
        	'column_names' => $column_name_a,
        	'column_data' => $data
        	);
        @unlink($file);
        return $excel_fields;
    }


}

/* End of file EIS_Controller.php */
/* Location: ./application/core/EIS_Controller.php */
