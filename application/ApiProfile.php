<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ApiProfile extends MY_Controller {

    private $key = "Z9WjU1M6HDOUY.Qb";
	public function __construct()
	{
		parent::__construct();
		$this->jsonInput();
		$this->load->model('User_model');
		$this->load->model('AppHome_model');
		$this->load->model('Common_model');
	}

	public function profileAndroid(){
		$query_e    = isset($_POST['query']) ? $_POST['query'] : '';
		include 'application/libraries/MCrypt.php';
		$encryption = new MCrypt();
		$inputText = $query_e;
		$query1 = $encryption->decrypt($inputText);
		$data = $this->profile($query1);
		$this->encryptedOutput($data, $encryption);
	}

	public function profileiOS(){
		$query_e    = isset($_POST['query']) ? $_POST['query'] : '';
		$inputText = $query_e;
		$key = $this->key;
		$query1 = $this->iOSaesDecrypt($inputText, $key);
		$data = $this->profile($query1);
		$this->iOSencryptedOutput($data);
	}

	private function profile($query){
		$query     = json_decode($query);

		if(!$query){
			return array( "status" => "error", "message" => "query is blank.", "query_e" => $query_e, "query" => $query1);
		}

		if( !count($query) ){
			return array( "status" => "error", "message" => "query is blank.", "query_e" => $query_e, "query" => $query1);
		}

		if( isset($query->mobile) ){
            $mobile = $query->mobile;
		}

		$error_data = array();
		if( !$mobile ){
			$error_data = array(
					'status' => 'error',
					'message' => 'Provide a mobile number.'
				);
		}

		if($error_data){
			return $error_data;
		}

		$cWhere = array('mobile' => $mobile);
		$data = $this->Common_model->getAll('id, name, dob, email', 'customer', $cWhere);
		if( !$data ){
			$data = array(
					'status' => 'error',
					'message' => 'This Number is not registered.'
				);
		}
		return $data;
	}

	/**
	 * update Profile from Android Devices
	 * Input Encypted
	 */
	public function updateProfileAndroid(){
		$query_e    = isset($_POST['query']) ? $_POST['query'] : '';
		include 'application/libraries/MCrypt.php';
		$encryption = new MCrypt();
		$inputText = $query_e;
		$query1 = $encryption->decrypt($inputText);
		$data = $this->updateProfile($query1);
		$this->jsonOutput($data);
	}

	/**
	 * update Profile from iOS Devices
	 * Input Encypted
	 */
	public function updateProfileiOS(){
		$query_e    = isset($_POST['query']) ? $_POST['query'] : '';
		$inputText = $query_e;
		$key = $this->key;
		$query1 = $this->iOSaesDecrypt($inputText, $key);
		$data = $this->updateProfile($query1);
		$this->jsonOutput($data);
	}

	/**
	 * Common method for update Customer Profile
	 */
	private function updateProfile($query){
		$query     = json_decode($query);

		if(!$query){
			return array( "status" => "error", "message" => "query is blank.", "query_e" => $query_e, "query" => $query1);
		}

		if( !count($query) ){
			return array( "status" => "error", "message" => "query is blank.", "query_e" => $query_e, "query" => $query1);
		}

		if( isset($query->mobile) ){
            $mobile = $query->mobile;
		}

		if( isset($query->name) ){
            $name = $query->name;
		}

		if( isset($query->email) ){
            $email = $query->email;
		}

		if( isset($query->dob) ){
            $dob = $query->dob;
        }

		$error_data = array();
		if( !$mobile ){
			$error_data = array(
					'status' => 'error',
					'message' => 'Provide a mobile number.'
				);
		}

		if( $dob ){
			$valid_date = $this->checkDate($dob);

			if( !$valid_date ){
	            $error_data = array(
						'status' => 'error',
						'message' => "Date should be in YYYY-MM-DD format."
					);
	        }
		}

		if($error_data){
			return $error_data;
		}

		$cWhere = array('mobile' => $mobile);
		$data = $this->Common_model->getAll('id', 'customer', $cWhere);
		if( !$data ){
			$data = array(
					'status' => 'error',
					'message' => 'This Number is not registered.'
				);
			return $data;
		}
		else{
			$customer_id = $data[0]->id;
			$data = array(
				'name' => $name,
				'email' => $email,
				'dob' => $dob,
				);
			$this->Common_model->update($data , $customer_id, "customer");
			$data = array(
					'status' => 'success',
				);
			return $data;
		}
	}

	private function checkDate($date)
    {

        $arr=explode("-",$date); // splitting the array
        if( count($arr) == 3 ){
            $yy = (int)$arr[0];
            $mm = (int)$arr[1];
            $dd = (int)$arr[2];
            $mm = $this->padZero($mm);
            $dd = $this->padZero($dd);
            if( in_array(0, array($yy, $mm, $dd)) ){
                return false;
            }
            if(!checkdate($yy,$mm,$dd)){
                return $yy.'-'.$this->padZero($mm).'-'.$this->padZero($dd);
            }
            else{
                return false;
            }
        }
        else{
            return false;
        }
	}

	private function padZero($str){
    	$strlen = strlen($str);
    	if($strlen == 1){
    		return '0'.$str;
    	}
    	return $str;
	}


	/**
	 * For OTP verification in Android devices
	 */
	public function activateAndroid(){
		$query_e    = isset($_POST['query']) ? $_POST['query'] : '';
		include 'application/libraries/MCrypt.php';
		$encryption = new MCrypt();
		$query1 = $encryption->decrypt($query_e);
        $data = $this->activate($query1);
        $this->encryptedOutput($data,$encryption);
	}

	/**
	 * For OTP verification in iOS devices
	 */
	public function activateiOS(){
		$query_e    = isset($_POST['query']) ? $_POST['query'] : '';
		$inputText = $query_e;
		$key = $this->key;
		$query1 = $this->iOSaesDecrypt($inputText, $key);
		$data = $this->activate($query1);
		$this->iOSencryptedOutput($data);
    }

	private function activate($query){
		$query = json_decode($query);

		if(!$query){
			return array( "status" => "error", "message" => "query is blank.", "query_e" => $query_e, "query" => $query1);
		}

		if( !count($query) ){
			return array( "status" => "error", "message" => "query is blank.", "query_e" => $query_e, "query" => $query1);
		}

		if( isset($query->mobile) ){
            $mobile = $query->mobile;
        }
        if( isset($query->otp) ){
            $otp = $query->otp;
        }

		if( !$mobile ){
			$data = array(
					'status' => 'error',
					'message' => 'Provide a mobile number.'
				);
			return $data;
		}
		if( !$otp ){
			$data = array(
					'status' => 'error',
					'message' => 'Provide an otp.'
				);
			return $data;
		}
		$cWhere = array('mobile' => $mobile);
		$data = $this->Common_model->getAll('id', 'customer', $cWhere);
		if( !$data ){
			$data = array(
					'status' => 'error',
					'message' => 'This Number is not registered.'
				);
			return $data;
		}
		$customer_id = $data[0]->id;
		$oWhere = array(
			'customer_id' => $customer_id,
			'otp' => $otp,
			'is_active' =>1,
			'is_deleted' => 0
			);
		$otpData = $this->Common_model->getAll('id', 'otp', $oWhere);
		if( !$otpData ){
			$data = array(
				'status' => 'error',
				'message' => 'OTP is not valid'
			);
           return $data;
		}
		$otp_id = $otpData[0]->id;
		$this->Common_model->update(array('is_deleted' => 1), $otp_id, 'otp');
		$data = $this->Common_model->update(array('is_active' => 1), $customer_id, 'customer');

		$data = array(
			'status' => 'success',
			'message' => 'Registration Completed'
		);
		return $data;
	}

	/**
	 * BOF Region Android encrypt
	 */
    private function encryptedOutput($data = array(), $encryption){
        $text = json_encode($data);
        $result = $encryption->encrypt($text);
        $returnObj = array(
			'result' => $result
        );
        $this->jsonOutput($returnObj);
	}
	/**
	 * EOF Region Android encrypt
	 */

	/**
	 * BOF Region iOS encrypt
	 */
    private function iOSencryptedOutput($data = array()){
        $text = json_encode($data);
        $key = $this->key;
        $result = $this->iOSaesEncrypt($text, $key);
        $data = array(
			'result' => $result
        );
        $this->jsonOutput($data);
    }

    private function iOSaesEncrypt($text, $key) {
        $ivSize = mcrypt_get_iv_size(MCRYPT_RIJNDAEL_128, MCRYPT_MODE_CBC);
        $iv = "0000000000000000";
        return trim(base64_encode(mcrypt_encrypt(MCRYPT_RIJNDAEL_128, $key, $text, MCRYPT_MODE_CBC, $iv)));
    }

	private function iOSaesDecrypt($base64encodedCipherText, $key) {
        //$base64encodedCipherText=3UUqOWpEXHPhFnOyC+8r/MZ8nkaRYUlBTPo0aL0B16ZStBoDwg/9Twfb6bj7MI3zPAqNggZfROOcNga2GZikslgZC3gJgwDLgig08CRgV4g'
        $ivSize = mcrypt_get_iv_size(MCRYPT_RIJNDAEL_128, MCRYPT_MODE_CBC);
        //$iv = mcrypt_create_iv($ivSize, MCRYPT_RAND);
        $iv = "0000000000000000";
        $cipherText = base64_decode($base64encodedCipherText);
        if (strlen($cipherText) < $ivSize) {
            throw new Exception('Missing initialization vector');
        }
        $iv = substr($cipherText, 0, $ivSize);
        $cipherText = substr($cipherText, $ivSize);

        $result = mcrypt_decrypt(MCRYPT_RIJNDAEL_128, $key, $cipherText, MCRYPT_MODE_CBC, $iv);
        $result = $this->iOSstripPadding(rtrim($result, chr(0x0b)));
        return $result;
    }

    private function iOSstripPadding($value){
        $pad = ord($value[($len = strlen($value)) - 1]);
        return $this->iOSpaddingIsValid($pad, $value) ? substr($value, 0, $len - $pad) : $value;
    }

    private function iOSpaddingIsValid($pad, $value){
        $beforePad = strlen($value) - $pad;
        return substr($value, $beforePad) == str_repeat(substr($value, -1), $pad);
	}
	/**
	 * EOF Region iOS encrypt
	 */

}