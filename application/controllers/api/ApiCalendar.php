<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ApiCalendar extends MY_Controller {

	public function __construct(){
		parent::__construct();
		$this->jsonInput();
        $this->load->model('Calendar_model');
		$this->load->model('Common_model');
	}

	public function test(){
    echo "string";
  }

    public function monthly(){
			// die("entry");
        $mobile    = isset($_POST['mobile']) ? $_POST['mobile'] : '';
        $year_month = isset($_POST['year_month']) ? $_POST['year_month'] : '';
        $region_id = isset($_POST['region']) ? (int)$_POST['region'] : 0;

        $valid_year_month = $this->validateMonth($year_month);

        $err_message = '';
        if( !$year_month ){
            $err_message = 'Provide Month & Year!';
        }
        else if( !$valid_year_month ){
            $err_message = "Month & Year should be in YYYY-MM format.";
        }

        $customer_id = 0;
        if($mobile != ""){
            $customer_id = $this->getCustomer($mobile);
            if( !$customer_id ){
                $err_message = "This number is not yet registered with us.";
            }
        }

        if($err_message){
            $data = array('status' => 'error', 'message' => $err_message );
            $this->jsonOutput($data);
        }

        $data = $this->Calendar_model->apiMonthly($customer_id, $valid_year_month, $region_id);

        if( !$data ){
            $data = array('status' => 'error', 'message' => 'No details for this month!' );
        }
        $this->jsonOutput($data);
    }

    

    public function getSplash(){
        $this->db
    		->select('id,year,image')
    		->from('splash')    		
    		->where('is_active', '1');
         $data = $this->db->get()->result();
         $this->db
    		->select('year_month,image')
    		->from('banner')    		
    		->where('is_active', '1');
         $banner = $this->db->get()->result();


         $this->jsonOutput(['data' =>  $data,'banner' =>  $banner]);
        // $this->jsonOutput($data);
    }

    public function encryptionTest(){
        
        
        $dataToEncrypt = 'Hello World';
/*
       // $cypherMethod = 'AES-256-GCM';
        $cypherMethod = 'aes-256-gcm';
        
        $key = $this->random_bytes(32);
        $iv = $this->random_bytes(openssl_cipher_iv_length($cypherMethod));
        echo("key".$key."\n");
        echo("iv".$iv."\n");
        $key = hex2bin('E0FAC2DD2C00FFE30F27A6D14568CB4F12EB84676A3A2BFB172A444C3BBB831F');
        $iv = '481500047193';
        echo("key".$key."\n");
        echo("iv".$iv."\n");

        $encryptedString = openssl_encrypt($dataToEncrypt, $cypherMethod, $key, $options=OPENSSL_ZERO_PADDING, $iv);
       // $encryptedString = openssl_encrypt($dataToEncrypt, $cypherMethod, $key, $options=0, $iv, $tag);
        echo($encryptedString."\n");
        $decryptedString = openssl_decrypt($encryptedString, $cypherMethod, $key, $options=OPENSSL_ZERO_PADDING, $iv);
        echo($decryptedString."\n");
        echo("1234\n");*/
       /* $plaintext = "message to be encrypted";
        $ivlen = openssl_cipher_iv_length($cipher="aes-256-gcm");
        $iv = openssl_random_pseudo_bytes($ivlen);
        $ciphertext_raw = openssl_encrypt($plaintext, $cipher, $key, $options=OPENSSL_RAW_DATA, $iv);
        $hmac = hash_hmac('sha256', $ciphertext_raw, $key, $as_binary=true);
        $ciphertext = base64_encode( $iv.$hmac.$ciphertext_raw );
        echo $ciphertext."\n";

        //decrypt later....
        $c = base64_decode($ciphertext);
        $ivlen = openssl_cipher_iv_length($cipher="aes-256-gcm");
        $iv = substr($c, 0, $ivlen);
        $hmac = substr($c, $ivlen, $sha2len=32);
        $ciphertext_raw = substr($c, $ivlen+$sha2len);
        $original_plaintext = openssl_decrypt($ciphertext_raw, $cipher, $key, $options=OPENSSL_RAW_DATA, $iv);
        $calcmac = hash_hmac('sha256', $ciphertext_raw, $key, $as_binary=true);
        if (hash_equals($hmac, $calcmac))// timing attack safe comparison
        {
            echo $original_plaintext."\n";
        }*/

        $textToEncrypt = $dataToEncrypt;
        $password = '3sc3RLrpd17';
        $key = substr(hash('sha256', $password, true), 0, 32);
        $cipher = 'aes-256-gcm';
        $iv_len = openssl_cipher_iv_length($cipher);
        $tag_length = 16;
        $iv = openssl_random_pseudo_bytes($iv_len);
        $tag = ""; // will be filled by openssl_encrypt

        $ciphertext = openssl_encrypt($textToEncrypt, $cipher, $key, OPENSSL_RAW_DATA, $iv);
        $encrypted = base64_encode($iv.$ciphertext.$tag);
        echo($encrypted);
        include 'application/libraries/Cipher.php';
       // $encryption = new MCrypt();
       // $this->load->library('Cipher.php');
        //$chacha20 = new ChaCha20\Cipher;
        $chacha20 = new Cipher();
        
        return;
    }

    private function random_bytes($length = 6)
    {
        $characters = '0123456789';
        $characters_length = strlen($characters);
        $output = '';
        for ($i = 0; $i < $length; $i++)
            $output .= $characters[rand(0, $characters_length - 1)];

        return $output;
    }


		public function monthlyNew(){

			// die("dei");
        $mobile    = isset($_POST['mobile']) ? $_POST['mobile'] : '';
        $year_month = isset($_POST['year_month']) ? $_POST['year_month'] : '';
        $region = isset($_POST['region']) ? $_POST['region'] : 'karnataka';

				$region_id=$this->Calendar_model->regionId($region);
				if($region_id == false){
					$region_id=13;
				}
// die($region_id);
        $valid_year_month = $this->validateMonth($year_month);

        $err_message = '';
        if( !$year_month ){
            $err_message = 'Provide Month & Year!';
        }
        else if( !$valid_year_month ){
            $err_message = "Month & Year should be in YYYY-MM format.";
        }

        $customer_id = 0;
        if($mobile != ""){
            $customer_id = $this->getCustomer($mobile);
            if( !$customer_id ){
                $err_message = "This number is not yet registered with us.";
            }
        }

        if($err_message){
            $data = array('status' => 'error', 'message' => $err_message );
            $this->jsonOutput($data);
        }

        $data = $this->Calendar_model->apiMonthly($customer_id, $valid_year_month, $region_id);

        if( !$data ){
            $data = array('status' => 'error', 'message' => 'No details for this month!' );
        }
        


        // $this->jsonOutput(['data' =>  $data,'banner' =>  $banner]);
        $this->jsonOutput($data);
    }

    public function daily(){
        $mobile    = isset($_POST['mobile']) ? $_POST['mobile'] : '';
        $date = isset($_POST['date']) ? $_POST['date'] : '';
        $region_id = isset($_POST['region']) ? (int)$_POST['region'] : 0;

        $valid_date = $this->calendarDate($date);

        $err_message = '';
        if( !$date ){
            $err_message = 'Provide Date!';
        }
        else if( !$valid_date ){
            $err_message = "Date should be in YYYY-MM-DD format.";
        }

        $customer_id = 0;
        if($mobile != ""){
            $customer_id = $this->getCustomer($mobile);
            if( !$customer_id ){
                $err_message = "This number is not yet registered with us.";
            }
        }
        if($err_message){
            $data = array('status' => 'error', 'message' => $err_message );
            $this->jsonOutput($data);
        }

        $data = $this->Calendar_model->apiDaily( $customer_id, $valid_date, $region_id);

        if( !$data ){
            $data = array('status' => 'error', 'message' => 'No Events on this day!' );
        }
        $this->jsonOutput($data);
    }

		public function dailyNew(){
        $mobile    = isset($_POST['mobile']) ? $_POST['mobile'] : '';
        $date = isset($_POST['date']) ? $_POST['date'] : '';
        $region = isset($_POST['region']) ? $_POST['region'] : 'KERALA';

				$region_id=$this->Calendar_model->regionId($region);
				if($region_id == false){
					$region_id=13;
				}
        $valid_date = $this->calendarDate($date);

        $err_message = '';
        if( !$date ){
            $err_message = 'Provide Date!';
        }
        else if( !$valid_date ){
            $err_message = "Date should be in YYYY-MM-DD format.";
        }

        $customer_id = 0;
        if($mobile != ""){
            $customer_id = $this->getCustomer($mobile);
            if( !$customer_id ){
                $err_message = "This number is not yet registered with us.";
            }
        }
        if($err_message){
            $data = array('status' => 'error', 'message' => $err_message );
            $this->jsonOutput($data);
        }

        $data = $this->Calendar_model->apiDaily( $customer_id, $valid_date, $region_id);

        if( !$data ){
            $data = array('status' => 'error', 'message' => 'No Events on this day!' );
        }
        $this->jsonOutput($data);
    }

    public function create(){
        $mobile    = isset($_POST['mobile']) ? $_POST['mobile'] : '';
        $date = isset($_POST['date']) ? $_POST['date'] : '';
        $time = isset($_POST['time']) ? $_POST['time'] : '';
        $title = isset($_POST['title']) ? $_POST['title'] : '';
        $description = isset($_POST['description']) ? $_POST['description'] : '';

        $valid_date = $this->calendarDate($date);

        $err_message = '';
        if( !$mobile ){
            $err_message = 'Provide Mobile Number!';
        }
        else if( !$date ){
            $err_message = 'Provide Date!';
        }
        else if( !$time ){
            $err_message = 'Provide Time!';
        }
        else if( !$title ){
            $err_message = 'Provide Title!';
        }
        else if( !$description ){
            $err_message = 'Provide Description!';
        }
        else if( !$valid_date ){
            $err_message = "Date should be in YYYY-MM-DD format.";
        }
        $customer_id = $this->getCustomer($mobile);
        if( !$customer_id ){
            $err_message = "This number is not yet registered with us.";
        }
        if($err_message){
            $data = array('status' => 'error', 'message' => $err_message );
            $this->jsonOutput($data);
        }

        /*
        create
        */
        $iData = array(
            'customer_id' => $customer_id,
            'date'        => $date,
            'time'        => $time,
            'title'       => $title,
            'description' => $description,
            );
        $created = $this->Common_model->insert($iData, 'customer_events');
        if(!$created){
            $data = array('status' => 'error', 'message' => '' );
            $this->jsonOutput($data);
        }
        $data = array('status' => 'success' );
        $this->jsonOutput($data);
    }

    public function customer_event_delete(){
        $mobile   = isset($_POST['mobile']) ? $_POST['mobile'] : '';
        $event_id = isset($_POST['event_id']) ? $_POST['event_id'] : '';

        $err_message = '';
        if( !$mobile ){
            $err_message = 'Provide Mobile Number!';
        }
        if( !$event_id ){
            $err_message = 'Provide Event Id!';
        }
        $customer_id = $this->getCustomer($mobile);
        if( !$customer_id ){
            $err_message = "This number is not yet registered with us.";
        }
        if($err_message){
            $data = array('status' => 'error', 'message' => $err_message );
            $this->jsonOutput($data);
        }
        $dWhere = array("id" => $event_id, "customer_id" => $customer_id, );
        $deleted = $this->db->where($dWhere)->delete('customer_events');

        if(!$deleted){
            $data = array('status' => 'error', 'message' => 'Sorry. Some Error Occurred.' );
            $this->jsonOutput($data);
        }
        $data = array('status' => 'success', 'message' => 'Deleted Successfully.' );
        $this->jsonOutput($data);
    }

    private function getCustomer($mobile){
        $cWhere = array('mobile' => $mobile);
        $data = $this->Common_model->getAll('id', 'customer', $cWhere);
        if( !$data ){
            $customer_id = 0;
        }
        $customer_id = $data[0]->id;
        return $customer_id;
    }


    private function validateMonth($date)
    {
        $arr=explode("-",$date); // splitting the array
        if( count($arr) == 2 ){
            $yy = (int)$arr[0];
            $mm = (int)$arr[1];
            $dd = 10;
            if( in_array(0, array($yy, $mm)) ){
                return false;
            }
            if(!checkdate($yy,$mm,$dd)){
                return $yy.'-'.$this->padZero($mm);
            }
            else{
                return false;
            }
        }
        else{
            return false;
        }
    }

    private function calendarDate($date)
    {
        $arr=explode("-",$date); // splitting the array
        if( count($arr) == 3 ){
            $yy = (int)$arr[0];
            $mm = (int)$arr[1];
            $dd = (int)$arr[2];
            $mm = $this->padZero($mm);
            $dd = $this->padZero($dd);
            if( in_array(0, array($yy, $mm, $dd)) ){
                return false;
            }
            if(!checkdate($yy,$mm,$dd)){
                return $yy.'-'.$this->padZero($mm).'-'.$this->padZero($dd);
            }
            else{
                return false;
            }
        }
        else{
            return false;
        }
    }

    private function padZero($str){
    	$strlen = strlen($str);
    	if($strlen == 1){
    		return '0'.$str;
    	}
    	return $str;
    }


}
