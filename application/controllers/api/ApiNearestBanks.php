<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ApiNearestBanks extends MY_Controller {

    public function __construct(){
        parent::__construct();
        $this->jsonInput();
    }

    function encrypt($plaintext, $key) {
        $plaintext = $this->pkcs5_pad($plaintext, 16);
        return bin2hex(mcrypt_encrypt(MCRYPT_RIJNDAEL_128, hex2bin($key), $plaintext, MCRYPT_MODE_ECB));
        //return bin2hex(mcrypt_encrypt(MCRYPT_RIJNDAEL_128, hex2bin($key), $plaintext, MCRYPT_MODE_CBC));
    }

    function decrypt($encrypted, $key) {
        $decrypted = mcrypt_decrypt(MCRYPT_RIJNDAEL_128, hex2bin($key), hex2bin($encrypted), MCRYPT_MODE_ECB);
        //$decrypted = mcrypt_decrypt(MCRYPT_RIJNDAEL_128, hex2bin($key), hex2bin($encrypted), MCRYPT_MODE_CBC);
        $padSize = ord(substr($decrypted, -1));
        return substr($decrypted, 0, $padSize*-1);
    }

    function pkcs5_pad ($text, $blocksize)
    {
        $pad = $blocksize - (strlen($text) % $blocksize);
        return $text . str_repeat(chr($pad), $pad);
    }

    public function index(){
        $query_e    = isset($_POST['query']) ? $_POST['query'] : '';

        include 'application/libraries/MCrypt.php';
        $encryption = new MCrypt();
        //echo $query_enc = $encryption->encrypt($query_e) . "<br/>";
        //$this->dd($query_dec);


                $inputText = $query_e;
                //$inputKey  = '57238004e784498bbc2f8bf984565090';
                //$blockSize = 256;

                $query1 = $encryption->decrypt($inputText);
                $query = $query1;
        //$this->dd($query);

        // $aes       = new AES($inputText, $inputKey, $blockSize);
        // $query     = $aes->decrypt();
        $query     = json_decode($query);
        // $this->dd($query);
        if(!$query){
            $this->jsonOutput(array( "status" => "error", "message" => "query is blank.", "query_e" => $query_e, "query" => $query1) );
        }

        if( !count($query) ){
            $this->jsonOutput(array( "status" => "error", "message" => "query is blank.", "query_e" => $query_e, "query" => $query1) );
        }
        $latitude = $longitude = $radius = $type = "";
        if( isset($query->latitude) ){
            $latitude = $query->latitude;
        }
        if( isset($query->longitude) ){
            $longitude = $query->longitude;
        }
        if( isset($query->radius) ){
            $radius = $query->radius;
        }
        if( isset($query->type) ){
            $type = $query->type;
        }

        if( !$latitude ){
            $this->jsonOutput(array( "status" => "error", "message" => "latitude is blank.") );
        }
        if( !$longitude ){
            $this->jsonOutput(array( "status" => "error", "message" => "longitude is blank.") );
        }
        if( !$radius ){
            $this->jsonOutput(array( "status" => "error", "message" => "radius is blank.") );
        }
        if( !$type ){
            $this->jsonOutput(array( "status" => "error", "message" => "type is blank.") );
        }

        if( !in_array($type, ["bank", "atm"]) ){
            $this->jsonOutput(array( "status" => "error", "message" => "type is not a valid value.") );
        }
        $radius = (int)$radius;
        if( !$radius ){
            $this->jsonOutput(array( "status" => "error", "message" => "radius should be an integer.") );
        }

        $latitude = (float)$latitude;
        if( !$latitude ){
            $this->jsonOutput(array( "status" => "error", "message" => "latitude should be a floating value.") );
        }
        if( !is_float($latitude) ){
            if( is_numeric($latitude) ){
                $this->jsonOutput(array( "status" => "error", "message" => "latitude should be a floating value.") );
            }
        }
        $longitude = (float)$longitude;
        if( !$longitude ){
            $this->jsonOutput(array( "status" => "error", "message" => "longitude should be a floating value.") );
        }
        if( !is_float($longitude) ){
            if( is_numeric($latitude) ){
                $this->jsonOutput(array( "status" => "error", "message" => "longitude should be a floating value.") );
            }
        }

        $extra_query = "&location=$latitude,$longitude&radius=$radius&type=$type";
        $geocode = file_get_contents('https://maps.googleapis.com/maps/api/place/search/json?sensor=true&key=AIzaSyCIIeJvdCIs6oNwEojpJAFyVr1MiE0Xdno&keyword=federal&name=federal'.$extra_query);

        if(!$geocode){
            $this->jsonOutput(array( "status" => "error", "message" => "No Result") );
        }

        $output = json_decode($geocode);
        //$output = $this->encrypt($geocode,$inputKey);
        // $output = new AES($output, $inputKey, $blockSize);
        // $output = $aes->encrypt();

        $this->jsonOutput($output);
    }

    public function getUrlResult(){
        $query_e    = isset($_POST['query']) ? $_POST['query'] : '';
        include 'application/libraries/MCrypt.php';
        $encryption = new MCrypt();
        $inputText = $query_e;
        $inputKey  = '57238004e784498bbc2f8bf984565090';
        $blockSize = 256;
        $query = $encryption->decrypt($inputText);
        //$query = $this->decrypt($inputText,$inputKey);
        if(!$query){
            $this->jsonOutput(array( "status" => "error", "message" => "query is blank.") );
        }

        $result = file_get_contents($query);

        if(!$result){
            $this->jsonOutput(array( "status" => "error", "message" => "No Result") );
        }
        //$output = $this->encrypt($result,$inputKey);
        $result = json_decode($result);
        $this->jsonOutput(["status"=>"success","result" => $result]);
    }

    public function iOSgetUrlResult(){
        $query_e    = isset($_POST['query']) ? $_POST['query'] : '';
        $inputText = $query_e;
        $inputKey  = '57238004e784498bbc2f8bf984565090';
        $blockSize = 256;
        $query = $this->iOSaesDecrypt($inputText);
        //$query = $this->decrypt($inputText,$inputKey);
        if(!$query){
            $this->jsonOutput(array( "status" => "error", "message" => "query is blank.") );
        }

        $result = file_get_contents($query);

        if(!$result){
            $this->jsonOutput(array( "status" => "error", "message" => "No Result") );
        }
        //$output = $this->encrypt($result,$inputKey);
        $result = json_decode($result);
        $this->jsonOutput(["status"=>"success","result" => $result]);
    }


    public function iOSindex(){
        $query_e    = isset($_POST['query']) ? $_POST['query'] : '';


        //echo $query_enc = $encryption->encrypt($query_e) . "<br/>";
        //$this->dd($query_dec);


        $inputText = $query_e;
        //$inputKey  = '57238004e784498bbc2f8bf984565090';
        //$blockSize = 256;
        $key = "Z9WjU1M6HDOUY.Qb";
        $query1 = $this->iOSaesDecrypt($inputText, $key);
        $query = $query1;
        //$this->dd($query);

        // $aes       = new AES($inputText, $inputKey, $blockSize);
        // $query     = $aes->decrypt();
        $query     = json_decode($query);
        // $this->dd($query);
        if(!$query){
            $this->jsonOutput(array( "status" => "error", "message" => "query is blank.", "query_e" => $query_e, "query" => $query1) );
        }

        if( !count($query) ){
            $this->jsonOutput(array( "status" => "error", "message" => "query is blank.", "query_e" => $query_e, "query" => $query1) );
        }
        $latitude = $longitude = $radius = $type = "";
        if( isset($query->latitude) ){
            $latitude = $query->latitude;
        }
        if( isset($query->longitude) ){
            $longitude = $query->longitude;
        }
        if( isset($query->radius) ){
            $radius = $query->radius;
        }
        if( isset($query->type) ){
            $type = $query->type;
        }

        if( !$latitude ){
            $this->jsonOutput(array( "status" => "error", "message" => "latitude is blank.") );
        }
        if( !$longitude ){
            $this->jsonOutput(array( "status" => "error", "message" => "longitude is blank.") );
        }
        if( !$radius ){
            $this->jsonOutput(array( "status" => "error", "message" => "radius is blank.") );
        }
        if( !$type ){
            $this->jsonOutput(array( "status" => "error", "message" => "type is blank.") );
        }

        if( !in_array($type, ["bank", "atm"]) ){
            $this->jsonOutput(array( "status" => "error", "message" => "type is not a valid value.") );
        }
        $radius = (int)$radius;
        if( !$radius ){
            $this->jsonOutput(array( "status" => "error", "message" => "radius should be an integer.") );
        }

        $latitude = (float)$latitude;
        if( !$latitude ){
            $this->jsonOutput(array( "status" => "error", "message" => "latitude should be a floating value.") );
        }
        if( !is_float($latitude) ){
            if( is_numeric($latitude) ){
                $this->jsonOutput(array( "status" => "error", "message" => "latitude should be a floating value.") );
            }
        }
        $longitude = (float)$longitude;
        if( !$longitude ){
            $this->jsonOutput(array( "status" => "error", "message" => "longitude should be a floating value.") );
        }
        if( !is_float($longitude) ){
            if( is_numeric($latitude) ){
                $this->jsonOutput(array( "status" => "error", "message" => "longitude should be a floating value.") );
            }
        }

        $extra_query = "&location=$latitude,$longitude&radius=$radius&type=$type";
        $geocode = file_get_contents('https://maps.googleapis.com/maps/api/place/search/json?sensor=true&key=AIzaSyCIIeJvdCIs6oNwEojpJAFyVr1MiE0Xdno&keyword=federal&name=federal'.$extra_query);

        if(!$geocode){
            $this->jsonOutput(array( "status" => "error", "message" => "No Result") );
        }

        $output = json_decode($geocode);
        //$output = $this->encrypt($geocode,$inputKey);
        // $output = new AES($output, $inputKey, $blockSize);
        // $output = $aes->encrypt();

        $this->jsonOutput($output);
    }


    private function iOSaesDecrypt($base64encodedCipherText, $key) {
        //$base64encodedCipherText=3UUqOWpEXHPhFnOyC+8r/MZ8nkaRYUlBTPo0aL0B16ZStBoDwg/9Twfb6bj7MI3zPAqNggZfROOcNga2GZikslgZC3gJgwDLgig08CRgV4g'
        $ivSize = mcrypt_get_iv_size(MCRYPT_RIJNDAEL_128, MCRYPT_MODE_CBC);
        //$iv = mcrypt_create_iv($ivSize, MCRYPT_RAND);
        $iv = "0000000000000000";
        $cipherText = base64_decode($base64encodedCipherText);
        if (strlen($cipherText) < $ivSize) {
            throw new Exception('Missing initialization vector');
        }
        $iv = substr($cipherText, 0, $ivSize);
        $cipherText = substr($cipherText, $ivSize);

        $result = mcrypt_decrypt(MCRYPT_RIJNDAEL_128, $key, $cipherText, MCRYPT_MODE_CBC, $iv);
        $result = $this->iOSstripPadding(rtrim($result, chr(0x0b)));
        return $result;
    }

    private function iOSstripPadding($value){
        $pad = ord($value[($len = strlen($value)) - 1]);
        return $this->iOSpaddingIsValid($pad, $value) ? substr($value, 0, $len - $pad) : $value;
    }

    private function iOSpaddingIsValid($pad, $value){
        $beforePad = strlen($value) - $pad;
        return substr($value, $beforePad) == str_repeat(substr($value, -1), $pad);
    }

}