<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ApiVersion extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->jsonInput();
		$this->load->model('Common_model');
	}

	public function latest()
	{
		$sWhere = array(
			'is_active' => 1,
			'is_deleted' => 0,
			);
		$data = $this->db->query("
			SELECT 
				version_name 
			FROM app_version 
			WHERE is_active = 1 AND is_deleted = 0 
			ORDER BY id DESC
			")->result();
		$this->jsonOutput($data);
	}

}
