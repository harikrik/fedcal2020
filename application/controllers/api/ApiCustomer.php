<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ApiCustomer extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->jsonInput();
		$this->load->model('User_model');
		$this->load->model('AppHome_model');
		$this->load->model('Common_model');
	}

	public function register()
	{
		
		$country_code = isset($_POST['country_code']) ? trim($_POST['country_code']) : '';
		$mobile = isset($_POST['mobile']) ? trim($_POST['mobile']) : '';
		$deviceOs = isset($_POST['DeviceOs']) ? trim($_POST['DeviceOs']): '';
		
		if( !$country_code ){
			$data = array(
					'status' => 'error',
					'message' => 'Provide a Country Code.'
				);
			$this->jsonOutput($data);
		}
		if( !$mobile ){
			$data = array(
					'status' => 'error',
					'message' => 'Provide a mobile number.'
				);
			$this->jsonOutput($data);
		}
	
		$country_code = str_replace(" ","",$country_code);
		$country_code = str_replace("+","",$country_code);
		$country_code = (int)str_replace("-","",$country_code);

		$mobile = $this->remove_countrycode($mobile, $country_code);

		if($country_code == "91"){
			
			$mob_len = strlen($mobile);
			if( $mob_len != 10 ){
			    $data = array(
					'status' => 'error',
					'message' => 'Sorry.. Invalid Mobile Number.'
				);
				$this->jsonOutput($data);
				
			}
		}
		
		$cWhere = array('mobile' => $mobile);
		
		$data = $this->Common_model->getAll('id', 'customer', $cWhere);
		
		if( $data ){
			$customer_id = $data[0]->id;
		}
		else{
			/* insert customer */
			$iData = array('mobile' => $mobile, 'country_code' => $country_code);
			$customer_id = $this->Common_model->insert($iData, 'customer');
			if( !$customer_id ){
				$data = array(
					'status' => 'error',
					'message' => 'Sorry.. Some error occurred.'
				);
				$this->jsonOutput($data);
			}
		}
		
		//$reg_otp_val=$this->Common_model->validateRegOtp($mobile);
		$reg_otp_val = true;
		if($reg_otp_val==true){
			$updt=$this->Common_model->updateExistOtp($customer_id);

			$otp = mt_rand(1000, 9999);
			$oiData = array(
				'customer_id' => $customer_id,
				'otp' => $otp
				);
			$otp_id = $this->Common_model->insert($oiData, 'otp');
			$data = array(
				'status' => 'success'
				);


			// $message = "OTP for Federal Calendar Login is $otp";
			if($deviceOs == 'Android'){
				//OTP to activate Federal Calendar App is
	      // $message = "<#> OTP for Federal Calendar Login is $otp ".APPHASH;
		   $message = "<#> OTP to activate Federal Calendar App is $otp - Federal Bank".APPHASH;
	    }else{
	       $message = "OTP to activate Federal Calendar App is $otp - Federal Bank";
	    }
			if($country_code == "91"){
				if( $mobile == "4168874038" ){
					log_message('debug', '4168874038 in india:'.$country_code);
				}
				$mob_len = strlen($mobile);
				if( $mob_len != 10 ){
				    $data = array(
						'status' => 'error',
						'message' => 'Sorry.. Invalid Mobile Number.'
					);
					$this->jsonOutput($data);
				}
				$mobile = $country_code.$mobile;
				
				$this->sendOTP($message, $mobile);
			}
			else{
				if( $mobile == "4168874038" ){
					log_message('debug', '4168874038 in canada:'.$country_code);
				}
				$mobile = $country_code.$mobile;
				$this->sendIntlOTP($message, $mobile);
		}
	}else {
		$data = array(
		'status' => 'error',
		'message' => 'OTP is already sent'
	);
	}
		$this->jsonOutput($data);
	}

	private function remove_countrycode($mobile, $country_code){

		$mobile = ltrim( $mobile , '0+' );
		$mob_len = strlen($mobile);
		if( $country_code != "91" ){
		    $mobile = preg_replace("~^$country_code~", '', $mobile);
		}
		elseif( $mob_len > 10 ){
		    $mobile = preg_replace("~^$country_code~", '', $mobile);
		}
		return $mobile;
	}

	public function profile(){
		$mobile = isset($_POST['mobile']) ? trim($_POST['mobile']) : '';

		$error_data = array();
		if( !$mobile ){
			$error_data = array(
					'status' => 'error',
					'message' => 'Provide a mobile number.'
				);
		}

		if($error_data){
			$this->jsonOutput($error_data);
		}

		$cWhere = array('mobile' => $mobile);
		$data = $this->Common_model->getAll('id, name, dob, email', 'customer', $cWhere);
		if( !$data ){
			$data = array(
					'status' => 'error',
					'message' => 'This Number is not registered.'
				);
			$this->jsonOutput($data);
		}
		else{
			$this->jsonOutput($data);
		}
	}

	public function setRegion(){
		$mobile = isset($_POST['mobile']) ? trim($_POST['mobile']) : '';
		$region_id = isset($_POST['region_id']) ? trim($_POST['region_id']) : 0;

		$error_data = array();
		if( !$mobile ){
			$error_data = array(
					'status' => 'error',
					'message' => 'Provide a mobile number.'
				);
		}

		if( !$region_id ){
			$error_data = array(
					'status' => 'error',
					'message' => 'Provide a region.'
				);
		}

		if($error_data){
			$this->jsonOutput($error_data);
		}

		$cWhere = array('mobile' => $mobile);
		$customer = $this->Common_model->getAll('id', 'customer', $cWhere);
		if( !$customer ){
			$data = array(
					'status' => 'error',
					'message' => 'This Number is not registered.'
				);
			$this->jsonOutput($data);
		}

		$rWhere = array('id' => $region_id, 'is_active' => 1, 'is_deleted' => 0);
		$data = $this->Common_model->getAll('id', 'region', $rWhere);
		if( !$data ){
			$data = array(
					'status' => 'error',
					'message' => "This Region doesn't exist."
				);
			$this->jsonOutput($data);
		}

		$customer_id = $customer[0]->id;
		$data = array(
			'region_id' => $region_id
			);
		$this->Common_model->update($data , $customer_id, "customer");
		$data = array(
				'status' => 'success',
			);
		$this->jsonOutput($data);
	}

	public function updateCustomer(){
		$mobile = isset($_POST['mobile']) ? trim($_POST['mobile']) : '';
		$name   = isset($_POST['name']) ? trim($_POST['name']) : '';
		$email  = isset($_POST['email']) ? trim($_POST['email']) : '';
		$dob    = isset($_POST['dob']) ? trim($_POST['dob']) : '';

		$error_data = array();
		if( !$mobile ){
			$error_data = array(
					'status' => 'error',
					'message' => 'Provide a mobile number.'
				);
		}
		/*else if( !$name ){
			$error_data = array(
					'status' => 'error',
					'message' => 'Provide a name.'
				);
		}
		else if( !$email ){
			$error_data = array(
					'status' => 'error',
					'message' => 'Provide an email.'
				);
		}*/

		if( $dob ){
			$valid_date = $this->checkDate($dob);

			if( !$valid_date ){
	            $error_data = array(
						'status' => 'error',
						'message' => "Date should be in YYYY-MM-DD format."
					);
	        }
		}

		if($error_data){
			$this->jsonOutput($error_data);
		}

		$cWhere = array('mobile' => $mobile);
		$data = $this->Common_model->getAll('id', 'customer', $cWhere);
		if( !$data ){
			$data = array(
					'status' => 'error',
					'message' => 'This Number is not registered.'
				);
			$this->jsonOutput($data);
		}
		else{
			$customer_id = $data[0]->id;
			$data = array(
				'name' => $name,
				'email' => $email,
				'dob' => $dob,
				);
			$this->Common_model->update($data , $customer_id, "customer");
			$data = array(
					'status' => 'success',
				);
			$this->jsonOutput($data);
		}
	}

	public function activate(){
		$mobile = isset($_POST['mobile']) ? trim($_POST['mobile']) : '';
		$otp = isset($_POST['otp']) ? trim($_POST['otp']) : '';

		if( !$mobile ){
			$data = array(
					'status' => 'error',
					'message' => 'Provide a mobile number.'
				);
			$this->jsonOutput($data);
		}
		if( !$otp ){
			$data = array(
					'status' => 'error',
					'message' => 'Provide an otp.'
				);
			$this->jsonOutput($data);
		}
		$cWhere = array('mobile' => $mobile);
		$data = $this->Common_model->getAll('id', 'customer', $cWhere);
		if( !$data ){
			$data = array(
					'status' => 'error',
					'message' => 'This Number is not registered.'
				);
			$this->jsonOutput($data);
		}
		$customer_id = $data[0]->id;
		$oWhere = array(
			'customer_id' => $customer_id,
			'otp' => $otp,
			);
		$data = $this->Common_model->getAll('otp', 'otp', $oWhere);
		if( !$data ){
			$data = array(
				'status' => 'error',
				'message' => 'Invalid OTP'
			);
			$this->jsonOutput($data);
		}
		$data = $this->Common_model->update(array('is_active' => 1), $customer_id, 'customer');
		$data = array(
			'status' => 'success',
			'message' => 'Registration Completed'
		);
		$this->jsonOutput($data);

	}

	public function encrypt(){
		$query_e    = isset($_POST['query']) ? $_POST['query'] : '';

		include 'application/libraries/MCrypt.php';
		$encryption = new MCrypt();

		$inputText = $query_e;
		$query1 = $encryption->encrypt($inputText);
		$this->jsonOutput($query1);
	}

	public function activateNew(){
		$query_e    = isset($_POST['query']) ? $_POST['query'] : '';

		include 'application/libraries/MCrypt.php';
		$encryption = new MCrypt();

		$inputText = $query_e;
		$query1 = $encryption->decrypt($inputText);
		$query = $query1;
		$query     = json_decode($query);

		if(!$query){
			$this->jsonOutput(array( "status" => "error", "message" => "query is blank.", "query_e" => $query_e, "query" => $query1) );
		}

		if( !count($query) ){
			$this->jsonOutput(array( "status" => "error", "message" => "query is blank.", "query_e" => $query_e, "query" => $query1) );
		}

		if( isset($query->mobile) ){
            $mobile = $query->mobile;
        }
        if( isset($query->otp) ){
            $otp = $query->otp;
        }

		if( !$mobile ){
			$data = array(
					'status' => 'error',
					'message' => 'Provide a mobile number.'
				);
			$this->jsonOutput($data);
		}
		if( !$otp ){
			$data = array(
					'status' => 'error',
					'message' => 'Provide an otp.'
				);
			$this->jsonOutput($data);
		}
		$cWhere = array('mobile' => $mobile);
		$data = $this->Common_model->getAll('id', 'customer', $cWhere);
		if( !$data ){
			$data = array(
					'status' => 'error',
					'message' => 'This Number is not registered.'
				);
			$this->jsonOutput($data);
		}
		$customer_id = $data[0]->id;
		$oWhere = array(
			'customer_id' => $customer_id,
			'otp' => $otp,
			'is_active' =>1,
			'is_deleted' => 0
			);
		$otpData = $this->Common_model->getAll('id', 'otp', $oWhere);
		if( !$otpData ){
			$data = array(
				'status' => 'error',
				'message' => 'OTP is not valid'
			);
			$this->jsonOutput($data);
		}
		$otp_id = $otpData[0]->id;
		$this->Common_model->update(array('is_deleted' => 1), $otp_id, 'otp');
		$data = $this->Common_model->update(array('is_active' => 1), $customer_id, 'customer');

		$data = array(
			'status' => 'success',
			'message' => 'Registration Completed'
		);
		$this->jsonOutput($data);

	}

	public function activateNewiOS(){
		$query_e    = isset($_POST['query']) ? $_POST['query'] : '';
		$inputText = $query_e;
		$key = "Z9WjU1M6HDOUY.Qb";
		$query1 = $this->iOSaesDecrypt($inputText, $key);
		$query = $query1;
		$query     = json_decode($query);

		if(!$query){
			$this->jsonOutput(array( "status" => "error", "message" => "query is blank.", "query_e" => $query_e, "query" => $query1) );
		}

		if( !count($query) ){
			$this->jsonOutput(array( "status" => "error", "message" => "query is blank.", "query_e" => $query_e, "query" => $query1) );
		}

		if( isset($query->mobile) ){
            $mobile = $query->mobile;
        }
        if( isset($query->otp) ){
            $otp = $query->otp;
        }

		if( !$mobile ){
			$data = array(
					'status' => 'error',
					'message' => 'Provide a mobile number.'
				);
			$this->jsonOutput($data);
		}
		if( !$otp ){
			$data = array(
					'status' => 'error',
					'message' => 'Provide an otp.'
				);
			$this->jsonOutput($data);
		}
		$cWhere = array('mobile' => $mobile);
		$data = $this->Common_model->getAll('id', 'customer', $cWhere);
		if( !$data ){
			$data = array(
					'status' => 'error',
					'message' => 'This Number is not registered.'
				);
			$this->jsonOutput($data);
		}
		$customer_id = $data[0]->id;


		if($mobile=="9846435721" && $otp=="3647"){
				$data = array(
					'status' => 'success',
					'message' => 'Registration Completed'
				);
				$this->jsonOutput($data);
		}




		$oWhere = array(
			'customer_id' => $customer_id,
			'otp' => $otp,
			'is_active' =>1,
			'is_deleted' => 0
			);
		$data = $this->Common_model->getAll('id,created_time', 'otp', $oWhere);
		if( !$data ){
			$data = array(
				'status' => 'error',
				'message' => 'OTP is not valid'
			);
			$this->jsonOutput($data);
		}

		$otp_id = $data[0]->id;
		$otp_creat_time = $data[0]->created_time;
		$isvalid = $this->Common_model->validateTimeOTP($otp_id,$otp_creat_time);


		if($isvalid){
			$this->Common_model->update(array('is_deleted' => 1), $otp_id, 'otp');
			$data = $this->Common_model->update(array('is_active' => 1), $customer_id, 'customer');
			$data = array(
				'status' => 'success',
				'message' => 'Registration Completed'
			);
			$this->jsonOutput($data);
		}else {
			$data = array(
				'status' => 'error',
				'message' => 'OTP is not valid'
			);
			$this->jsonOutput($data);
		}
	}

	public function resendOTP(){
		$country_code = isset($_POST['country_code']) ? trim($_POST['country_code']) : '';
		$mobile = isset($_POST['mobile']) ? trim($_POST['mobile']) : '';
		$deviceOs = isset($_POST['DeviceOs']) ? trim($_POST['DeviceOs']): '';

		if( !$mobile ){
			$data = array(
					'status' => 'error',
					'message' => 'Provide a mobile number.'
				);
			$this->jsonOutput($data);
		}

		if( !$country_code ){
			$data = array(
					'status' => 'error',
					'message' => 'Provide Country Code.'
				);
			$this->jsonOutput($data);
		}

		$country_code = str_replace(" ","",$country_code);
		$country_code = str_replace("+","",$country_code);
		$country_code = (int)str_replace("-","",$country_code);

		$mobile = $this->remove_countrycode($mobile, $country_code);

		if($country_code == "91"){
			$mob_len = strlen($mobile);
			if( $mob_len != 10 ){
			    $data = array(
					'status' => 'error',
					'message' => 'Sorry.. Invalid Mobile Number.'
				);
				$this->jsonOutput($data);
			}
		}

		$cWhere = array('mobile' => $mobile);
		$data = $this->Common_model->getAll('id', 'customer', $cWhere);
		if( !$data ){
			$data = array(
					'status' => 'error',
					'message' => 'This Number is not registered.'
				);
			$this->jsonOutput($data);
		}
		$customer_id = $data[0]->id;

		//$reg_otp_val=$this->Common_model->validateResendOtp($mobile);
		$reg_otp_val=true;
		if($reg_otp_val==false){

				$data = array(
				'status' => 'error',
				'message' => 'OTP is already resent, try after sometime'
			);
			$this->jsonOutput($data);
			return;

		}

		$updt=$this->Common_model->updateExistOtp($customer_id);

		$otp = mt_rand(1000, 9999);
		$oiData = array(
			'customer_id' => $customer_id,
			'otp' => $otp
			);
		$otp_id = $this->Common_model->insert($oiData, 'otp');
		$data = array(
			'status' => 'success'
			);

		// $message = "OTP for Federal Calendar Login is $otp";
		if($deviceOs == 'Android'){
       $message = "<#> OTP for Federal Calendar Login is $otp ".APPHASH;
    }else{
       $message = "OTP for Federal Calendar Login is $otp";
    }
		if($country_code == "91"){
			$mob_len = strlen($mobile);
			if( $mob_len != 10 ){
			    $data = array(
					'status' => 'error',
					'message' => 'Sorry.. Invalid Mobile Number.'
				);
				$this->jsonOutput($data);
			}
			$mobile = $country_code.$mobile;
			$this->sendOTP($message, $mobile);
		}
		else{
			$mobile = $country_code.$mobile;
			$this->sendIntlOTP($message, $mobile);
		}
		$data = array(
				'status' => 'success',
				'message' => ''
			);
		$this->jsonOutput($data);

	}

	public function notification(){
		$mobile = isset($_POST['mobile']) ? trim($_POST['mobile']) : '';
		$page   = isset($_POST['page']) ? $_POST['page'] : 1;
		$page  = ( $page != 0 ) ? $page : 1;
		 $offset = 20;

		$error_data = array();
		if( !$mobile ){
			$error_data = array(
					'status' => 'error',
					'message' => 'Provide a mobile number.'
				);
		}
		if($error_data){
			$this->jsonOutput($error_data);
		}


		$cWhere = array('mobile' => $mobile);
		$data = $this->Common_model->getAll('id', 'customer', $cWhere);
		if( !$data ){
			$data = array(
					'status' => 'error',
					'message' => 'This Number is not registered.'
				);
			$this->jsonOutput($data);
		}
		$customer_id = $data[0]->id;


		$this->load->model('Customer_model');
		$sWhere = "";
		if( $page == 1 ){
			$sLimit = " LIMIT $offset ";
		}
		else{
			$start = ($page - 1) * $offset;
			$sLimit = " LIMIT $start, $offset ";
		}

		$data = $this->Customer_model->notification($sWhere, " ORDER BY id DESC ", $sLimit, $customer_id)->result();
		$this->jsonOutput($data);
	}

	public function addGcmToken()
	{
		$data = array();
		$mobile         = isset($_POST['mobile']) ? $_POST['mobile'] : '';
		$gcm_token = isset($_POST['gcm_token']) ? $_POST['gcm_token'] : '';
		$device_os = isset($_POST['device_os']) ? $_POST['device_os'] : 'android';
		/*$device_id       = isset($_POST['device_id']) ? $_POST['device_id'] : '';*/
		if( $mobile == '' ){
			$data = array( 'status' => 'fail', 'message' => 'Provide Valid Mobile Number!' );
		}
		else if( $gcm_token == '' ){
			$data = array( 'status' => 'fail', 'message' => 'Provide Token!' );
		}
		/*else if( $device_id == '' ){
			$data = array( 'status' => 'fail', 'message' => 'Provide Device Id!' );
		}*/
        else{
        	$cWhere = array('mobile' => $mobile);
			$data = $this->Common_model->getAll('id', 'customer', $cWhere);
			if( !$data ){
				$data = array(
						'status' => 'error',
						'message' => 'This Number is not registered.'
					);
				$this->jsonOutput($data);
			}
			$customer_id = $data[0]->id;

        	$existing_gcmUser = $this->User_model->existingGcmToken($gcm_token);
	        if( $existing_gcmUser ){
	            /* update */
	            $data = $this->User_model->updateGCM($existing_gcmUser->id, $customer_id, $gcm_token);
	        }
	        else{
	            /* insert */
	            $ins_data = array(
					"customer_id"   => $customer_id,
					/*"device_id" => $device_id,*/
					"gcm_token" => $gcm_token,
					"device_os" => $device_os
	            	);
	            $insert_id = $this->User_model->insertGCM($ins_data);
	            if( $insert_id ){
		        	$data = array( 'status' => 'success', 'data' => "token created." );
		        }
		        else{
		        	$data = array( 'status' => 'fail', 'data' => "Some error occurred." );
		        }
	        }

        }
        $this->jsonOutput($data);
    }

    private function checkDate($date)
    {

        $arr=explode("-",$date); // splitting the array
        if( count($arr) == 3 ){
            $yy = (int)$arr[0];
            $mm = (int)$arr[1];
            $dd = (int)$arr[2];
            $mm = $this->padZero($mm);
            $dd = $this->padZero($dd);
            if( in_array(0, array($yy, $mm, $dd)) ){
                return false;
            }
            if(!checkdate($yy,$mm,$dd)){
                return $yy.'-'.$this->padZero($mm).'-'.$this->padZero($dd);
            }
            else{
                return false;
            }
        }
        else{
            return false;
        }
    }

    private function padZero($str){
    	$strlen = strlen($str);
    	if($strlen == 1){
    		return '0'.$str;
    	}
    	return $str;
	}


	private function iOSaesDecrypt($base64encodedCipherText, $key) {
        //$base64encodedCipherText=3UUqOWpEXHPhFnOyC+8r/MZ8nkaRYUlBTPo0aL0B16ZStBoDwg/9Twfb6bj7MI3zPAqNggZfROOcNga2GZikslgZC3gJgwDLgig08CRgV4g'
        $ivSize = mcrypt_get_iv_size(MCRYPT_RIJNDAEL_128, MCRYPT_MODE_CBC);
        //$iv = mcrypt_create_iv($ivSize, MCRYPT_RAND);
        $iv = "0000000000000000";
        $cipherText = base64_decode($base64encodedCipherText);
        if (strlen($cipherText) < $ivSize) {
            throw new Exception('Missing initialization vector');
        }
        $iv = substr($cipherText, 0, $ivSize);
        $cipherText = substr($cipherText, $ivSize);

        $result = mcrypt_decrypt(MCRYPT_RIJNDAEL_128, $key, $cipherText, MCRYPT_MODE_CBC, $iv);
        $result = $this->iOSstripPadding(rtrim($result, chr(0x0b)));
        return $result;
    }

    private function iOSstripPadding($value){
        $pad = ord($value[($len = strlen($value)) - 1]);
        return $this->iOSpaddingIsValid($pad, $value) ? substr($value, 0, $len - $pad) : $value;
    }

    private function iOSpaddingIsValid($pad, $value){
        $beforePad = strlen($value) - $pad;
        return substr($value, $beforePad) == str_repeat(substr($value, -1), $pad);
    }


		public function getTimeZone(){

			$query_e    = isset($_POST['query']) ? $_POST['query'] : '';
			$inputText = $query_e;
			$key = "Z9WjU1M6HDOUY.Qb";
			$query1 = $this->iOSaesDecrypt($inputText, $key);
			$query = $query1;
			$query     = json_decode($query);

			if(!$query){
				$this->jsonOutput(array( "status" => "error", "message" => "query is blank.", "query_e" => $query_e, "query" => $query1) );
			}

			if( !count($query) ){
				$this->jsonOutput(array( "status" => "error", "message" => "query is blank.", "query_e" => $query_e, "query" => $query1) );
			}


			$lat = isset($query->lat) ? trim($query->lat): '9.9312328000000001';
			$lng = isset($query->lng) ? trim($query->lng): '76.26730409999999';


	      $url = "http://api.geonames.org/timezoneJSON?lat=".$lat."&lng=".$lat."&username=oranzsoftwares";
	      $ch = curl_init($url);
	      curl_setopt($ch, CURLOPT_HEADER, true);
	      // curl_setopt($ch, CURLOPT_USERPWD, "$api_key:$password");
	      curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	      $server_output = curl_exec($ch);
	      $info = curl_getinfo($ch);
	      $header_size = curl_getinfo($ch, CURLINFO_HEADER_SIZE);
	      $headers = substr($server_output, 0, $header_size);
	      $response = substr($server_output, $header_size);
	      curl_close($ch);
	      if($info['http_code'] == 200) {
	        $json_obj = json_decode('');
	        $postdata = file_get_contents("php://input");
	        $json_obj=json_decode($response.'');

	        $result['status'] = SUCCESS;
	        $result['data']= $json_obj;

					$this->jsonOutput($json_obj);
	      } else {
					$data = array(
						'status' => 'error',
						'message' => 'somthing wrong'
					);
					$this->jsonOutput($data);
	      }

	    }


}
