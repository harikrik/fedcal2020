<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ApiSeasonal extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->jsonInput();
		$this->load->model('Common_model');
	}

	public function index()
	{
		$sWhere = array(
			'is_active' => 1,
			'is_deleted' => 0,
			);
		$data = $this->db->query("
			SELECT 
				CONCAT( '".base_url().SEASONAL_IMG."', image ) as image
			FROM seasonal_image 
			WHERE is_active = 1 AND is_deleted = 0 
			ORDER BY id DESC
			")->result();
		$this->jsonOutput($data);
	}

}
