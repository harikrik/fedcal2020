<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ApiRailway extends MY_Controller {

    private $date_format = 'd-m-Y';

    public function __construct(){
        parent::__construct();
        $this->jsonInput();
    }

    public function suggestTrain(){
        $name = isset($_POST['name']) ? trim($_POST['name']): '';

        if($name == ""){
            $this->jsonOutput(array( "status" => "error", "message" => "Please provide Name") );
        }
       
        $query = RAILWAY_API_BASEPATH.'suggest-train/train/'.$name.'/apikey/'.RAILWAY_API_KEY.'/';
        $result = file_get_contents($query);

        if(!$result){
            $this->jsonOutput(array( "status" => "error", "message" => "No Result") );
        }
        $result = json_decode($result);
        $this->jsonOutput(["status"=>"success","result" => $result]);
    }

    public function suggestStation(){
        $name = isset($_POST['name']) ? trim($_POST['name']): '';

        if($name == ""){
            $this->jsonOutput(array( "status" => "error", "message" => "Please provide Name") );
        }
        
        $query = RAILWAY_API_BASEPATH.'suggest-station/name/'.$name.'/apikey/'.RAILWAY_API_KEY;
        echo($query."\n");
        $result = file_get_contents($query);
        var_dump($result);
        if(!$result){
            $this->jsonOutput(array( "status" => "error", "message" => "No Result") );
        }
        $result = json_decode($result);
        $this->jsonOutput(["status"=>"success","result" => $result]);
    }

    public function pnrStatus(){
        $pnr = isset($_POST['pnr']) ? trim($_POST['pnr']): '';

        if($pnr == ""){
            $this->jsonOutput(array( "status" => "error", "message" => "Please provide PNR Number") );
        }
       
        $query = RAILWAY_API_BASEPATH.'pnr-status/pnr/'.$pnr.'/apikey/'.RAILWAY_API_KEY;
        $result = file_get_contents($query);

        if(!$result){
            $this->jsonOutput(array( "status" => "error", "message" => "No Result") );
        }
        $result = json_decode($result);
        $this->jsonOutput(["status"=>"success","result" => $result]);
    }

    public function route(){
        $train_number = isset($_POST['train_number']) ? trim($_POST['train_number']): '';

        if($train_number == ""){
            $this->jsonOutput(array( "status" => "error", "message" => "Please provide Train Number") );
        }
       
        $query = RAILWAY_API_BASEPATH.'route/train/'.$train_number.'/apikey/'.RAILWAY_API_KEY;
        $result = file_get_contents($query);

        if(!$result){
            $this->jsonOutput(array( "status" => "error", "message" => "No Result") );
        }
        $result = json_decode($result);
        $this->jsonOutput(["status"=>"success","result" => $result]);
    }

    public function liveStatus(){

        $train_number = isset($_POST['train_number']) ? trim($_POST['train_number']): '';
        $date = isset($_POST['date']) ? trim($_POST['date']): '';

        if($train_number == ""){
            $this->jsonOutput(array( "status" => "error", "message" => "Please provide Train Number") );
        }
        if($date == ""){
            $this->jsonOutput(array( "status" => "error", "message" => "Please provide Date") );
        }
        if (DateTime::createFromFormat($this->date_format, $date) == FALSE) {
           $this->jsonOutput(array( "status" => "error", "message" => "Please provide a valid date") );
        }
        $date    =date($this->date_format, strtotime($date));
       
        $query = RAILWAY_API_BASEPATH.'live/train/'.$train_number.'/date/'.$date.'/apikey/'.RAILWAY_API_KEY;
        $result = file_get_contents($query);

        if(!$result){
            $this->jsonOutput(array( "status" => "error", "message" => "No Result") );
        }
        $result = json_decode($result);
        $this->jsonOutput(["status"=>"success","result" => $result]);
    }

    public function trainBetweenStations(){
        $source = isset($_POST['source']) ? trim($_POST['source']): '';
        $destination = isset($_POST['destination']) ? trim($_POST['destination']): '';
        $date = isset($_POST['date']) ? trim($_POST['date']): '';

        if($source == ""){
            $this->jsonOutput(array( "status" => "error", "message" => "Please provide source") );
        }
        
        if($destination == ""){
            $this->jsonOutput(array( "status" => "error", "message" => "Please provide destination") );
        }
        
        if($date == ""){
            $this->jsonOutput(array( "status" => "error", "message" => "Please provide date") );
        }

        if (DateTime::createFromFormat($this->date_format, $date) == FALSE) {
           $this->jsonOutput(array( "status" => "error", "message" => "Please provide a valid date") );
        }
        $date    =date($this->date_format, strtotime($date));
       
        $query = RAILWAY_API_BASEPATH.'between/source/'.$source.'/dest/'.$destination.'/date/'.$date.'/apikey/'.RAILWAY_API_KEY;
        $result = file_get_contents($query);

        if(!$result){
            $this->jsonOutput(array( "status" => "error", "message" => "No Result") );
        }
        $result = json_decode($result);
        $this->jsonOutput(["status"=>"success","result" => $result]);
    }

}