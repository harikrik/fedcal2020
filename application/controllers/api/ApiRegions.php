<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ApiRegions extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->jsonInput();
		$this->load->model('Common_model');
	}

	public function index()
	{
		$sWhere = array(
			'is_active' => 1,
			'is_deleted' => 0,
			);
		$data = $this->Common_model->getAll('id, region', 'region', $sWhere, 0);
		$this->jsonOutput($data);
	}

}
