<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends MY_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	public function __construct(){
		parent::__construct();

		$this->load->helper('form');
		$this->load->library('form_validation');
		$this->load->model('User_model');
	}

	public function index(){
			//die("$login");
   		if( $this->loginCheck() ){
			$this->local_redirect('calendar');
		}
        $this->form_validation->set_rules('username', 'Username', 'required');
        $this->form_validation->set_rules('password', 'Password', 'required');

        if ($this->form_validation->run() === TRUE) {

			$username = $this->input->post('username');
			$password = $this->input->post('password');
			/* $remember = $this->input->post('remember'); */
			
            $login = $this->User_model->login($username, $password);

            if( !array_key_exists( 'error', $login ) ){
            	if( ($login->usertype == SUPERADMIN) || ($login->usertype == ADMIN) ){
            		$login->profile_pic = 'default.png';
            		/* if( $remember ){
	                	/* 10 days *
	                	$cookie_time = 60*60*24*10;
	                	$this->input->set_cookie('cookie_user_id', $login->user_id, $cookie_time);
	                } */
            		$this->setSession($login);
            		$this->local_redirect('calendar');
            	}
                else{
                	$this->session->set_flashdata('loginError', "You don't have permission to login!");
                	$this->local_redirect('login');
                }
            }
            else {
                $this->session->set_flashdata('loginError', $login['message']);
                $this->local_redirect('login');
            }
        }
        else{
        	if( isset( $_POST['username'] ) ){
				$this->session->set_flashdata('loginError', 'Provide Username and password!');
        	}
        }
		$data = array(
			'title' => 'Login'
		);
		$this->load->view('login', $data);
	}


	public function logout(){
		$this->session->sess_destroy();
		//$this->input->set_cookie('cookie_user_id', '');
        $this->local_redirect('login');
	}


	public function settings(){
		$data = array(
			'title'     => 'Settings',
			'page_name' => 'settings'
		);

		if( !$this->loginCheck() ){
			$this->local_redirect('login');
		}

		$user_id   = $this->session->userdata('uid');

		$userSettings = $this->User_model->userSettings($user_id);
		$data['userSettings'] = $userSettings;

		$this->changeSettings($user_id, $userSettings);
		$this->changePassword($user_id);

		$path_array = array(
			'User Settings' => ''
		);
		$data['breadcrumbs'] =  $this->breadcrumbs( $path_array );

		$this->display($data);
	}



	private function changeSettings($user_id, $userSettings){
		if($this->input->post('update_settings')){
			$this->form_validation->set_rules('first_name', 'First Name', 'required');
			$this->form_validation->set_rules('email', 'Email', 'valid_email');
			if ($this->form_validation->run() == TRUE){
				$data['first_name']    = $this->input->post('first_name');
				$data['middle_name']   = $this->input->post('middle_name');
				$data['last_name']     = $this->input->post('last_name');
				$data['email']         = $this->input->post('email');

				$data['profile_photo'] = $userSettings->profile_photo;


				$upload_data = $this->do_upload(PROFILE_IMG, 'profile_photo');

				if( isset( $upload_data['pic'] ) ){
					$data['profile_photo'] = $upload_data['pic'];
			        $this->session->unset_userdata('profile_pic');

			        $this->session->set_userdata('profile_pic', PROFILE_IMG.$data['profile_photo']);

				}
				$data['profile_photo'] = str_replace(PROFILE_IMG,"",$data['profile_photo']);

				$update_res = $this->User_model->updateSettings($user_id, $data);
				if( !array_key_exists( 'error', $update_res ) ){
					$this->session->unset_userdata('first_name' );
					$this->session->unset_userdata('middle_name' );
					$this->session->unset_userdata('last_name' );
					$this->session->unset_userdata('email' );

					$this->session->set_userdata('first_name', $data['first_name'] );
					$this->session->set_userdata('middle_name', $data['middle_name'] );
					$this->session->set_userdata('last_name', $data['last_name'] );
					$this->session->set_userdata('email', $data['email'] );
					$this->session->set_flashdata('success', $update_res['message']);
					$this->local_redirect('settings');
				}
				else{
					$this->session->set_flashdata('error', $update_res['message']);
					$this->localAdmin_redirect($this->curr_controller);
				}
				/*$data['profile_photo'] = $userSettings->profile_photo;

				$upload_config = array(
					'big_thumb_w' => 100,
					'big_thumb_h' => 100,
					'small_thumb_w' => 50,
					'small_thumb_h' => 50
					);
				$upload_data = $this->do_upload(PROFILE_IMG, 'profile_photo', $upload_config);
				if( isset( $upload_data['pic'] ) ){
					$data['profile_photo']  echo("$url is a valid URL");
			} else {
			    echo("$url is not a valid URL");
			}= $upload_data['pic'];
			  	}
				$data['profile_photo'] = str_replace(PROFILE_IMG,"",$data['profile_photo']);

				$update_res = $this->User_model->updateSettings($user_id, $data);
				if( !array_key_exists( 'error', $update_res ) ){
					$this->session->unset_userdata('first_name' );
					$this->session->unset_userdata('middle_name' );
					$this->session->unset_userdata('last_name' );
					$this->session->unset_userdata('email' );

					$this->session->set_userdata('first_name', $data['first_name'] );
					$this->session->set_userdata('middle_name', $data['middle_name'] );
					$this->session->set_userdata('last_name', $data['last_name'] );
					$this->session->set_userdata('email', $data['email'] );
					$this->local_redirect('settings');
				}*/
			}
		}
	}


	/*private function changePassword($user_id){
		if($this->input->post('doChangePassword')){
			$this->form_validation->set_rules('curr_password', 'Existing password', 'required');
            $this->form_validation->set_rules('password', 'New Password', 'required|matches[cpassword]|min_length[5]|max_length[12]');
			$this->form_validation->set_rules('cpassword', 'Confirm New password', 'required');
            if ($this->form_validation->run() == TRUE){
				$curr_password = $this->input->post('curr_password');
				$new_password  = $this->input->post('password');
                $changePassword = $this->User_model->changePassword($user_id, $curr_password, $new_password);
                if($changePassword){
                    $data['password_change_success'] = true;
                }else{
                    $data['current_password_does_not_match'] = true;
                }
            }
        }
	}*/


	private function changePassword($user_id){
		if($this->input->post('doChangePassword')){

			$this->form_validation->set_rules('curr_password', 'Existing password', 'required');
            $this->form_validation->set_rules('password', 'New Password', 'required|matches[cpassword]|min_length[5]|max_length[12]');
			$this->form_validation->set_rules('cpassword', 'Confirm New password', 'required');
            if ($this->form_validation->run() == TRUE){

				$curr_password = $this->input->post('curr_password');
				$new_password  = $this->input->post('password');

				 $update_res = $this->User_model->checkPassword($user_id, $curr_password);
				 $message='';

				if($update_res){
					$changePassword = $this->User_model->changePassword($user_id, $curr_password, $new_password);
				}
				else{
					$message='Wrong Password';
					$this->session->set_flashdata('error', $message);
				    $this->local_redirect('settings/');
				}
				 if($changePassword){
                    $this->session->set_flashdata('success', $changePassword['message']);
				    $this->local_redirect('settings/');
                }else{
                    $this->session->set_flashdata('error', $changePassword['message']);
				    $this->local_redirect('settings/');
                }
            }
        }
	}





	public function _check_phone($phone)
	{
	   	if(preg_match('/^(?:(?:\+|0{0,2})91(\s*[\-]\s*)?|[0]?)?[789]\d{9}$/',$phone))
			{
				return true;
			} else {
					$this->form_validation->set_message('_check_phone', '%s '.$phone.' is invalid format');
					return false;
			}
	}

/*function checkwebsiteurl($string_url)
 {
        $reg_exp = "@^(http\:\/\/|https\:\/\/)?([a-z0-9][a-z0-9\-]*\.)+[a-z0-9][a-z0-9\-]*$@i";
        if(preg_match($reg_exp, $string_url) == TRUE){
         return TRUE;
        }
        else{
         $this->form_validation->set_message('checkwebsiteurl', 'URL is invalid format');
         return FALSE;
        }
 }*/

 function validUrl($url)
		{
		    /*$pattern = "/^((ht|f)tp(s?)\:\/\/|~/|/)?([w]{2}([\w\-]+\.)+([\w]{2,5}))(:[\d]{1,5})?/";
		    if (!preg_match($pattern, $url))
		    {
		        return FALSE;
		    }*/

		    return TRUE;
		}

}
