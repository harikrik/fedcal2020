<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class AdminUsers extends MY_Controller {

	private $user_id = 0;
	private $school_id = 0;

	private $curr_controller = 'adminuser';
	private $curr_title = 'Users';

	public function __construct(){
		parent::__construct();

		if( !$this->loginCheck() ){
			$this->local_redirect('login');
		}

		$this->user_id   = $this->session->userdata('uid');

		$this->load->library('form_validation');
		$this->load->model('Common_model');
		$this->load->model('Customer_model');
		$this->load->model('User_model');
		
	}

	public function index(){
		$data = array(
			'title'     => $this->curr_title,
			'page_name' => $this->curr_controller.'/index',
			'page_type' => 'listing'
		);

		$path_array = array(
			'Dashboard' =>  'dashboard',
			$this->curr_title => ''
		);
		$data['breadcrumbs'] =  $this->breadcrumbs( $path_array );

		$this->display($data);
	}

	public function create(){
		$data = array(
			'title'     => 'Create '.$this->curr_title,
			'page_name' => $this->curr_controller.'/create',
			'page_type' => 'create'
		);

		$path_array = array(
			'Dashboard' =>  'dashboard',
			$this->curr_title => $this->curr_controller,
			'Create' => ''
		);


		$this->doCreate();
		$data['breadcrumbs'] =  $this->breadcrumbs( $path_array );
		$this->display($data);
	}

	private function doCreate(){

		if($this->input->post('create')){
			$this->form_validation->set_rules('first_name', 'First Name', 'required');
			$this->form_validation->set_rules('password', 'Password', 'required');
			$this->form_validation->set_rules('email', 'Email', 'valid_email');
			$this->form_validation->set_rules('username', 'Username', 'required|is_unique[user.username]');
			if ($this->form_validation->run() == TRUE){
				$data['first_name']    = $this->input->post('first_name');
				$data['middle_name']   = $this->input->post('middle_name');
				$data['last_name']     = $this->input->post('last_name');
				$data['email']         = $this->input->post('email');
				$data['username']         = $this->input->post('username');
				$data['password']         = $this->input->post('password');
				$data['password']  = password_hash($data['password'] , PASSWORD_DEFAULT);
				$data['profile_photo'] = $userSettings->profile_photo;

				$upload_data = $this->do_upload(PROFILE_IMG, 'profile_photo');

				if( isset( $upload_data['pic'] ) ){
					$data['profile_photo'] = $upload_data['pic'];
			        $this->session->unset_userdata('profile_pic');

			        $this->session->set_userdata('profile_pic', PROFILE_IMG.$data['profile_photo']);

				}
				$data['profile_photo'] = str_replace(PROFILE_IMG,"",$data['profile_photo']);
				$data['usertype_id'] = 2;
				$return_data = $this->Common_model->insert($data, 'user');
				if( $return_data ){
					$this->session->set_flashdata('success', $update_res['message']);
					$this->local_redirect($this->curr_controller);
				}
				else{
					$this->session->set_flashdata('error', $update_res['message']);
					$this->local_redirect($this->curr_controller);
				}
			}
			/*else{
				$this->session->set_flashdata('error', 'validation error');
				$this->local_redirect($this->curr_controller);
			}*/
		}
	}

	public function edit($user_id){
		$data = array(
			'title'     => 'Edit '.$this->curr_title,
			'page_name' => $this->curr_controller.'/edit',
			'page_type' => 'edit'
		);

		$path_array = array(
			'Dashboard' =>  'dashboard',
			$this->curr_title => $this->curr_controller,
			'Edit' => ''
		);
		$userSettings = $this->User_model->userSettings($user_id);
		

		
		if($userSettings){
			$data['userSettings'] = $userSettings;
			$this->changeSettings($user_id, $userSettings);
			$this->changePassword($user_id);
			/*$data['single_data'] = $userSettings;*/
			$data['breadcrumbs'] =  $this->breadcrumbs( $path_array );
			$this->display($data);
		}
		else{
			$this->session->set_flashdata('error', 'Sorry! The page you requested does not exist.');
			$this->local_redirect($this->curr_controller);
		}
	}

	private function changeSettings($user_id, $userSettings){
		if($this->input->post('update_settings')){
			$this->form_validation->set_rules('first_name', 'First Name', 'required');
			$this->form_validation->set_rules('email', 'Email', 'valid_email');
			if ($this->form_validation->run() == TRUE){
				$data['first_name']    = $this->input->post('first_name');
				$data['middle_name']   = $this->input->post('middle_name');
				$data['last_name']     = $this->input->post('last_name');
				$data['email']         = $this->input->post('email');

				$data['profile_photo'] = $userSettings->profile_photo;


				$upload_data = $this->do_upload(PROFILE_IMG, 'profile_photo');

				if( isset( $upload_data['pic'] ) ){
					$data['profile_photo'] = $upload_data['pic'];
			        $this->session->unset_userdata('profile_pic');

			        $this->session->set_userdata('profile_pic', PROFILE_IMG.$data['profile_photo']);

				}
				$data['profile_photo'] = str_replace(PROFILE_IMG,"",$data['profile_photo']);

				$update_res = $this->User_model->updateSettings($user_id, $data);
				if( !array_key_exists( 'error', $update_res ) ){
					$this->session->unset_userdata('first_name' );
					$this->session->unset_userdata('middle_name' );
					$this->session->unset_userdata('last_name' );
					$this->session->unset_userdata('email' );

					$this->session->set_userdata('first_name', $data['first_name'] );
					$this->session->set_userdata('middle_name', $data['middle_name'] );
					$this->session->set_userdata('last_name', $data['last_name'] );
					$this->session->set_userdata('email', $data['email'] );
					$this->session->set_flashdata('success', $update_res['message']);
					$this->local_redirect('settings');
				}
				else{
					$this->session->set_flashdata('error', $update_res['message']);
					$this->localAdmin_redirect($this->curr_controller);
				}
			}
		}
	}

	private function changePassword($user_id){
		if($this->input->post('doChangePassword')){

			$this->form_validation->set_rules('curr_password', 'Existing password', 'required');
            $this->form_validation->set_rules('password', 'New Password', 'required|matches[cpassword]|min_length[5]|max_length[12]');
			$this->form_validation->set_rules('cpassword', 'Confirm New password', 'required');
            if ($this->form_validation->run() == TRUE){

				$curr_password = $this->input->post('curr_password');
				$new_password  = $this->input->post('password');

				 $update_res = $this->User_model->checkPassword($user_id, $curr_password);
				 $message='';

				if($update_res){
					$changePassword = $this->User_model->changePassword($user_id, $curr_password, $new_password);
				}
				else{
					$message='Wrong Password';
					$this->session->set_flashdata('error', $message);
				    $this->local_redirect('settings/');
				}
				 if($changePassword){
                    $this->session->set_flashdata('success', $changePassword['message']);
				    $this->local_redirect('settings/');
                }else{
                    $this->session->set_flashdata('error', $changePassword['message']);
				    $this->local_redirect('settings/');
                }
            }
        }
	}

	private function update($single_data){
		if($this->input->post('update')){
			$this->form_validation->set_rules('name', 'Name', 'required');
			$this->form_validation->set_rules('mobile', 'Mobile', 'required');
			$this->form_validation->set_rules('email', 'Email', 'valid_email');
			$this->form_validation->set_rules('dob', 'DOB', 'required');
			if ($this->form_validation->run() == TRUE){
				$data['name'] = $this->input->post('name');
				$data['mobile'] = $this->input->post('mobile');
				$data['email'] = $this->input->post('email');
				$data['dob'] = $this->input->post('dob');
				$data['usertype_id'] = 2;
				$data['dob'] = str_replace('/', '-', $data['dob']);
				$data['dob'] = date("Y-m-d", strtotime($data['dob']));

				$id = $single_data->id;

				$update_res = $this->Common_model->update($data, $id, 'user');
				if( $data ){
					$this->session->set_flashdata('success', $update_res['message']);
					$this->local_redirect($this->curr_controller);
				}
				else{
					$this->session->set_flashdata('error', $update_res['message']);
					$this->local_redirect($this->curr_controller);
				}
			}
		}
	}

	public function details($id){
		$data = array(
			'title'     => 'Details '.$this->curr_title,
			'page_name' => $this->curr_controller.'/details',
			'page_type' => 'details'
		);

		$path_array = array(
			'Dashboard' =>  'dashboard',
			$this->curr_title => $this->curr_controller,
			'Details' => ''
		);
		$single_data = $this->User_model->userSettings($id);

		if($single_data){
			$data['single_data'] = $single_data;
			$data['id'] = $id;
			$data['breadcrumbs'] =  $this->breadcrumbs( $path_array );
			$this->display($data);
		}
		else{
			$this->session->set_flashdata('error', 'Sorry! The page you requested does not exist.');
			$this->local_redirect($this->curr_controller);
		}
	}

	public function delete($id){
		$data['is_deleted'] = 1;
		$deleted = $this->Common_model->update($data, $id, 'customer');
		if( $data ){
			$this->session->set_flashdata('success', 'Deleted Successfully!');
			$this->local_redirect($this->curr_controller);
		}
		else{
			$this->session->set_flashdata('error', $deleted['message']);
			$this->local_redirect($this->curr_controller);
		}
	}

	public function list_entries(){
		if( $this->input->post('iColumns')  ){
		  echo $this->loadDataAjax( $_POST );
		  exit;
		}
	}

	public function loadDataAjax( $inputArray = array() ) {

		$aColumns = array( 'username', 'first_name', 'email' );
		/* call common functions  */
		$filter_array = $this->our_filtering($aColumns, $inputArray);
		$sWhere =  $filter_array['sWhere'];
		$sLimit =  $filter_array['sLimit'];
		$sOrder = $filter_array['sOrder'];
		/* Total data */
		$total_data = $this->Customer_model->user_lists();
		/* Total data count */
		$iTotal = $total_data->num_rows();

		/*
		  * filtered data
		*/
    	$filtered_data = $this->Customer_model->user_lists($sWhere, $sOrder, $sLimit);

		$filter_count = $this->Customer_model->user_lists($sWhere, $sOrder)->num_rows();

		$j = 0;
		$rrResult = array();
		$row_num = intval( $inputArray[ 'iDisplayStart' ] );
		foreach ($filtered_data->result() as $row) {
			$row_num++;
			$rrResult[ $j ][ "row_num" ] = $row_num;
			$rrResult[ $j ][ "username" ]    = $row->username;
			$rrResult[ $j ][ "first_name" ]  = $row->first_name;
			$rrResult[ $j ][ "email" ]   = $row->email;

			$edit_btn = "<a class='text-yellow edit_btn' href='".$this->url_prefix.$this->curr_controller."/edit/".$row->id."'><i class='fa fa-pencil'></i></a>";
			$details_btn = "<a class=' text-green details_btn' href='".$this->url_prefix.$this->curr_controller."/details/".$row->id."'><i class='fa fa-eye'></i></a>";
			$del_btn = "<a class='text-red del_btn'><i class='fa fa-trash'></i></a>";
			$del_btn .= "<a class='hide del_action' href='".$this->url_prefix.$this->curr_controller."/delete/".$row->id."'></a>";

			$rrResult[ $j ][ "actions" ]     = '<div class="btn-group action_btns">'.$edit_btn.$details_btn.$del_btn.'</div>';
			$j++;

		}

		$output = array(
			    "sEcho"                => intval( $inputArray[ 'sEcho' ] ),
	            "iTotalRecords"        => (int)$iTotal,
	            "iTotalDisplayRecords" => (int)$filter_count,
	            "aaData"               => $rrResult
			  );
		//$this->dd($output);
		return json_encode( $output );
	}

}