<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Calendar extends MY_Controller {

	private $user_id = 0;

	private $curr_controller = 'calendar';
	private $curr_title = 'Calendar';

	public function __construct(){
		parent::__construct();
		/*print_r($this->session->userdata('username'));
		exit;*/
		if( !$this->loginCheck() ){
			$this->local_redirect('login');
		}
		$this->user_id   = $this->session->userdata('uid');
		$this->load->library('form_validation');
		$this->load->model('Calendar_model');
		$this->load->model('Common_model');
	}

	public function index(){
		$data = array(
			'title'     => 'Calendar',
			'page_name' => 'calendar/index',
			'page_type' => 'calendar'
		);
		$path_array = array(
			$this->curr_title => ''
		);
		$rWhere = array(
			"is_active" => 1,
			"is_deleted" => 0,
			);
		 $rOrder = "region";
		$regions = $this->Common_model->getAll('id, region', 'region', $rWhere, 0, $rOrder);
		$regions_a = [0 => 'Common'];
		foreach ($regions as $region) {
			$regions_a[ $region->id ] = $region->region;
		}
		$data['regions'] = $regions_a;
		$data['event_types'] = $this->Calendar_model->event_types();
		$data['breadcrumbs'] =  $this->breadcrumbs( $path_array );
		$this->display($data);
	}

	/**
	Region AJAX Begin
	 */

	/**
		ajax call for creating event type
		from calendar plugin
	 */
		public function add_event_type(){
			$name    = trim( $this->input->post('name') );
			$bgcolor = trim( $this->input->post('bgcolor') );
			if( $name && $bgcolor ){
				$data['name'] = $name;
				$data['borderColor'] = $bgcolor;
				$data['backgroundColor'] = $bgcolor;
				$result = $this->Calendar_model->add_event_type($data);
				echo $result;

			}
			else{
				echo 'fail';
			}
		}

	/**
		ajax call for deleting event type
		from calendar plugin
	 */
		public function dlt_event_type(){
			$id = trim( $this->input->post('id') );
			if( $id ){
				$result = $this->Calendar_model->dlt_event_type($id);
			}
		}

	/**
		ajax call for creating event
		from calendar plugin
	 */
		public function add_event(){
			$title    = trim( $this->input->post('title') );
			$bgcolor = trim( $this->input->post('bgcolor') );
			$start = trim( $this->input->post('start') );
			$end = trim( $this->input->post('end') );
			$region_id = trim( $this->input->post('region') );
			$event_type = trim( $this->input->post('event_type') );
			if( $title && $bgcolor && $event_type ){
				$data['title'] = $title;
				$data['borderColor'] = $bgcolor;
				$data['backgroundColor'] = $bgcolor;
				$data['event_type'] = $event_type;
				$data['start_time'] = $start;
				$data['end_time'] = $end;
				$data['region_id'] = (int)$region_id;
				$result = $this->Calendar_model->add_event($data);

				if( $result ){
					/* send notification to all users */
					//$this->sendPushNotification('event_create', $title, $start);
					return $result;
				}
				return 0;
			}
		}

	/**
		ajax call for removing event
		from calendar plugin
	 */
		public function remove_event(){
			$id = trim( $this->input->post('id') );
			if( $id ){
				$result = $this->Calendar_model->delete_event($id);
			}
		}

	/**
		ajax call for updating event
		from calendar plugin
	 */
		public function update_event(){
			$title    = trim( $this->input->post('title') );
			$id = trim( $this->input->post('id') );
			$start = trim( $this->input->post('start') );
			$end = trim( $this->input->post('end') );
			if( $title && $id ){
				$data['title'] = $title;
				$data['start_time'] = $start;
				$data['end_time'] = $end;
				$result = $this->Calendar_model->update_event($id, $data);
				
				if( $result ){
					$event_typeObj = $this->db->query("
					SELECT et.name 
					FROM event_types et 
					INNER JOIN events e ON et.id = e.event_type
					WHERE e.id = $id
					")->result();

					if( $event_typeObj ){
						$type = $event_typeObj[0]->name;
						/* send notification to all users */
						/*$event_date = date("F j, Y", strtotime($start));
						$message = "New $type '$title' added on $event_date";
						$this->sendPushNotification($type, $message, $start);*/
					}	
					
				}
			}
		}

	/**
		ajax call for listing all events
		from calendar plugin
	 */
		public function lists(){
			$start = trim( $this->input->post('start') );
			$end= trim( $this->input->post('end') );
			$region= trim( $this->input->post('region') );
			$region_id = (int)$region;

			$data = $this->Calendar_model->events($start, $end, $region_id);
			$this->jsonOutput($data);
		}

	/**
	Region AJAX End
	 */


	/**
		Push Notification
		Send to whole
	 */
	private function sendPushNotification($type, $message, $time){
		$this->load->model('Gcm_model');
		$sWhere = "";

        $token_obj = $this->Gcm_model->getWholeGcmTokens($sWhere);
		$extra_data = [];
		$iOSregistrationIds = [];
		$registrationIds = [];
		
		if($token_obj){
		    foreach ($token_obj as $token_a) {
		        switch( $token_a->device_os ){
		    		case 'ios':
		    			$iOSregistrationIds[] = $token_a->gcm_token;
		    			break;
		    		case 'android':
		    			$registrationIds[] = $token_a->gcm_token;
		    			break;
		    	}
		    }
		}
		if( $iOSregistrationIds ){
		    $this->sendPushNotif($iOSregistrationIds, $message, $time, $type, $extra_data, 'ios');
		}
		if( $registrationIds ){
		    $this->sendPushNotif($registrationIds, $message, $time, $type, $extra_data, 'android');
		}
		$data = array(
				'message'    => $message, 
				'time'       => $time.' 00:00:00', 
				'type'       => $type
		    	);
	    $this->Common_model->insert($data, 'notification');
    }

}