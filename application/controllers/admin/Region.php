<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Region extends MY_Controller {

	private $user_id = 0;
	private $school_id = 0;

	private $curr_controller = 'region';
	private $curr_title = 'Region';

	public function __construct(){
		parent::__construct();

		if( !$this->loginCheck() ){
			$this->local_redirect('login');
		}

		$this->user_id   = $this->session->userdata('uid');
		$this->school_id = $this->session->userdata('school_id');

		$this->load->library('form_validation');
		$this->load->model('Common_model');
	}

	public function index(){
		$data = array(
			'title'     => $this->curr_title,
			'page_name' => $this->curr_controller.'/index',
			'page_type' => 'listing'
		);

		$path_array = array(
			'Dashboard' =>  'dashboard',
			$this->curr_title => ''
		);
		$data['breadcrumbs'] =  $this->breadcrumbs( $path_array );

		$this->display($data);
	}

	public function create(){
		$data = array(
			'title'     => 'Create '.$this->curr_title,
			'page_name' => $this->curr_controller.'/create',
			'page_type' => 'create'
		);

		$path_array = array(
			'Dashboard' =>  'dashboard',
			$this->curr_title => $this->curr_controller,
			'Create' => ''
		);


		$this->doCreate();
		$data['breadcrumbs'] =  $this->breadcrumbs( $path_array );
		$this->display($data);
	}

	private function doCreate(){
		if($this->input->post('create')){
			$this->form_validation->set_rules('group_name', 'Group Name', 'required');
			if ($this->form_validation->run() == TRUE){
				$data['group_name'] = $this->input->post('group_name');
				$update_res = $this->ChatGroup_model->create($this->school_id, $data);
				if( !array_key_exists( 'error', $update_res ) ){
					$this->session->set_flashdata('success', $update_res['message']);
					$this->local_redirect($this->curr_controller);
				}
				else{
					$this->session->set_flashdata('error', $update_res['message']);
					$this->local_redirect($this->curr_controller);
				}
			}
		}
	}

	public function edit($id){
		$data = array(
			'title'     => 'Edit '.$this->curr_title,
			'page_name' => $this->curr_controller.'/edit',
			'page_type' => 'edit'
		);

		$path_array = array(
			'Dashboard' =>  'dashboard',
			$this->curr_title => $this->curr_controller,
			'Edit' => ''
		);
		$single_data = $this->ChatGroup_model->single_data($this->school_id, $id);
		if($single_data){
			$this->update($single_data);
			$data['single_data'] = $single_data;
			$data['group_id'] = $single_data->id;
			$data['class_list'] = $this->SchoolClass_model->classList($this->user_id, $this->school_id);
			$data['group_members'] = $this->ChatGroup_model->groupMembers($this->school_id, $id);
			$data['breadcrumbs'] =  $this->breadcrumbs( $path_array );
			$this->display($data);
		}
		else{
			$this->session->set_flashdata('error', 'Sorry! The page you requested does not exist.');
			$this->local_redirect($this->curr_controller);
		}
	}

	private function update($single_data){
		if($this->input->post('update')){
			$this->form_validation->set_rules('group_name', 'Group Name', 'required');
			if ($this->form_validation->run() == TRUE){
				$data['group_name']    = $this->input->post('group_name');
				$data['id'] = $single_data->id;

				$update_res = $this->ChatGroup_model->update($this->school_id, $data);
				if( !array_key_exists( 'error', $update_res ) ){
					$this->session->set_flashdata('success', $update_res['message']);
					$this->local_redirect($this->curr_controller);
				}
				else{
					$this->session->set_flashdata('error', $update_res['message']);
					$this->local_redirect($this->curr_controller);
				}
			}
		}
	}

	public function details($id){
		$data = array(
			'title'     => 'Details '.$this->curr_title,
			'page_name' => $this->curr_controller.'/details',
			'page_type' => 'details'
		);

		$path_array = array(
			'Dashboard' =>  'dashboard',
			$this->curr_title => $this->curr_controller,
			'Details' => ''
		);
		$single_data = $this->ChatGroup_model->single_data($this->school_id, $id);

		if($single_data){
			$data['single_data'] = $single_data;
			$data['group_members'] = $this->ChatGroup_model->groupMembers($this->school_id, $id);
			$data['breadcrumbs'] =  $this->breadcrumbs( $path_array );
			$this->display($data);
		}
		else{
			$this->session->set_flashdata('error', 'Sorry! The page you requested does not exist.');
			$this->local_redirect($this->curr_controller);
		}
	}

	public function delete($id){
		$data['id'] = $id;
		$data['is_deleted'] = 1;
		$deleted = $this->ChatGroup_model->update($this->school_id, $data);
		if( !array_key_exists( 'error', $deleted ) ){
			$this->session->set_flashdata('success', 'Deleted Successfully!');
			$this->local_redirect($this->curr_controller);
		}
		else{
			$this->session->set_flashdata('error', $deleted['message']);
			$this->local_redirect($this->curr_controller);
		}
	}

	public function list_entries(){
		if( $this->input->post('iColumns')  ){
		  echo $this->loadDataAjax( $_POST );
		  exit;
		}
	}

	public function loadDataAjax( $inputArray = array() ) {

		$aColumns = array( 'gc.group_name' );
		/* call common functions  */
		$filter_array = $this->our_filtering($aColumns, $inputArray);
		$sWhere =  $filter_array['sWhere'];
		$sLimit =  $filter_array['sLimit'];
		$sOrder = $filter_array['sOrder'];
		/* Total data */
		$total_data = $this->ChatGroup_model->lists( $this->school_id);
		/* Total data count */
		$iTotal = $total_data->num_rows();

		/*
		  * filtered data
		*/
    	$filtered_data = $this->ChatGroup_model->lists($this->school_id, $sWhere, $sOrder, $sLimit);


		$filter_count = $this->ChatGroup_model->lists($this->school_id, $sWhere, $sOrder)->num_rows();

		$j = 0;
		$rrResult = array();
		$row_num = intval( $inputArray[ 'iDisplayStart' ] );
		foreach ($filtered_data->result() as $row) {
			$row_num++;
			$rrResult[ $j ][ "row_num" ]   = $row_num;
			$rrResult[ $j ][ "group_name" ]       = $row->group_name;

			$edit_btn = "<a class='text-yellow edit_btn' href='".$this->url_prefix.$this->curr_controller."/edit/".$row->id."'><i class='fa fa-pencil'></i></a>";
			$details_btn = "<a class=' text-green details_btn' href='".$this->url_prefix.$this->curr_controller."/details/".$row->id."'><i class='fa fa-eye'></i></a>";
			$del_btn = "<a class='text-red del_btn'><i class='fa fa-trash'></i></a>";
			$del_btn .= "<a class='hide del_action' href='".$this->url_prefix.$this->curr_controller."/delete/".$row->id."'></a>";

			$rrResult[ $j ][ "actions" ]     = '<div class="btn-group action_btns">'.$edit_btn.$details_btn.$del_btn.'</div>';
			$j++;

		}

		$output = array(
			    "sEcho"                => intval( $inputArray[ 'sEcho' ] ),
	            "iTotalRecords"        => (int)$iTotal,
	            "iTotalDisplayRecords" => (int)$filter_count,
	            "aaData"               => $rrResult
			  );
		//$this->dd($output);
		return json_encode( $output );
	}

}