<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Appuser extends MY_Controller {

	private $user_id = 0;
	private $school_id = 0;

	private $curr_controller = 'appuser';
	private $curr_title = 'App User';

	public function __construct(){
		parent::__construct();

		if( !$this->loginCheck() ){
			$this->local_redirect('login');
		}

		$this->user_id   = $this->session->userdata('uid');
		$this->school_id = $this->session->userdata('school_id');

		$this->load->library('form_validation');
		$this->load->model('Common_model');
		$this->load->model('Customer_model');
		
	}

	public function index(){
		$data = array(
			'title'     => $this->curr_title,
			'page_name' => $this->curr_controller.'/index',
			'page_type' => 'listing'
		);

		$path_array = array(
			'Dashboard' =>  'dashboard',
			$this->curr_title => ''
		);
		$data['breadcrumbs'] =  $this->breadcrumbs( $path_array );

		$this->display($data);
	}

	public function details($id){
		$data = array(
			'title'     => 'Details '.$this->curr_title,
			'page_name' => $this->curr_controller.'/details',
			'page_type' => 'details'
		);

		$path_array = array(
			'Dashboard' =>  'dashboard',
			$this->curr_title => $this->curr_controller,
			'Details' => ''
		);
		$single_data = $this->Common_model->getDetails($id, 'customer');

		if($single_data){
			$data['single_data'] = $single_data;
			$data['breadcrumbs'] =  $this->breadcrumbs( $path_array );
			$this->display($data);
		}
		else{
			$this->session->set_flashdata('error', 'Sorry! The page you requested does not exist.');
			$this->local_redirect($this->curr_controller);
		}
	}

	public function notify($customer_id){
		$data = array(
			'title'     => 'Details '.$this->curr_title,
			'page_name' => $this->curr_controller.'/notify',
			'page_type' => 'notify'
		);

		$path_array = array(
			'Dashboard' =>  'dashboard',
			$this->curr_title => $this->curr_controller,
			'Details' => ''
		);
		$single_data = $this->Common_model->getDetails($customer_id, 'customer');

		if($single_data){
			$this->doNotify($customer_id);
			$data['single_data'] = $single_data;
			$data['breadcrumbs'] =  $this->breadcrumbs( $path_array );
			$this->display($data);
		}
		else{
			$this->session->set_flashdata('error', 'Sorry! The page you requested does not exist.');
			$this->local_redirect($this->curr_controller);
		}
	}

	private function doNotify($customer_id){

		if($this->input->post('send')){
			$this->form_validation->set_rules('message', 'Title', 'required');
			if ($this->form_validation->run() == TRUE){
				$data['message']    = $this->input->post('message');
				$data['description']    = $this->input->post('description');

				$upload_data = $this->do_upload(NOTIFICATION_IMG, 'image');
				if( isset( $upload_data['pic'] ) ){
					$data['image'] = $upload_data['pic'];
				}
				$data['image'] = str_replace(NOTIFICATION_IMG,"",$data['image']);
				$data['customer_id'] = $customer_id;
				$data['type'] = 'custom';
				$return_data = $this->Common_model->insert($data, 'notification');
				if( $return_data ){
					$this->sendUserPushNotification($data['type'], $data['message'], '', $customer_id);
					$this->session->set_flashdata('success', $update_res['message']);
					$this->local_redirect($this->curr_controller);
				}
				else{
					$this->session->set_flashdata('error', $update_res['message']);
					$this->local_redirect($this->curr_controller);
				}
			}
			/*else{
				$this->session->set_flashdata('error', 'validation error');
				$this->local_redirect($this->curr_controller);
			}*/
		}
	}

	/**
		Push Notification
		Send to user
	 */
	private function sendUserPushNotification($type, $message, $time, $customer_id){
		$this->load->model('Gcm_model');
		$sWhere = "";

        $token_obj = $this->Gcm_model->getuserGcmTokens($customer_id, $sWhere);
		$extra_data = [];
		$iOSregistrationIds = [];
		$registrationIds = [];
		
		if($token_obj){
		    foreach ($token_obj as $token_a) {
		        switch( $token_a->device_os ){
		    		case 'ios':
		    			$iOSregistrationIds[] = $token_a->gcm_token;
		    			break;
		    		case 'android':
		    			$registrationIds[] = $token_a->gcm_token;
		    			break;
		    	}
		    }
		}
		if( $iOSregistrationIds ){
		    $this->sendPushNotif($iOSregistrationIds, $message, $time, $type, $extra_data, 'ios');
		}
		if( $registrationIds ){
		    $this->sendPushNotif($registrationIds, $message, $time, $type, $extra_data, 'android');
		}
    }

	public function list_entries(){
		if( $this->input->post('iColumns')  ){
		  echo $this->loadDataAjax( $_POST );
		  exit;
		}
	}

	public function loadDataAjax( $inputArray = array() ) {

		$aColumns = array( 'name', 'mobile', 'email', 'dob' );
		/* call common functions  */
		$filter_array = $this->our_filtering($aColumns, $inputArray);
		$sWhere =  $filter_array['sWhere'];
		$sLimit =  $filter_array['sLimit'];
		$sOrder = $filter_array['sOrder'];
		/* Total data */
		$total_data = $this->Customer_model->lists();
		/* Total data count */
		$iTotal = $total_data->num_rows();

		/*
		  * filtered data
		*/
    	$filtered_data = $this->Customer_model->lists($sWhere, $sOrder, $sLimit);

		$filter_count = $this->Customer_model->lists($sWhere, $sOrder)->num_rows();
		$j = 0;
		$rrResult = array();
		$row_num = intval( $inputArray[ 'iDisplayStart' ] );
		foreach ($filtered_data->result() as $row) {
			$row_num++;
			$rrResult[ $j ][ "row_num" ] = $row_num;
			$rrResult[ $j ][ "name" ]    = $row->name;
			$rrResult[ $j ][ "mobile" ]  = $row->mobile;
			$rrResult[ $j ][ "email" ]   = $row->email;
			$rrResult[ $j ][ "dob" ]     = $row->dob;

			/*$edit_btn = "<a class='text-yellow edit_btn' href='".$this->url_prefix.$this->curr_controller."/edit/".$row->id."'><i class='fa fa-pencil'></i></a>";*/
			$details_btn = "<a class=' text-green details_btn' href='".$this->url_prefix.$this->curr_controller."/details/".$row->id."'><i class='fa fa-eye'></i></a>";
			$send_msg_btn = "<a class='' href='".$this->url_prefix.$this->curr_controller."/notify/".$row->id."'>Send Message</a>";
			/*$del_btn = "<a class='text-red del_btn'><i class='fa fa-trash'></i></a>";
			$del_btn .= "<a class='hide del_action' href='".$this->url_prefix.$this->curr_controller."/delete/".$row->id."'></a>";*/

			$rrResult[ $j ][ "actions" ]     = '<div class="btn-group action_btns">'.$details_btn.$send_msg_btn.'</div>';
			$j++;

		}

		$output = array(
			    "sEcho"                => intval( $inputArray[ 'sEcho' ] ),
	            "iTotalRecords"        => (int)$iTotal,
	            "iTotalDisplayRecords" => (int)$filter_count,
	            "aaData"               => $rrResult
			  );
		//$this->dd($output);
		return json_encode( $output );
	}

}