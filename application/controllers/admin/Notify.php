<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Notify extends MY_Controller {

	private $curr_controller = 'notify';
	private $curr_title = 'Notify';

	public function __construct(){
		parent::__construct();

		if( !$this->loginCheck() ){
			$this->local_redirect('login');
		}

		$this->user_id   = $this->session->userdata('uid');

		$this->load->library('form_validation');
		$this->load->model('Common_model');
	}

	public function index(){
		$data = array(
			'title'     => $this->curr_title,
			'page_name' => $this->curr_controller.'/index',
			'page_type' => 'notify'
		);
		
		$path_array = array(
			'Dashboard' =>  'dashboard',
			$this->curr_title => $this->curr_controller
		);
	
		$this->doNotify();
		$data['breadcrumbs'] =  $this->breadcrumbs( $path_array );
		$this->display($data);
	}

	private function doNotify(){

		if($this->input->post('send')){
			$this->form_validation->set_rules('message', 'Title', 'required');
			$this->form_validation->set_rules('type', 'User Type', 'required');
			if ($this->form_validation->run() == TRUE){
				$user_type    = $this->input->post('type');
				$data['message']    = $this->input->post('message');
				$data['description']    = $this->input->post('description');
				$data['image'] = '';
				$upload_data = $this->do_upload(NOTIFICATION_IMG, 'image');

				if( isset( $upload_data['pic'] ) ){
					$data['image'] = $upload_data['pic'];
				}
				$data['image'] = str_replace(NOTIFICATION_IMG,"",$data['image']);
				$data['type'] = 'custom';
				$return_data = $this->Common_model->insert($data, 'notification');
				
				if( $return_data ){
					$this->sendUserPushNotification($data['type'], $user_type, $data['message']);
					$this->session->set_flashdata('success', $update_res['message']);
					$this->local_redirect($this->curr_controller);
				}
				else{
					$this->session->set_flashdata('error', $update_res['message']);
					$this->local_redirect($this->curr_controller);
				}
			}
		}
	}

	/**
		Push Notification
		Send to user
	 */
	private function sendUserPushNotification($type, $user_type, $message){
		$this->load->model('Gcm_model');
		$sWhere = "";
		
		switch($user_type){
			case 'all':
				$this->tokens($sWhere, $type, $message);
				$this->tokens($sWhere, $type, $message, 'ios');
			break;
			case 'android':
				$this->tokens($sWhere, $type, $message);
			break;
			case 'ios':
				$this->tokens($sWhere, $type, $message, 'ios');
			break;
		}
	}

	private function tokens($sWhere = "", $type, $message, $device_os = 'android'){
		$sWhere .= " AND device_os = '$device_os' ";
		$token_count = $this->Gcm_model->wholeGcmTokenQuery($sWhere)->num_rows();
		$time = '';
		if($token_count > 0){
			$offset = 100;
			for( $i = 0; $i <= $token_count; $i = $i+$offset ){
				$registrationIds = [];
				$limit = " LIMIT $i, $offset ";
				$token_obj = $this->Gcm_model->wholeGcmTokenQuery($sWhere, $limit)->result();
				if($token_obj){
					foreach ($token_obj as $token_a) {
						$registrationIds[] = $token_a->gcm_token;
					}
					$extra_data = [];
					$this->sendPushNotif($registrationIds, $message, $time, $type, $extra_data, $device_os);
				}
			}
		}
		
	}

}