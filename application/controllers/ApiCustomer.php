<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ApiCustomer extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->jsonInput();
		$this->load->model('User_model');
		$this->load->model('AppHome_model');
		$this->load->model('Common_model');
	}

	public function register()
	{
		echo("0");
		$country_code = isset($_POST['country_code']) ? trim($_POST['country_code']) : '';
		$mobile = isset($_POST['mobile']) ? trim($_POST['mobile']) : '';
		echo($mobile);
		if( !$country_code ){
			$data = array(
					'status' => 'error',
					'message' => 'Provide a Country Code.'
				);
			$this->jsonOutput($data);
		}
		if( !$mobile ){
			$data = array(
					'status' => 'error',
					'message' => 'Provide a mobile number.'
				);
			$this->jsonOutput($data);
		}

		$cWhere = array('mobile' => $mobile);
		$data = $this->Common_model->getAll('id', 'customer', $cWhere);
		echo("0");
		if( $data ){
			echo("1");
			$customer_id = $data[0]->id;
		}
		else{
			/* insert customer */
			echo("2");
			$iData = array('mobile' => $mobile);
			$customer_id = $this->Common_model->insert($iData, 'customer');
			if( !$customer_id ){
				$data = array(
					'status' => 'error',
					'message' => 'Sorry.. Some error occurred.'
				);
				$this->jsonOutput($data);
			}
		}
		echo("3");
		$otp = mt_rand(1000, 9999);
		$oiData = array(
			'customer_id' => $customer_id,
			'otp' => $otp
			);
		$otp_id = $this->Common_model->insert($oiData, 'otp');
		echo("4");
		$data = array(
			'status' => 'success'
			);
		$country_code1 = str_replace(" ","",$country_code);
		$country_code2 = str_replace("+","",$country_code1);
		$country_code3 = (int)str_replace("-","",$country_code2);

		$mobile = $this->remove_countrycode($mobile);

		$message = "OTP for Federal Calendar Login is $otp";
		if($country_code3 == "91"){
			$mobile = $country_code3.$mobile;
			$this->sendOTP($message, $mobile);
		}
		else{
			$mobile = $country_code3.$mobile;
			$this->sendIntlOTP($message, $mobile);
		}		
		$this->jsonOutput($data);
	}

	private function remove_countrycode($mobile){
		$country_code_a = ["+7840","+93","+355","+213","+1684","+376","+244","+1264","+1268","+54","+374","+297","+247","+61","+672","+43","+994","+1242","+973","+880","+1246","+1268","+375","+32","+501","+229","+1441","+975","+591","+387","+267","+55","+246","+1284","+673","+359","+226","+257","+855","+237","+1","+238","+345","+236","+235","+56","+86","+61","+61","+57","+269","+242","+243","+682","+506","+385","+53","+599","+537","+420","+45","+246","+253","+1767","+1809","+670","+56","+593","+20","+503","+240","+291","+372","+251","+500","+298","+679","+358","+33","+596","+594","+689","+241","+220","+995","+49","+233","+350","+30","+299","+1473","+590","+1671","+502","+224","+245","+595","+509","+504","+852","+36","+354","+91","+62","+98","+964","+353","+972","+39","+225","+1876","+81","+962","+77","+254","+686","+965","+996","+856","+371","+961","+266","+231","+218","+423","+370","+352","+853","+389","+261","+265","+60","+960","+223","+356","+692","+596","+222","+230","+262","+52","+691","+1808","+373","+377","+976","+382","+1664","+212","+95","+264","+674","+977","+31","+599","+1869","+687","+64","+505","+227","+234","+683","+672","+850","+1670","+47","+968","+92","+680","+970","+507","+675","+595","+51","+63","+48","+351","+1787","+974","+262","+40","+7","+250","+685","+378","+966","+221","+381","+248","+232","+65","+421","+386","+677","+27","+500","+82","+34","+94","+249","+597","+268","+46","+41","+963","+886","+992","+255","+66","+670","+228","+690","+676","+1868","+216","+90","+993","+1649","+688","+1340","+256","+380","+971","+44","+1","+598","+998","+678","+58","+84","+1808","+681","+967","+260","+255","+263"];

		foreach ($country_code_a as $item) {
			$mobile = trim($mobile, $item);
		}
		return $mobile;
	}

	public function profile(){
		$mobile = isset($_POST['mobile']) ? trim($_POST['mobile']) : '';

		$error_data = array();
		if( !$mobile ){
			$error_data = array(
					'status' => 'error',
					'message' => 'Provide a mobile number.'
				);
		}

		if($error_data){
			$this->jsonOutput($error_data);
		}

		$cWhere = array('mobile' => $mobile);
		$data = $this->Common_model->getAll('id, name, dob, email', 'customer', $cWhere);
		if( !$data ){
			$data = array(
					'status' => 'error',
					'message' => 'This Number is not registered.'
				);
			$this->jsonOutput($data);
		}
		else{
			$this->jsonOutput($data);
		}
	}

	public function updateCustomer(){
		$mobile = isset($_POST['mobile']) ? trim($_POST['mobile']) : '';
		$name   = isset($_POST['name']) ? trim($_POST['name']) : '';
		$email  = isset($_POST['email']) ? trim($_POST['email']) : '';
		$dob    = isset($_POST['dob']) ? trim($_POST['dob']) : '';

		$error_data = array();
		if( !$mobile ){
			$error_data = array(
					'status' => 'error',
					'message' => 'Provide a mobile number.'
				);
		}
		/*else if( !$name ){
			$error_data = array(
					'status' => 'error',
					'message' => 'Provide a name.'
				);
		}
		else if( !$email ){
			$error_data = array(
					'status' => 'error',
					'message' => 'Provide an email.'
				);
		}*/

		if( $dob ){
			$valid_date = $this->checkDate($dob);

			if( !$valid_date ){
	            $error_data = array(
						'status' => 'error',
						'message' => "Date should be in YYYY-MM-DD format."
					);
	        }
		}

		if($error_data){
			$this->jsonOutput($error_data);
		}

		$cWhere = array('mobile' => $mobile);
		$data = $this->Common_model->getAll('id', 'customer', $cWhere);
		if( !$data ){
			$data = array(
					'status' => 'error',
					'message' => 'This Number is not registered.'
				);
			$this->jsonOutput($data);
		}
		else{
			$customer_id = $data[0]->id;
			$data = array(
				'name' => $name,
				'email' => $email,
				'dob' => $dob,
				);
			$this->Common_model->update($data , $customer_id, "customer");
			$data = array(
					'status' => 'success',
				);
			$this->jsonOutput($data);
		}
	}

	public function activate(){
		$mobile = isset($_POST['mobile']) ? trim($_POST['mobile']) : '';
		$otp = isset($_POST['otp']) ? trim($_POST['otp']) : '';

		if( !$mobile ){
			$data = array(
					'status' => 'error',
					'message' => 'Provide a mobile number.'
				);
			$this->jsonOutput($data);
		}
		if( !$otp ){
			$data = array(
					'status' => 'error',
					'message' => 'Provide an otp.'
				);
			$this->jsonOutput($data);
		}
		$cWhere = array('mobile' => $mobile);
		$data = $this->Common_model->getAll('id', 'customer', $cWhere);
		if( !$data ){
			$data = array(
					'status' => 'error',
					'message' => 'This Number is not registered.'
				);
			$this->jsonOutput($data);
		}
		$customer_id = $data[0]->id;
		$oWhere = array(
			'customer_id' => $customer_id,
			'otp' => $otp,
			);
		$data = $this->Common_model->getAll('otp', 'otp', $oWhere);
		if( !$data ){
			$data = array(
				'status' => 'error',
				'message' => 'OTP is not valid'
			);
			$this->jsonOutput($data);
		}
		$data = $this->Common_model->update(array('is_active' => 1), $customer_id, 'customer');
		$data = array(
			'status' => 'success',
			'message' => 'Registration Completed'
		);
		$this->jsonOutput($data);
		
	}

	public function notification(){
		$mobile = isset($_POST['mobile']) ? trim($_POST['mobile']) : '';
		$page   = isset($_POST['page']) ? $_POST['page'] : 1;
		$page  = ( $page != 0 ) ? $page : 1;
		 $offset = 20;

		$error_data = array();
		if( !$mobile ){
			$error_data = array(
					'status' => 'error',
					'message' => 'Provide a mobile number.'
				);
		}
		if($error_data){
			$this->jsonOutput($error_data);
		}


		$cWhere = array('mobile' => $mobile);
		$data = $this->Common_model->getAll('id', 'customer', $cWhere);
		if( !$data ){
			$data = array(
					'status' => 'error',
					'message' => 'This Number is not registered.'
				);
			$this->jsonOutput($data);
		}
		$customer_id = $data[0]->id;

		
		$this->load->model('Customer_model');
		$sWhere = "";
		if( $page == 1 ){
			$sLimit = " LIMIT $offset ";
		}
		else{
			$start = ($page - 1) * $offset;
			$sLimit = " LIMIT $start, $offset ";
		}

		$data = $this->Customer_model->notification($sWhere, " ORDER BY id DESC ", $sLimit, $customer_id)->result();
		$this->jsonOutput($data);
	}

	public function addGcmToken()
	{
		$data = array();
		$mobile         = isset($_POST['mobile']) ? $_POST['mobile'] : '';
		$gcm_token = isset($_POST['gcm_token']) ? $_POST['gcm_token'] : '';
		/*$device_id       = isset($_POST['device_id']) ? $_POST['device_id'] : '';*/
		if( $mobile == '' ){
			$data = array( 'status' => 'fail', 'message' => 'Provide Valid Mobile Number!' );
		}
		else if( $gcm_token == '' ){
			$data = array( 'status' => 'fail', 'message' => 'Provide Token!' );
		}
		/*else if( $device_id == '' ){
			$data = array( 'status' => 'fail', 'message' => 'Provide Device Id!' );
		}*/
        else{
        	$cWhere = array('mobile' => $mobile);
			$data = $this->Common_model->getAll('id', 'customer', $cWhere);
			if( !$data ){
				$data = array(
						'status' => 'error',
						'message' => 'This Number is not registered.'
					);
				$this->jsonOutput($data);
			}
			$customer_id = $data[0]->id;

        	$existing_gcmUser = $this->User_model->existingGcmToken($gcm_token);
	        if( $existing_gcmUser ){
	            /* update */
	            $data = $this->User_model->updateGCM($existing_gcmUser->id, $customer_id, $gcm_token);
	        }
	        else{
	            /* insert */
	            $ins_data = array(
					"customer_id"   => $customer_id,
					/*"device_id" => $device_id,*/
					"gcm_token" => $gcm_token
	            	);
	            $insert_id = $this->User_model->insertGCM($ins_data);
	            if( $insert_id ){
		        	$data = array( 'status' => 'success', 'data' => "token created." );
		        }
		        else{
		        	$data = array( 'status' => 'fail', 'data' => "Some error occurred." );
		        }
	        }

        }
        $this->jsonOutput($data);
    }

    private function checkDate($date)
    {

        $arr=explode("-",$date); // splitting the array
        if( count($arr) == 3 ){
            $yy = (int)$arr[0];
            $mm = (int)$arr[1];
            $dd = (int)$arr[2];
            $mm = $this->padZero($mm);
            $dd = $this->padZero($dd);
            if( in_array(0, array($yy, $mm, $dd)) ){
                return false;
            }
            if(!checkdate($yy,$mm,$dd)){
                return $yy.'-'.$this->padZero($mm).'-'.$this->padZero($dd);
            }
            else{
                return false;
            }
        }
        else{
            return false;
        }
    }

    private function padZero($str){
    	$strlen = strlen($str);
    	if($strlen == 1){
    		return '0'.$str;
    	}
    	return $str;
    }



}