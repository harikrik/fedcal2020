<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class Fileupload {

    public $imgType_a = array(
        'image/gif', 'image/jpeg', 'image/jpg', 'image/png'
        );

    public function singleImageUpload($upload_path, $form_file_name, $upload_config = []){
    	$big_thumb_w = isset( $upload_config['big_thumb_w'] ) ? $upload_config['big_thumb_w'] : 800;
    	$big_thumb_h = isset( $upload_config['big_thumb_h'] ) ? $upload_config['big_thumb_h'] : 800;
    	$small_thumb_w = isset( $upload_config['small_thumb_w'] ) ? $upload_config['small_thumb_w'] : 200;
    	$small_thumb_h = isset( $upload_config['small_thumb_h'] ) ? $upload_config['small_thumb_h'] : 200;

		$type = $_FILES[$form_file_name]['type'];
		$size = $_FILES[$form_file_name]['size'];


		if( !in_array($type, $this->imgType_a) ){
			return array('error' => '', 'status' => 'fail', 'message' => 'Image Type is not supported');
		}

		if( !$size ){
			return array('error' => '', 'status' => 'fail', 'message' => 'No Image');
		}

		$tmp_name = $_FILES[$form_file_name]['tmp_name'];
        $rand      = mt_rand();
        $today     = date('YmdHis');
        $filename = $_FILES[$form_file_name]['name'];
        $for_ext_a = explode('.', $filename);
        $fileExt = end($for_ext_a);
        $new_name  = $rand.$today.'.'.$fileExt;

        $target_file = $upload_path.$new_name;
        $thumb_file = $upload_path.'thumbs/'.$new_name;
        $moveResult = move_uploaded_file($tmp_name, $target_file);
        // Check to make sure the move result is true before continuing
        if ($moveResult != true) {
            //echo "ERROR: File not uploaded. Try again.";
            return false;
        }
        $this->img_resize($target_file, $target_file, $big_thumb_w, $big_thumb_h, $fileExt);
        $this->img_resize($target_file, $thumb_file, $small_thumb_w, $small_thumb_h, $fileExt);
        return $new_name;

    }

    private $width = 0;
    private $height = 0;
    private function img_resize($target, $newcopy, $w, $h, $ext) {

        list($w_orig, $h_orig) = getimagesize($target);


        $scale_ratio = $w_orig / $h_orig;
        if (($w / $h) > $scale_ratio) {
               $w = $h * $scale_ratio;
        } else {
               $h = $w / $scale_ratio;
        }


        /*$this->width = $w_orig;
        $this->height = $h_orig;
        $optionArray = $this->crop($w_orig, $h_orig, $w, $h);
        $cropStartX = $optionArray['cropStartX'];
        $cropStartY = $optionArray['cropStartY'];
        $h = $optionArray['optimalHeight'];
        $h = $optionArray['optimalHeight'];*/


        $img = "";
        $ext = strtolower($ext);
        if ($ext == "gif"){
          $img = imagecreatefromgif($target);
        } else if($ext =="png"){
          $img = imagecreatefrompng($target);
        } else {
          $img = imagecreatefromjpeg($target);
        }
        $tci = imagecreatetruecolor($w, $h);
        // imagecopyresampled(dst_img, src_img, dst_x, dst_y, src_x, src_y, dst_w, dst_h, src_w, src_h)
        imagecopyresampled($tci, $img, 0, 0, 0, 0, $w, $h, $w_orig, $h_orig);
       /* imagecopyresampled($tci, $img, 0, 0, $cropStartX, $cropStartY, $w, $h, $w, $h);*/


        $imageQuality = 100;
        switch($ext)
        {
            case 'jpg':
            case 'jpeg':
                if (imagetypes() & IMG_JPG) {
                    imagejpeg($tci, $newcopy, $imageQuality);
                }
                break;

            case 'gif':
                if (imagetypes() & IMG_GIF) {
                    imagegif($tci, $newcopy);
                }
                break;

            case 'png':
                // *** Scale quality from 0-100 to 0-9
                $scaleQuality = round(($imageQuality/100) * 9);

                // *** Invert quality setting as 0 is best, not 9
                $invertScaleQuality = 9 - $scaleQuality;

                if (imagetypes() & IMG_PNG) {
                     imagepng($tci, $newcopy, $invertScaleQuality);
                }
                break;

            // ... etc

            default:
                // *** No extension - No save.
                break;
        }
    }

    public function img_resize_crop($source_file, $dst_dir,$max_width, $max_height, $ext){
        $imgsize = getimagesize($source_file);
        $width = $imgsize[0];
        $height = $imgsize[1];

        $ext = strtolower($ext);
        switch($ext){
            case 'gif':
                if (imagetypes() & IMG_GIF) {
                    $image_create = "imagecreatefromgif";
                    $image = "imagegif";
                }
                break;

            case 'png':
                if (imagetypes() & IMG_PNG) {
                    $image_create = "imagecreatefrompng";
                    $image = "imagepng";
                    $quality = 9;
                }
                break;

            case 'jpg':
            case 'jpeg':
                if (imagetypes() & IMG_JPG) {
                    $image_create = "imagecreatefromjpeg";
                    $image = "imagejpeg";
                    $quality = 100;
                }
                break;

            default:
                return false;
                break;
        }

        $dst_img = imagecreatetruecolor($max_width, $max_height);
        $src_img = $image_create($source_file);

        $width_new = $height * $max_width / $max_height;
        $height_new = $width * $max_height / $max_width;
        //if the new width is greater than the actual width of the image, then the height is too large and the rest cut off, or vice versa
        if($width_new > $width){
            //cut point by height
            $h_point = (($height - $height_new) / 2);
            //copy image
            imagecopyresampled($dst_img, $src_img, 0, 0, 0, $h_point, $max_width, $max_height, $width, $height_new);
        }else{
            //cut point by width
            $w_point = (($width - $width_new) / 2);
            imagecopyresampled($dst_img, $src_img, 0, 0, $w_point, 0, $max_width, $max_height, $width_new, $height);
        }

        $image($dst_img, $dst_dir, $quality);

        if($dst_img)imagedestroy($dst_img);
        if($src_img)imagedestroy($src_img);
    }

    private function getOptimalCrop($newWidth, $newHeight)
    {

        $heightRatio = $this->height / $newHeight;
        $widthRatio  = $this->width /  $newWidth;

        if ($heightRatio < $widthRatio) {
            $optimalRatio = $heightRatio;
        } else {
            $optimalRatio = $widthRatio;
        }

        $optimalHeight = $this->height / $optimalRatio;
        $optimalWidth  = $this->width  / $optimalRatio;

        return array('optimalWidth' => $optimalWidth, 'optimalHeight' => $optimalHeight);
    }

    private function crop($optimalWidth, $optimalHeight, $newWidth, $newHeight)
    {
        // *** Find center - this will be used for the crop
        $cropStartX = ( $optimalWidth / 2) - ( $newWidth /2 );
        $cropStartY = ( $optimalHeight/ 2) - ( $newHeight/2 );

        return array('cropStartX' => $cropStartX, 'cropStartY' => $cropStartY, 'optimalWidth' => $optimalWidth, 'optimalHeight' => $optimalHeight);

        /*$crop = $this->imageResized;*/

        // *** Now crop from center to exact requested size
        /*$this->imageResized = imagecreatetruecolor($newWidth , $newHeight);
        imagecopyresampled($this->imageResized, $crop , 0, 0, $cropStartX, $cropStartY, $newWidth, $newHeight , $newWidth, $newHeight);*/
    }

}