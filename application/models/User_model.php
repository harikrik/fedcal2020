<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class User_model extends CI_model
{

    private $checked_account_maps = false;

    public function __construct()
    {
        parent::__construct();
    }

    function create($data)
    {
        $validate_result = $this->signupValidate($data['username'], $data['password']);
        if( array_key_exists( 'error', $validate_result ) ){
            return $validate_result;
        }
        else{
            $password_hash = password_hash($data['password'], PASSWORD_DEFAULT);
            unset($data['password']);
            $ins_data = $data;
            $ins_data['password'] = $password_hash;
            // now insert into user table
            $this->db->insert('user', $ins_data);
            return array('user_id' => $this->db->insert_id() );
        }
    }

    public function signupValidate($username, $password)
    {
        /* return blank array if no error */
        $valid = true;
        if( $username == '' ){
            $valid = false;
            return array('error'=> '', 'status'=> 'fail', 'message'=> 'No Username');
        }
        if( $password == '' ){
            $valid = false;
            return array('error'=> '', 'status'=> 'fail', 'message'=> 'No Password');
        }
        if( $valid ){
            if( $this->check_username( $username ) ){
                return array('error'=> '', 'status'=> 'fail', 'message'=> 'A user with this username exists already!' );
            }
            else{
                /* return blank array if no error */
                return array();
            }
        }
    }

    public function check_username($username=''){
        if($username) {
            $this->db->select('*');
            $this->db->from('user');
            $this->db->where('username', $username);
            return $this->db->count_all_results();
        }
        return array();
    }

    /* public function cookieLogin($user_id) {
        $query = $this->db
                ->select("u.id as user_id,u.username,ut.usertype,ut.id as usertype_id,
                    CONCAT( COALESCE(u.first_name,''), ' ', COALESCE(u.middle_name,''), ' ', COALESCE(u.last_name,'') ) as full_name,
                    CONCAT( '".PROFILE_IMG."', IFNULL(u.profile_photo, 'default.png') ) as profile_pic")
                ->from('user u')
                ->join('usertype ut','u.usertype_id=ut.id')
                ->where('u.id', $user_id )
                ->where('u.is_active', 1 )
                ->where('u.is_deleted', 0 )
                ->get()->row();
        if ($query) {
            return $query;
        }
        else{
            return array('error'=> '', 'status'=> 'fail', 'message'=> 'Incorrect UserId!');
        }
    } */

    public function login($username='', $password='')
    {
        $validate_result = $this->loginValidate($username, $password);
        if( array_key_exists( 'error', $validate_result ) ){
            return $validate_result;
        }
        else{
            $query = $this->db
                ->select("u.id as user_id,u.username,u.password,ut.usertype,ut.id as usertype_id,
                    CONCAT( COALESCE(u.first_name,''), ' ', COALESCE(u.middle_name,''), ' ', COALESCE(u.last_name,'') ) as full_name,
                    CONCAT( '".PROFILE_IMG."', IFNULL(u.profile_photo, 'default.png') ) as profile_pic")
                ->from('user u')
                ->join('usertype ut','u.usertype_id=ut.id')
                ->where('u.username', $username )
                ->where('u.is_active', 1 )
                ->where('u.is_deleted', 0 )
                ->get()->row();
                
            if ($query) {
               // if ( password_verify($password, $query->password) ) {
                if ( $password == $query->password) {
                    return $query;

                } else {
                    return array('error'=> '', 'status'=> 'fail', 'message'=> 'Incorrect Username or password!');
                }
            }
            else{
                return array('error'=> '', 'status'=> 'fail', 'message'=> 'Incorrect Username or password!');
            }
        }
    }

    public function usertype($user_id){
        $query = $this->db
                ->select("ut.usertype,ut.id as usertype_id")
                ->from('user u')
                ->join('usertype ut','u.usertype_id=ut.id')
                ->where('u.id', $user_id )
                ->where('u.is_active', 1 )
                ->where('u.is_deleted', 0 )
                ->get()->row();
            if ($query) {
                return $query;
            }
            else{
                return array('error'=> '', 'status'=> 'fail', 'message'=> 'No Data!');
            }
    }

    public function loginValidate($username = '', $password = ''){
        /* return blank array if no error */
        $valid = true;
        if( $username == '' ){
            $valid = false;
            return array('error'=> '', 'status'=> 'fail', 'message'=> 'No Username');
        }
        if( $password == '' ){
            $valid = false;
            return array('error'=> '', 'status'=> 'fail', 'message'=> 'No Password');
        }
        if( $valid ){
            /* return blank array if no error */
            return array();
        }
    }


    public function changePassword($user_id, $curr_password, $new_password)
    {
        $validate_result = $this->passwordValidate($user_id);
        if( array_key_exists( 'error', $validate_result ) ){
            return $validate_result;
        }
        else{
            if ( password_verify($curr_password, $validate_result->password) ) {
                return $this->updatePassword($user_id, $new_password);
            }
            else{
                return array('error'=> '', 'status'=> 'fail', 'message'=> 'Invalid current Password!');
            }
        }
    }

    public function updatePassword($user_id, $new_password){
        $password_hash = password_hash($new_password, PASSWORD_DEFAULT);
        $updated = $this->db
        ->set('password', $password_hash)
        ->where('id', $user_id)
        ->update('user');
        if( $updated ){
            return array('status' => 'success', 'message' => 'Password Changed');
        }
        else{
            return array('error'=> '', 'status'=> 'fail', 'message'=> 'Sorry Some Error Occurred!');
        }
    }

    public function checkPassword($user_id, $curr_password)
    {
        $validate_result = $this->passwordValidate($user_id);
        if( array_key_exists( 'error', $validate_result ) ){
            return $validate_result;
        }
        else{
            if ( password_verify($curr_password, $validate_result->password) ) {
                return true;
            }
            else{
                return false;
            }
        }
    }

    public function userSettings($user_id)
    {
       $result = $this->db->select("username, first_name, middle_name, last_name, email, CONCAT( '".PROFILE_IMG."', IFNULL(profile_photo, 'default.png') ) as profile_photo")
        ->from('user')
        ->where('id', $user_id)
        ->get()->row();
        return $result;
    }
    public function existingGcmToken($gcm_token)
    {
        /* take only from user table */
       $result = $this->db->select("id, gcm_token")
        ->from('gcm_tokens')
        ->where('gcm_token', $gcm_token)
        ->get()->row();
        return $result;
    }

    public function gcmUserToken($customer_id)
    {
        /* take only from user table */
       $result = $this->db->select("gcm_token")
        ->from('gcm_tokens')
        ->where('customer_id', $customer_id)
        ->get()->row();
        return $result;
    }

    public function updateGCM($id, $customer_id, $registration_id)
    {
        $data = array(
            'gcm_token' => $registration_id,
            'customer_id' => $customer_id,
            );
        $updated = $this->db
        ->set($data)
        ->where('id', $id)
        ->update('gcm_tokens');
        if( $updated ){
            return array('status' => 'success', 'message' => 'Token Updated');
        }
        else{
            return array('error'=> '', 'status'=> 'fail', 'message'=> 'Sorry Some Error Occurred!');
        }
    }

    function insertGCM($ins_data)
    {
        $this->db->insert('gcm_tokens', $ins_data);
        return $this->db->insert_id();
    }

    public function updateSettings($user_id, $data)
    {
        $updated = $this->db
        ->set($data)
        ->where('id', $user_id)
        ->update('user');
        if( $updated ){
            return array('status' => 'success', 'message' => 'Settings Updated');
        }
        else{
            return array('error'=> '', 'status'=> 'fail', 'message'=> 'Sorry Some Error Occurred!');
        }
    }

    public function update($data)
    {
        $user_id = $data['id'];
        $usertype_id = $data['usertype_id'];
        unset($data['id']);
        unset($data['usertype_id']);

        $updated = $this->db
        ->set($data)
        ->where('id', $user_id)
        ->where('usertype_id', $usertype_id)
        ->update('user');
        if( $updated ){
            return array('status' => 'success', 'message' => 'Updated');
        }
        else{
            return array('error'=> '', 'status'=> 'fail', 'message'=> 'Sorry Some Error Occurred!');
        }
    }

    public function delete($data)
    {
        $id = $data['id'];
        unset($data['id']);
        $updated = $this->db
                ->set($data)
                ->where('id', $id)
                ->update('user');

        if( $updated ){
            return array('status' => 'success', 'message' => 'Updated Successfully!');
        }
        else{
            return array('error'=> '', 'status'=> 'fail', 'message'=> 'Sorry Some Error Occurred!');
        }
    }

    public function passwordValidate($user_id){
        /* return blank array if no error */
        $valid = true;
        $query = $this->db
        ->select('password')
        ->from('user')
        ->where('id', $user_id)
        ->where('is_active', 1)
        ->where('is_deleted', 0)
        ->get()->row();
        if( $query ){
            return $query;
        }
        else{
            return array('error'=> '', 'status'=> 'fail', 'message'=> 'User Doesn\'t Exist or not active!');
        }
    }

    public function check_userid($user_id=''){
        $this->db->select('*');
        $this->db->from('user');
        $this->db->where('id', $user_id);
        return $this->db->count_all_results();
    }

}