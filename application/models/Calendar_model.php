<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Calendar_model extends CI_model
{

	public function __construct()
	{
		parent::__construct();
	}

	function regionId($region){
		$query = $this->db
            ->select('')
            ->from('region')
            ->where('region', $region)
            ->where('is_active', 1)
            ->where('is_deleted', 0)
            ->get()->row();

						if( $query ){
							return $query->id;
						}else {
							return false;
						}

	}

	function apiMonthly($customer_id = 0, $year_month, $region_id = 0)
	{
		$events_query = "SELECT
						e.id,
						DATE_FORMAT(e.start_time,'%Y-%m-%d') as date,
						et.name as event_type,
						e.title as display_text, '' as detail,
						e.backgroundColor as bgcolor
					FROM events e
					INNER JOIN event_types et ON et.id = e.event_type
					WHERE DATE_FORMAT(e.start_time,'%Y-%m') = '$year_month'
					AND e.region_id IN (0,$region_id)
					";

		if($customer_id > 0){
			$customer_events = "SELECT
						e.id,
						DATE_FORMAT(e.date,'%Y-%m-%d') as date,
						'customer_event' as event_type,
						e.title as display_text, e.description as detail,
						'' as bgcolor
					FROM customer_events e
					WHERE DATE_FORMAT(e.date,'%Y-%m') = '$year_month'
					AND e.customer_id = $customer_id
					";
			$query = $this->db->query(
			"SELECT * FROM(
					$events_query
					UNION
					$customer_events
				) as a"
			)->result();
		}
		else{
			$query = $this->db->query("SELECT * FROM( $events_query ) as a")->result();
		}
		return $query;
	}

	function apiDaily($customer_id = 0, $date, $region_id = 0)
	{
		$events_query = "SELECT
						e.id,
						DATE_FORMAT(e.start_time,'%Y-%m-%d') as date,
						et.name as event_type,
						e.title as display_text, '' as detail,
						e.backgroundColor as bgcolor
					FROM events e
					INNER JOIN event_types et ON et.id = e.event_type
					WHERE DATE_FORMAT(e.start_time,'%Y-%m-%d') = '$date'
					AND e.region_id IN (0,$region_id)
					";

		if( $customer_id > 0 ){
			$customer_events = "SELECT
						e.id,
						DATE_FORMAT(e.date,'%Y-%m-%d') as date,
						'customer_event' as event_type,
						e.title as display_text, e.description as detail,
						'' as bgcolor
					FROM customer_events e
					WHERE DATE_FORMAT(e.date,'%Y-%m-%d') = '$date'
					AND e.customer_id = $customer_id
					";


			$query = $this->db->query(
			"SELECT * FROM(
					$events_query
					UNION
					$customer_events
				) as a"
			)->result();
		}
		else{
			$query = $this->db->query("SELECT * FROM( $events_query ) as a")->result();
		}

		return $query;
	}

	public function event_types(){
		$query = "SELECT
					id, name, backgroundColor, borderColor
				FROM event_types
				WHERE is_deleted = 0";
		return $this->db->query($query)->result();
	}

	public function add_event_type($data){
		$this->db->insert('event_types', $data);
		return $this->db->insert_id();
	}

	function dlt_event_type($id){
		$data = array(
			'id' => $id,
			);
		$deleted = $this->db->delete('event_types', $data);
	}

	public function add_event($data){
		$this->db->insert('events', $data);
		return $this->db->insert_id();
	}

	public function update_event($id, $data){
		$updated = $this->db
				->set($data)
				->where('id', $id)
				->update('events');
		return $updated;
	}

	function delete_event($id){
		$data = array(
			'id' => $id,
			);
		$deleted = $this->db->delete('events', $data);
		return $deleted;
	}


	public function events($start, $end, $region_id = 0)
	{
		$event_query = "SELECT
						id,
						DATE_FORMAT(start_time,'%Y-%m-%d %H:%i:%s') as start,
						DATE_FORMAT(end_time,'%Y-%m-%d %H:%i:%s') as end,
						allDay,	title,
						backgroundColor as color,
						'white' as textColor
					FROM events
					WHERE DATE_FORMAT(start_time,'%Y-%m-%d') >= '$start'
					AND DATE_FORMAT(end_time,'%Y-%m-%d') <= '$end'
					AND region_id = $region_id
					";
		return $this->db->query($event_query)->result();
	}

}
