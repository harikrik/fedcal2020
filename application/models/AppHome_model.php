<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class AppHome_model extends CI_model
{

	public function __construct()
	{
		parent::__construct();
	}

	public function user_details($user_id){
		$query = $this->db
				->query("SELECT
					u.id as user_id, CONCAT( COALESCE(u.first_name,''), ' ', COALESCE(u.middle_name,''), ' ', COALESCE(u.last_name,'') ) as full_name, CONCAT( '".PROFILE_IMG."', IFNULL(u.profile_photo, 'default.png') ) as profile_pic, ut.usertype,ut.id as usertype_id
				FROM user u
				INNER JOIN usertype ut ON u.usertype_id=ut.id
				WHERE u.id = $user_id
				AND `u`.`is_active` = 1 AND `u`.`is_deleted` =0
				")
				->row();
		return $query;
	}

}