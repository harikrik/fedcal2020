<?php

class Version_model extends CI_model
{

	public function __construct()
	{
		parent::__construct();
	}

    public function lists($where = "", $order = "", $limit = ""){
		$query = $this->db
		->query("SELECT
				id, version_name
			FROM app_version
			WHERE is_deleted = 0
			$where $order $limit");
		return $query;
	}

}