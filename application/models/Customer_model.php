<?php

class Customer_model extends CI_model
{

	public function __construct()
	{
		parent::__construct();
	}

    public function lists($where = "", $order = "", $limit = ""){
		$query = $this->db
		->query("SELECT
				id, name, mobile, email, dob
			FROM customer
			WHERE is_deleted = 0
			$where $order $limit");
		return $query;
	}

	public function user_lists($where = "", $order = "", $limit = ""){
		$query = $this->db
		->query("SELECT
				id, username, first_name, email
			FROM user
			WHERE usertype_id = 2
			AND is_deleted = 0
			$where $order $limit");
		return $query;
	}

	public function notification($where = "", $order = "", $limit = "", $customer_id = 0){
		$notification_query = "SELECT
				id, message, description, image, '' as image_path, type, time, created_time
			FROM notification
			WHERE is_deleted = 0
			AND (customer_id IS NULL OR customer_id = '' OR customer_id = 0)
			$where";

		$whole_query = $notification_query;

		if( $customer_id ){
			$image_path = base_url().NOTIFICATION_IMG;
			$customer_notification_query = "SELECT
				id, message, description, 
				image, '$image_path' as image_path, 
				type, time, created_time
			FROM notification
			WHERE is_deleted = 0
			AND customer_id = $customer_id
			$where ";
			$whole_query .= " UNION
					$customer_notification_query";
		}
	
		$query = $this->db->query(
			"SELECT * FROM(
					$whole_query
				)  as a $order $limit"
			);
		return $query;
	}

}