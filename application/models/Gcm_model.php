<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Gcm_model extends CI_model
{

	public function __construct()
	{
		parent::__construct();
	}


    public function getuserGcmTokens($customer_id, $sWhere=""){
        $query = $this->db
				->query(
				"SELECT
	                gcm_token, device_os
	            FROM gcm_tokens
	            WHERE customer_id = $customer_id
	            ORDER BY id DESC"
            )
			->result();
        return $query;

    }

    public function getWholeGcmTokens($sWhere="", $limit = ""){
        $query = wholeGcmTokenQuery($sWhere, $limit)->result();
        return $query;
	}

	public function wholeGcmTokenQuery($sWhere="", $limit = ""){
		$query = $this->db
				->query(
				"SELECT
	                gcm_token, device_os
	            FROM gcm_tokens
	            WHERE 1
	            $sWhere
	            GROUP BY gcm_token
				ORDER BY id DESC 
				$limit"
			);
		return $query;
	}
	

}