<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Common_model extends CI_Model {
	public function __construct()
	{
		parent::__construct();
	}
	public function getAll($fields = "", $table = "", $sWhere = array(), $limit = 10, $sOrder = "",$group_by =""){
		
		$this->db->select($fields)->from($table);
		
		if($sWhere){			
			foreach ($sWhere as $key => $val) {				
				$this->db->where($key, $val);				
			}			
		}
		
		if( $sOrder ){
			$this->db->order_by($sOrder);
		}		
		if( $limit > 0 )			
			return $this->db->limit($limit)->get()->result();
		else
			return $this->db->get()->result();
				
			
	}

	public function getCount($fields = "", $table = "", $sWhere = array()){
		$this->db->select($fields)->from($table);
		if($sWhere){
			foreach ($sWhere as $key => $val) {
				$this->db->where($key, $val);
			}
		}
		return $this->db->count_all_results();
	}

	public function getAllCount($table = "")
	{
	 	$this->db->select("*")->from($table);
		return $this->db->count_all_results();
	}

	public function getDetails($id = "", $table = ""){
		return $this->db->select("*")->from($table)->where('id',$id)->get()->row();
	}

	public function update($data =array() ,$id = "", $table = ""){
		$this->db->set('modified_time', 'NOW()', FALSE);
		$this->db->where('id',$id)->update($table,$data);
		return true;
	}

	public function insert($data =array(), $table = ""){
		$this->db->query("SET time_zone='+5:30'");
		$this->db->insert($table, $data);
		$insert_id = $this->db->insert_id(); //Get Last inserted id
		return $insert_id;
	}

	public function delete($id = "", $table = ""){
		$this->db->where('id',$id)->delete($table);
		return true;
	}

	public function updateExistOtp($customer_id = ""){
		$table = "otp";
		$cWhere = array('customer_id' => $customer_id,'is_active' => 1,'is_deleted' => 0);
		$res = $this->db->select("*")->from($table)->where($cWhere)->get()->result();

		for($i=0;$i<count($res);$i++){
      $dat=$res[$i];
			$updateotp = array(
																'is_active'=> 1,
																'is_deleted'=> 1
														);
					 $updateorditms = $this->db
					 ->set($updateotp)
					 ->where('id', $dat->id)
					 ->update($table);
		}
		return true;
	}

	public function validateRegOtp($mobile){
		$is_valid=false;
			$curr_time=date("Y-m-d H:i:s");
			$curr_time=date("Y-m-d H:i:s",strtotime($curr_time . "-330 minutes"));

			$table = "customer";
			$cWhere = array('mobile' => $mobile,'is_active' => 1,'is_deleted' => 0);
			$res = $this->db->select("*")->from($table)->where($cWhere)->get()->result();
			$cust_id='';
			$otp_creat_time='';
			for($i=0;$i<count($res);$i++){
	      $dat=$res[$i];
				$cust_id=$dat->id;
			}


			$tableo = "otp";
			$cWhereo = array('customer_id' => $cust_id,'is_active' => 1,'is_deleted' => 0);
			
			$reso = $this->db->select("*")->from($tableo)->where($cWhereo);
			$reso = $this->db->get()->result();
			for($j=0;$j<count($reso);$j++){
	      $dato=$reso[$j];
				$otp_creat_time=$dato->created_time;
			}
			if(count($reso)==0){
				$is_valid=true;
				return $is_valid;
			}

			$otp_creat_time_add30sec = date("Y-m-d H:i:s",strtotime($otp_creat_time . "+30 seconds"));

			if ($curr_time > $otp_creat_time_add30sec) {
				$is_valid=true;
			}else {
				$is_valid=false;
			}
			
		return $is_valid;
	}


		public function validateResendOtp($mobile){
			$is_valid=false;
				$curr_time=date("Y-m-d H:i:s");
				$curr_time=date("Y-m-d H:i:s",strtotime($curr_time . "-330 minutes"));

				$table = "customer";
				$cWhere = array('mobile' => $mobile,'is_active' => 1,'is_deleted' => 0);
				$res = $this->db->select("*")->from($table)->where($cWhere)->get()->result();
				$cust_id='';
				$otp_creat_time='';
				for($i=0;$i<count($res);$i++){
		      $dat=$res[$i];
					$cust_id=$dat->id;
				}


				$tableo = "otp";
				$cWhereo = array('customer_id' => $cust_id,'is_active' => 1,'is_deleted' => 0);
				$reso = $this->db->select("*")->from($tableo)->where($cWhereo)->get()->result();
				for($j=0;$j<count($reso);$j++){
		      $dato=$reso[$j];
					$otp_creat_time=$dato->created_time;
				}
				if(count($reso)==0){
					$is_valid=true;
					return $is_valid;
				}

				$otp_creat_time_add30sec = date("Y-m-d H:i:s",strtotime($otp_creat_time . "+15 minutes"));

				if ($curr_time > $otp_creat_time_add30sec) {
					$is_valid=true;
				}else {
					$is_valid=false;
				}

			return $is_valid;
		}

	public function validateTimeOTP($otp_id,$otp_creat_time){
		$is_valid=false;
      $curr_time=date("Y-m-d H:i:s");
			$curr_time=date("Y-m-d H:i:s",strtotime($curr_time . "-330 minutes"));
      $otp_creat_time_add2min = date("Y-m-d H:i:s",strtotime($otp_creat_time . "+2 minutes"));

      if ($curr_time > $otp_creat_time_add2min) {
           $is_valid=false;
					 $table = "otp";
			 		$updateotp = array(
			 															'is_active'=> 1,
			 															'is_deleted'=> 1
			 													);
			 		$updateorditms = $this->db
			 		->set($updateotp)
			 		->where('id', $otp_id)
			 		->update($table);
			 		$cWhere = array('id' => $otp_id);
			 		$res = $this->db->select("*")->from($table)->where($cWhere)->get()->result();
      }else{
           $is_valid=true;
      }



		return $is_valid;
	}

}
/* End of file */
