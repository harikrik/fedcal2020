<?php

defined('BASEPATH') OR exit('No direct script access allowed');


if ( ! function_exists('form_input'))
{
	function form_input($data = '', $value = '', $extra = '', $div_vars ='', $type='input')
	{
		$name = is_array($data) ? $data['name'] : $data;
		$defaults = array(
			'type' => 'text',
			'name' => is_array($data) ? '' : $data,
			'value' => $value
		);
		$div_class = isset($div_vars['div_class']) ? $div_vars['div_class'] : '';
		$required = isset($div_vars['required']) ? '<span class="text-red">*</span>' : '';
		$helptext = isset($div_vars['helptext']) ? '<p class="help-block">'.$div_vars['helptext'].'</p>' : '';
		$div_err_class = form_error($name) ? ' has-error' : '';
		$placeholder = isset($extra['placeholder']) ? $extra['placeholder'] : '';

		if( $type == 'textarea' ){
			unset($defaults['value']); // textareas don't use the value attribute
			$defaults['cols'] = '40';
			$defaults['rows'] = '10';
		}
		?>
		<div class="form-group <?php echo $div_class.$div_err_class; ?>">
            <label for="<?php echo $name; ?>"><?php echo $placeholder.$required; ?></label>

            <?php if( $type == 'textarea' ){ ?>
	            <textarea <?php echo _parse_form_attributes($data, $defaults)._attributes_to_string($extra) ; ?> ><?php echo html_escape($value); ?></textarea>
			<?php } else{
				?>
				<input <?php echo _parse_form_attributes($data, $defaults)._attributes_to_string($extra) ; ?> autocomplete="off" />
			<?php } ?>
			<?php echo $helptext; ?>
             <?php if (form_error($name)){ ?>
              <span class="help-block"><?php echo form_error($name); ?></span>
             <?php } ?>
          </div>
         <?php
		//return '<input '._parse_form_attributes($data, $defaults)._attributes_to_string($extra)." />\n";
	}
}

if ( ! function_exists('form_password'))
{
	function form_password($data = '', $value = '', $extra = '', $div_vars ='')
	{
		is_array($data) OR $data = array('name' => $data);
		$data['type'] = 'password';
		return form_input($data, $value, $extra, $div_vars);
	}
}


if ( ! function_exists('form_textarea'))
{
	function form_textarea($data = '', $value = '', $extra = '', $div_vars ='')
	{
		return form_input($data, $value, $extra, $div_vars, 'textarea');
	}
}