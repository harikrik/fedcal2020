<?php $css_ver = '0.0.1'; ?>
<!-- Bootstrap 3.3.5 -->
<link rel="stylesheet" href="<?php echo base_url().A_boot; ?>css/bootstrap.min.css">
<!-- Font Awesome -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
<!-- Ionicons -->
<link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
<!-- DataTables -->
 <link rel="stylesheet" href="<?php echo base_url().A_plugins; ?>datatables/dataTables.bootstrap.css">
<!-- Theme style -->
<link rel="stylesheet" href="<?php echo base_url().A_distCSS; ?>AdminLTE.min.css">
<!-- AdminLTE Skins. Choose a skin from the css/skins
     folder instead of downloading all of them to reduce the load. -->
<link rel="stylesheet" href="<?php echo base_url().A_distCSS; ?>skins/_all-skins.min.css">
<link rel="stylesheet" href="<?php echo base_url().A_CSS; ?>custom.css?v=<?php echo $css_ver; ?>">

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->


<?php
if( isset( $page_type ) ){

	switch( $page_type ){
		case 'calendar':
		?>
			<!-- fullCalendar 2.2.5-->
		    <link rel="stylesheet" href="<?php echo base_url().A_plugins; ?>fullcalendar/fullcalendar.min.css">
		    <link rel="stylesheet" href="<?php echo base_url().A_plugins; ?>fullcalendar/fullcalendar.print.css" media="print">
		<?php
		break;
		case 'timetable':
		?>
			<link rel="stylesheet" href="<?php echo base_url().A_plugins; ?>timepicker/bootstrap-timepicker.min.css">
			<style>
				#set_timeOverlay{ position:fixed; top:0; bottom:0; right:0; left:0; background: rgba(0,0,0,.6) }
				#set_timePopUp{ position:fixed; top:0; bottom:0; right:0; left:0; background: #fff; width:300px; height:240px; padding:20px; margin:auto;  }
				#set_timePopUp h4{ margin: 0; }
			</style>
			<?php
		break;
	}

}

if( isset( $page_name ) ){

	switch( $page_name ){
		

		case 'customer/create':
		case 'customer/edit':
		?>
		    <link rel="stylesheet" href="<?php echo base_url().A_plugins; ?>datepicker/datepicker3.css">
		<?php
		break;
	}
}
?>

