$(document).ready(function() {
  $('#calendar').fullCalendar({
        header: {
          left: 'prev,next today',
          center: 'title',
          right: 'month,agendaWeek,agendaDay'
        },
        buttonText: {
          today: 'today',
          month: 'month',
          week: 'week',
          day: 'day'
        },
        events: {
            url: base_url+"calendar/list",
            type: 'POST',
            data: function() { 
                return {
                    class: $('#selectClass').val(),
                    section: $('#selectSection').val(),
                    student: $('#selectStudent').val(),
                }
               },
            /*cache: true,*/
        },
     
         // Convert the allDay from string to boolean
         eventRender: function(event, element, view) {
          if (event.allDay === 'true') {
           event.allDay = true;
          } else {
           event.allDay = false;
          }
         },

        selectable: true,
        selectHelper: true,
        editable: true,
        droppable: true,
        select: function(start, end) {
      		var title = prompt('Event Title:');
      		var eventData;
      		if (title) {
      			eventData = {
      				title: title,
      				start: start,
      				end: end
      			};
      			$('#calendar').fullCalendar('renderEvent', eventData, true); // stick? = true
      		}
      		$('#calendar').fullCalendar('unselect');
      	},
  });

  $('#selectClass').change(function(){
      $('#calendar').fullCalendar('refetchEvents');
  });

  $('#selectSection').change(function(){
      $('#calendar').fullCalendar('refetchEvents');
  });

  $('#selectStudent').change(function(){
      $('#calendar').fullCalendar('refetchEvents');
  });

});
