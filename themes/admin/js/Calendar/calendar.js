$(function() {

    /* initialize the external events
         -----------------------------------------------------------------*/
    function ini_events(ele) {
        ele.each(function() {

            // create an Event Object (http://arshaw.com/fullcalendar/docs/event_data/Event_Object/)
            // it doesn't need to have a start or end
            var eventObject = {
                title: $.trim($(this).text()) // use the element's text as the event title
            };

            // store the Event Object in the DOM element so we can get to it later
            $(this).data('eventObject', eventObject);

            // make the event draggable using jQuery UI
            $(this).draggable({
                zIndex: 1070,
                revert: true, // will cause the event to go back to its
                revertDuration: 0 //  original position after the drag
            });

        });
    }
    ini_events($('#external-events div.external-event'));

    /* initialize the calendar
         -----------------------------------------------------------------*/
    //Date for the calendar events (dummy data)
    var date = new Date();
    var d = date.getDate(),
        m = date.getMonth(),
        y = date.getFullYear();


    $('#calendar').fullCalendar({
        header: {
            left: 'prev,next today',
            center: 'title',
            right: 'month,agendaWeek,agendaDay'
        },
        buttonText: {
            today: 'today',
            month: 'month',
            week: 'week',
            day: 'day'
        },
        //Random default events
        events: {
            url: base_url+"calendar/list",
            data: function () { 
                return {
                    region: $('.regions').val(),
                };
            },
            type: "POST",
        },
        editable: true,
        droppable: true, // this allows things to be dropped onto the calendar !!!
        drop: function(date, allDay) { // this function is called when something is dropped

            // retrieve the dropped element's stored Event Object
            var originalEventObject = $(this).data('eventObject');

            // we need to copy it, so that multiple events don't have a reference to the same object
            var copiedEventObject = $.extend({}, originalEventObject);
            copiedEventObject.title = copiedEventObject.title.replace('\n                          ×', "");
            var bgcolor = rgb2hex( $(this).css("background-color") );
            var event_type = $(this).find(".event_type").text();
            event_type = $.trim(event_type);

            date = formatDate(date);

            // assign it the date that was reported
            copiedEventObject.start = date;
            copiedEventObject.allDay = allDay;
            copiedEventObject.backgroundColor = bgcolor;
            copiedEventObject.borderColor = $(this).css("border-color");
            var title = $(this).find('.event_type_text').text();

            var region = $('.regions').val();

            $.ajax({
                url: base_url+"calendar/add_event",
                data: 'title=' + title + '&bgcolor=' + bgcolor + '&start='+date + '&end='+date + '&event_type='+event_type + '&region='+region,
                type: "POST",
                success: function(x) {
                    $('#calendar').fullCalendar( 'refetchEvents' );
                    console.log('Added Successfully');
                }
            });

            // render the event on the calendar
            // the last `true` argument determines if the event "sticks" (http://arshaw.com/fullcalendar/docs/event_rendering/renderEvent/)
            //$('#calendar').fullCalendar('renderEvent', copiedEventObject, true);

            // is the "remove after drop" checkbox checked?
            if ($('#drop-remove').is(':checked')) {
                // if so, remove the element from the "Draggable Events" list
                $(this).remove();
            }

        },
        editable: true,
        eventDrop: function(event, delta) {
            update_event(event);
        },
        eventResize: function(event) {
            update_event(event);

        },
        eventClick: function(calEvent, jsEvent, view) {
            console.log($(this).attr('class'));
          var title = prompt('Event Title:', calEvent.title, { buttons: { Ok: true, Cancel: false} });
          if (title){
              calEvent.title = title;
              update_event(calEvent);
              /*$('#calendar').fullCalendar('updateEvent', calEvent);*/
          }
        },
        eventDragStop: function(event,jsEvent) {

            var trashEl = jQuery('#calendarTrash');
            var ofs = trashEl.offset();

            var x1 = ofs.left;
            var x2 = ofs.left + trashEl.outerWidth(true);
            var y1 = ofs.top;
            var y2 = ofs.top + trashEl.outerHeight(true);

            if (jsEvent.pageX >= x1 && jsEvent.pageX<= x2 &&
                jsEvent.pageY>= y1 && jsEvent.pageY <= y2) {
                remove_event(event);
            }

        }
    });



    function remove_event(event){
        $.ajax({
            url: base_url+'calendar/remove_event',
            data: 'id=' + event.id,
            type: "POST",
            success: function(json) {
                console.log("Deleted Successfully");
                $('#calendar').fullCalendar('removeEvents',event._id);
            }
        });
    }

    function update_event(event){
        var start = currDateFormat(event.start);
        var end = currDateFormat(event.end);
        $.ajax({
            url: base_url+'calendar/update_event',
            data: 'title=' + event.title + '&start=' + start + '&end=' + end + '&id=' + event.id,
            type: "POST",
            success: function(json) {
                console.log("Updated Successfully");

                $('#calendar').fullCalendar('updateEvent', event);
            }
        });
    }

    function formatDate(date) {
        var d = new Date(date),
            month = '' + (d.getMonth() + 1),
            day = '' + d.getDate(),
            year = d.getFullYear();

        if (month.length < 2) month = '0' + month;
        if (day.length < 2) day = '0' + day;

        return [year, month, day].join('-');
    }

    function currDateFormat(date) {
        var d = new Date(date);
        var timezone = d.getTimezoneOffset();
        // console.log( timezone );
        var year = d.getFullYear().toString();
        var month = d.getMonth();
        var date = d.getDate().toString();
        var hour = d.getHours().toString();
        var minute = d.getMinutes().toString();
        month = month + 1;
        month = month.toString();
        if (month.length < 2) {
            month = '0' + month;
        }
        if (date.length < 2) {
            date = '0' + date;
        }
        if (hour.length < 2) {
            hour = '0' + hour;
        }
        if (minute.length < 2) {
            minute = '0' + minute;
        }
        var day = [year, month, date].join('-');
        var time = [hour, minute, '00'].join(':');
        return day+' '+time;
    }


    var hexDigits = new Array
        ("0","1","2","3","4","5","6","7","8","9","a","b","c","d","e","f"); 

    //Function to convert hex format to a rgb color
    function rgb2hex(rgb) {
        rgb = rgb.match(/^rgb\((\d+),\s*(\d+),\s*(\d+)\)$/);
        return "#" + hex(rgb[1]) + hex(rgb[2]) + hex(rgb[3]);
    }

    function hex(x) {
        return isNaN(x) ? "00" : hexDigits[(x - x % 16) / 16] + hexDigits[x % 16];
     }





    /* ADDING EVENTS */
    var currColor = "#3c8dbc"; //light-blue by default
    bgColor = "light-blue"; //light-blue by default
    //Color chooser button
    var colorChooser = $("#color-chooser-btn");
    $("#color-chooser > li > a").click(function(e) {
        e.preventDefault();
        //Save color
        currColor = $(this).css("color");
        bgColor = $(this).attr("color");
        //Add color effect to button
        $('#add_event_type').css({
            "background-color": currColor,
            "border-color": currColor
        });
    });
    $("#add_event_type").click(function(e) {
        e.preventDefault();
        //Get value and make sure it is not null
        var val = $("#new_event_type").val();
        if (val.length == 0) {
            return;
        }

        //Create events
        var event = $("<div />");
        event.css({
            "background-color": currColor,
            "border-color": currColor,
            "color": "#fff"
        }).addClass("external-event");
        event.html('<span class="event_type_text">'+val+'</span>');

        console.log(bgColor);
        console.log(currColor);

        $.ajax({
            type:"POST",
            url: base_url+"calendar/add_event_type",
            data: "name="+val+"&bgcolor="+bgColor,
            beforeSend:function(){
                //$('.clearClassBlock').html('<div class="alert alert-danger alert-dismissable">Select A Section.</div>');
            },
            complete:function(){
                
            },
            success:function(x){
                if(  x != 'fail' ){
                    var event_type = '<button type="button" class="close dlt_event_type" data-dismiss="alert" aria-hidden="true" event_type_id="'+x+'">×</button>';
                    $(event).prepend(event_type)
                    $('#external-events').prepend(event);
                }

                //Add draggable funtionality
                ini_events(event);

                //Remove event from text input
                $("#new_event_type").val("");
            },
            error:function(x){
                //Add draggable funtionality
                ini_events(event);

                //Remove event from text input
                $("#new_event_type").val("");
            }
        });

        
    });
});


var calendar = {
    dlt_event_type: function(){
        var self = $(this);
        var event_type_id = parseInt($(self).attr('event_type_id') );
        console.log(event_type_id);

        $.ajax({
            type:"POST",
            url: base_url+"calendar/dlt_event_type",
            data: "id="+event_type_id,
            beforeSend:function(){
                //$('.clearClassBlock').html('<div class="alert alert-danger alert-dismissable">Select A Section.</div>');
            },
            complete:function(){
                $(self).parent().remove();
            },
            success:function(x){
               
            },
            error:function(x){
                
            }
        });
    },
    change_region: function(){
        $('#calendar').fullCalendar( 'refetchEvents' );
    },
}

$(document).on('click', '.dlt_event_type', calendar.dlt_event_type);
$(document).on('change', '.regions', calendar.change_region);