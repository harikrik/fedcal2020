

var common = new function() {

    this.sidebar_search = function() {
    	var search_input = $(this);
        var search_text = search_input.val();
		search_text = search_text.trim();
		var nav_items = $('ul.sidebar-menu li.nav_item');
		if( search_text != '' ){
			nav_items.hide();
			var nav_div = '';
			var nav_text = '';
			nav_items.find( 'span.nav_text' ).each( function(){
				nav_div = $(this);
				nav_text = nav_div.html().trim();
				if( nav_text != '' ){
					if( common.search( search_text, nav_text ) ){
						nav_div.parent('a').parent('li').show();
					}
				}
			});
		}
		else{
			nav_items.show();
		}
    }

    this.search = function(needle, haystack){
    	/*needle*/
		needle = needle.toLowerCase();

		/*haystack*/
		haystack = haystack.toLowerCase();
		if( haystack.indexOf( needle ) != -1 ){
			return true;
		}
		return false;
    }

    this.changeCheck = function(){
    	var isChecked = $('.all_check:checkbox:checked').length > 0;
    	$(".single_check").prop("checked", isChecked); 	
    }

}

$( document ).on( 'keyup', '#sidebar_search', common.sidebar_search);
$( document ).on( 'change', '.all_check', common.changeCheck);

