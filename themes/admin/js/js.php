<?php $ver = '0.1.0'; ?>
<!-- jQuery 2.1.4 -->
<script src="<?php echo base_url().A_plugins; ?>jQuery/jQuery-2.1.4.min.js"></script>
<!-- Bootstrap 3.3.5 -->
<script src="<?php echo base_url().A_boot; ?>js/bootstrap.min.js"></script>
<!-- SlimScroll -->
<script src="<?php echo base_url().A_plugins; ?>slimScroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="<?php echo base_url().A_plugins; ?>fastclick/fastclick.min.js"></script>
<script src="<?php echo base_url().A_plugins; ?>underscore/underscore-min.js"></script>
<!-- AdminLTE App -->
<script src="<?php echo base_url().A_distJS; ?>app.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="<?php echo base_url().A_distJS; ?>demo.js?ver=<?php echo $ver; ?>"></script>

<script src="<?php echo base_url().A_JS; ?>common.js?ver=<?php echo $ver; ?>"></script>

<script type="text/javascript">
	var base_url = '<?php echo base_url().$url_prefix;?>';
</script>

<?php
if( !isset($page_type) ){
	$page_type = '';
}

switch( $page_type ){
	case 'listing':
	?>
		<!-- DataTables -->
		<script src="<?php echo base_url().A_plugins; ?>datatables/jquery.dataTables.min.js"></script>
	    <script src="<?php echo base_url().A_plugins; ?>datatables/dataTables.bootstrap.min.js"></script>
		<?php
	break;
	case 'calendarOld':
	?>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.2/moment.min.js"></script>
	<script src="<?php echo base_url().A_plugins; ?>fullcalendar/fullcalendar.min.js"></script>
	<script src="<?php echo base_url().A_JS; ?>Calendar/calendar.js?ver=<?php echo $ver; ?>"></script>
	<?php
	break;
	case 'calendar':
	?>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.2/moment.min.js"></script>
	<!-- jQuery UI 1.11.4 -->
    <script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
	<script src="<?php echo base_url().A_plugins; ?>fullcalendar/fullcalendar.min.js"></script>
	<script src="<?php echo base_url().A_JS; ?>Calendar/calendar.js?ver=<?php echo $ver; ?>"></script>
	<?php
	break;
}

switch( $page_name ){
	case 'appuser/index':
		?>
		<!-- DataTables -->
	    <script src="<?php echo base_url().A_JS; ?>Appuser/list.js?ver=<?php echo $ver; ?>"></script>
		<?php
		break;
	case 'version/index':
		?>
		<!-- DataTables -->
	    <script src="<?php echo base_url().A_JS; ?>Version/list.js?ver=<?php echo $ver; ?>"></script>
		<?php
		break;
	case 'seasonal/index':
		?>
		<!-- DataTables -->
	    <script src="<?php echo base_url().A_JS; ?>Seasonal/list.js?ver=<?php echo $ver; ?>"></script>
		<?php
		break;
	case 'adminuser/index':
		?>
		<!-- DataTables -->
	    <script src="<?php echo base_url().A_JS; ?>AdminUser/list.js?ver=<?php echo $ver; ?>"></script>
		<?php
		break;
	case 'adminuser/create':
	case 'adminuser/edit':
	?>
		<script src="<?php echo base_url().A_plugins; ?>datepicker/bootstrap-datepicker.js"></script>
		<script src="<?php echo base_url().A_JS; ?>Appuser/customer.js?ver=<?php echo $ver; ?>"></script>
		<?php
	break;
}