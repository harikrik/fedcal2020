$(function () {
	$( '#example1' ).dataTable( {
		responsive: true,
		"bDestroy": true,
		"bAutoWidth": false,
		"serverSide": true,
		"sAjaxSource": base_url+"adminuser/list_entries",
		"sSearch": "Search all columns:",
		"aoColumns": [
			{ "sClass": "center", "sWidth": '2%', "mDataProp": "row_num", "bSortable": false, "bSearchable": false  },
			{ "sClass": "center", "sWidth": '5%', "mDataProp": "username" },
			{ "sClass": "center", "sWidth": '5%', "mDataProp": "first_name" },
			{ "sClass": "center", "sWidth": '5%', "mDataProp": "email" },
			{ "sClass": "center", "sWidth": '8%', "mDataProp": "actions", "bSortable": false, "bSearchable": false }
		],
		"sServerMethod": "POST"
	} );
});

oTable = $( '#example1' ).dataTable();

$( '#filter' ).keyup( function(){
	oTable.fnFilter( $( this ).val() );
} );



$(document).on('click', '.del_btn', function(e){
	e.preventDefault();
	var r = confirm('Are You sure You want to delete this?');
	if( r ){
		var del_action = $(this).siblings('.del_action').attr('href');
		window.location = del_action;
	}
});